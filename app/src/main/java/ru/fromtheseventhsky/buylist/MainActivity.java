package ru.fromtheseventhsky.buylist;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import ru.fromtheseventhsky.buylist.fragments.BuyListFragment;
import ru.fromtheseventhsky.buylist.fragments.BuyListPager;
import ru.fromtheseventhsky.buylist.fragments.FragmentCreateBuyList;
import ru.fromtheseventhsky.buylist.fragments.FragmentCreateBuyListItem;
import ru.fromtheseventhsky.buylist.fragments.MainPageFragment;
import ru.fromtheseventhsky.buylist.loaders.LoaderCreateBuyListItem;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyList;
import ru.fromtheseventhsky.buylist.utils.BuyListItem;
import ru.fromtheseventhsky.buylist.utils.DBHelper;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.StrConst;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class MainActivity extends ActionBarActivity implements MainPageFragment.MainPageFragmentListener,
        BuyListPager.OnFragmentInteractionListener, FragmentCreateBuyList.CreateBuyListInterface,
        BuyListFragment.OnFragmentInteractionListener, FragmentCreateBuyListItem.CreateBuyListItemInterface, LoaderManager.LoaderCallbacks<Long>{

    private int animationTime;

    MainPageFragment mainPageFragment;
    BuyListFragment buyListFragment;
    FragmentCreateBuyListItem fragmentCreateBuyListItem;
    FragmentCreateBuyList fragmentCreateBuyList;

    FragmentManager fm;

    private ObjectAnimator animateSlideFragmentOpen;
    private ObjectAnimator animateSlideFragmentClose;

    private FrameLayout containerCreateBL;
    private FrameLayout containerCreateBLI;
    private FrameLayout containerMain;
    private FrameLayout containerPreference;
    private FrameLayout containerBL;

    private int displayWidth;

    LoaderManager.LoaderCallbacks<Long> callback;

    private long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = this.getResources();

        SharedPreferences shp = getSharedPreferences(StrConst.APP_PREFS, MODE_PRIVATE);

        if(shp.getBoolean(StrConst.PREF_FIRST_START, true)){
            SharedPreferences.Editor edit = shp.edit();

            edit.putBoolean(StrConst.PREF_FIRST_START, false);
            edit.putBoolean(StrConst.COST_AVAILABLE, true);
            edit.putBoolean(StrConst.CATEGORIES_AVAILABLE, true);
            edit.putInt(StrConst.PREF_SCHEME_NUM, 0);
            edit.putInt(StrConst.PREF_FONT_SIZE_NUM, 1);
            edit.putInt(StrConst.PREF_FONT_TYPE_NUM, 1);
            edit.putInt(StrConst.PREF_COLOR_BCK, 0);
            edit.putInt(StrConst.PREF_COLOR_TXT, 4);
            edit.putInt(StrConst.PREF_COLOR_TEMP, 0);
            edit.putBoolean(StrConst.PREF_VIBRATION, true);
            edit.putBoolean(StrConst.PREF_REMEMBER, true);
            edit.putBoolean(StrConst.PREF_SHUT_OFF_DISPLAY, false);
            edit.putInt(StrConst.PREF_DELETE_PERIOD, -1);
            edit.putString(StrConst.PREF_CURRENCY, this.getResources().getString(R.string.currency_ru));
            edit.putBoolean(StrConst.DOUBLE_TAP_TO_EDIT, true);

            AppPreferences.currency = this.getResources().getString(R.string.currency_ru);
            AppPreferences.colorThemeNumber = 0;

            AppPreferences.costAvailable = true;
            AppPreferences.categoryAvailable = true;
            AppPreferences.colorTheme_Actionbar = res.getColor(R.color.OrangeTheme_Actionbar);
            AppPreferences.colorTheme_Background = res.getColor(R.color.OrangeTheme_Background);
            AppPreferences.colorTheme_EditTextBck = res.getColor(R.color.OrangeTheme_EdiTextBck);
            AppPreferences.colorTheme_FontColor = res.getColor(R.color.OrangeTheme_FontColor);
            AppPreferences.colorTheme_SelectedBtnColor = res.getColor(R.color.OrangeTheme_SelectedBtnColor);
            AppPreferences.colorTheme_DeselectedBtnColor = res.getColor(R.color.OrangeTheme_DeselectedBtnColor);
            AppPreferences.colorTheme_DisabledBtnColor = res.getColor(R.color.OrangeTheme_DisabledBtnColor);
            AppPreferences.colorTheme_HintColor = res.getColor(R.color.OrangeTheme_HintColor);
            AppPreferences.colorTheme_ListViewShadow = res.getColor(R.color.OrangeTheme_ListViewShadow);


            AppPreferences.doubleTapToEdit = true;
            AppPreferences.vibrationEnabled = true;
            AppPreferences.remember = true;
            AppPreferences.weightType = new ArrayList<>(Arrays.asList(res.getStringArray(R.array.weight_type)));
            AppPreferences.defaultBckColor = 0;
            AppPreferences.defaultTextColor = 4;
            AppPreferences.defaultTempColor = 0;
            AppPreferences.deletePeriod = -1;
            AppPreferences.screenLightAlways = false;

            AppPreferences.fontSize = 1;
            AppPreferences.fontSizeButton = res.getDimension(R.dimen.Pref_medium_SizeButton);
            AppPreferences.fontSizeTitleList = res.getDimension(R.dimen.Pref_medium_SizeTitleList);
            AppPreferences.fontSizeCommentList = res.getDimension(R.dimen.Pref_medium_SizeCommentList);
            AppPreferences.fontSizeCurrencyAndWeight = res.getDimension(R.dimen.Pref_medium_SizeCurrencyAndWeight);
            AppPreferences.fontSizeDate = res.getDimension(R.dimen.Pref_medium_SizeDate);
            AppPreferences.fontSizeEditText = res.getDimension(R.dimen.Pref_medium_SizeEditText);
            AppPreferences.fontSizeTextView = res.getDimension(R.dimen.Pref_medium_SizeTextView);
            AppPreferences.fontSizeCheckBox = res.getDimension(R.dimen.Pref_medium_SizeCheckBox);
            AppPreferences.fontSizeSpinner = res.getDimension(R.dimen.Pref_medium_SizeSpinner);
            AppPreferences.fontSizePrefGoodsTitle = res.getDimension(R.dimen.Pref_medium_pref_goods_text_size);
            AppPreferences.fontSizePrefComment = res.getDimension(R.dimen.Pref_medium_pref_comment_size);
            AppPreferences.fontSizePrefTitle = res.getDimension(R.dimen.Pref_medium_pref_text_size);





            edit.commit();
        } else {
            AppPreferences.currency = shp.getString(StrConst.PREF_CURRENCY, this.getResources().getString(R.string.currency_ru));

            AppPreferences.colorThemeNumber = shp.getInt(StrConst.PREF_SCHEME_NUM, 0);
            AppPreferences.fontSize = shp.getInt(StrConst.PREF_FONT_SIZE_NUM, 1);
            AppPreferences.vibrationEnabled = shp.getBoolean(StrConst.PREF_VIBRATION, true);
            AppPreferences.remember = shp.getBoolean(StrConst.PREF_REMEMBER, true);

            AppPreferences.doubleTapToEdit = shp.getBoolean(StrConst.DOUBLE_TAP_TO_EDIT, true);

            AppPreferences.defaultBckColor = shp.getInt(StrConst.PREF_COLOR_BCK, 0);
            AppPreferences.defaultTextColor = shp.getInt(StrConst.PREF_COLOR_TXT, 4);
            AppPreferences.defaultTempColor = shp.getInt(StrConst.PREF_COLOR_TEMP, 0);
            AppPreferences.deletePeriod = shp.getInt(StrConst.PREF_DELETE_PERIOD, -1);
            AppPreferences.screenLightAlways = shp.getBoolean(StrConst.PREF_SHUT_OFF_DISPLAY, false);

            AppPreferences.costAvailable = shp.getBoolean(StrConst.COST_AVAILABLE, false);
            AppPreferences.categoryAvailable = shp.getBoolean(StrConst.CATEGORIES_AVAILABLE, false);


            switch(AppPreferences.colorThemeNumber){
                case 0:
                    AppPreferences.colorTheme_Actionbar = res.getColor(R.color.OrangeTheme_Actionbar);
                    AppPreferences.colorTheme_Background = res.getColor(R.color.OrangeTheme_Background);
                    AppPreferences.colorTheme_EditTextBck = res.getColor(R.color.OrangeTheme_EdiTextBck);
                    AppPreferences.colorTheme_FontColor = res.getColor(R.color.OrangeTheme_FontColor);
                    AppPreferences.colorTheme_SelectedBtnColor = res.getColor(R.color.OrangeTheme_SelectedBtnColor);
                    AppPreferences.colorTheme_DeselectedBtnColor = res.getColor(R.color.OrangeTheme_DeselectedBtnColor);
                    AppPreferences.colorTheme_DisabledBtnColor = res.getColor(R.color.OrangeTheme_DisabledBtnColor);
                    AppPreferences.colorTheme_HintColor = res.getColor(R.color.OrangeTheme_HintColor);
                    AppPreferences.colorTheme_ListViewShadow = res.getColor(R.color.OrangeTheme_ListViewShadow);
                    break;
            }


            AppPreferences.weightType = new ArrayList<>(Arrays.asList(this.getResources().getStringArray(R.array.weight_type)));


            switch(AppPreferences.fontSize){
                case 0:
                    AppPreferences.fontSizeButton = res.getDimension(R.dimen.Pref_small_SizeButton);
                    AppPreferences.fontSizeTitleList = res.getDimension(R.dimen.Pref_small_SizeTitleList);
                    AppPreferences.fontSizeCommentList = res.getDimension(R.dimen.Pref_small_SizeCommentList);
                    AppPreferences.fontSizeCurrencyAndWeight = res.getDimension(R.dimen.Pref_small_SizeCurrencyAndWeight);
                    AppPreferences.fontSizeDate = res.getDimension(R.dimen.Pref_small_SizeDate);
                    AppPreferences.fontSizeEditText = res.getDimension(R.dimen.Pref_small_SizeEditText);
                    AppPreferences.fontSizeTextView = res.getDimension(R.dimen.Pref_small_SizeTextView);
                    AppPreferences.fontSizeCheckBox = res.getDimension(R.dimen.Pref_small_SizeCheckBox);
                    AppPreferences.fontSizeSpinner = res.getDimension(R.dimen.Pref_small_SizeSpinner);
                    AppPreferences.fontSizePrefGoodsTitle = res.getDimension(R.dimen.Pref_small_pref_goods_text_size);
                    AppPreferences.fontSizePrefComment = res.getDimension(R.dimen.Pref_small_pref_comment_size);
                    AppPreferences.fontSizePrefTitle = res.getDimension(R.dimen.Pref_small_pref_text_size);
                    break;
                case 1:
                    AppPreferences.fontSizeButton = res.getDimension(R.dimen.Pref_medium_SizeButton);
                    AppPreferences.fontSizeTitleList = res.getDimension(R.dimen.Pref_medium_SizeTitleList);
                    AppPreferences.fontSizeCommentList = res.getDimension(R.dimen.Pref_medium_SizeCommentList);
                    AppPreferences.fontSizeCurrencyAndWeight = res.getDimension(R.dimen.Pref_medium_SizeCurrencyAndWeight);
                    AppPreferences.fontSizeDate = res.getDimension(R.dimen.Pref_medium_SizeDate);
                    AppPreferences.fontSizeEditText = res.getDimension(R.dimen.Pref_medium_SizeEditText);
                    AppPreferences.fontSizeTextView = res.getDimension(R.dimen.Pref_medium_SizeTextView);
                    AppPreferences.fontSizeCheckBox = res.getDimension(R.dimen.Pref_medium_SizeCheckBox);
                    AppPreferences.fontSizeSpinner = res.getDimension(R.dimen.Pref_medium_SizeSpinner);
                    AppPreferences.fontSizePrefGoodsTitle = res.getDimension(R.dimen.Pref_medium_pref_goods_text_size);
                    AppPreferences.fontSizePrefComment = res.getDimension(R.dimen.Pref_medium_pref_comment_size);
                    AppPreferences.fontSizePrefTitle = res.getDimension(R.dimen.Pref_medium_pref_text_size);
                    break;
                case 2:
                    AppPreferences.fontSizeButton = res.getDimension(R.dimen.Pref_large_SizeButton);
                    AppPreferences.fontSizeTitleList = res.getDimension(R.dimen.Pref_large_SizeTitleList);
                    AppPreferences.fontSizeCommentList = res.getDimension(R.dimen.Pref_large_SizeCommentList);
                    AppPreferences.fontSizeCurrencyAndWeight = res.getDimension(R.dimen.Pref_large_SizeCurrencyAndWeight);
                    AppPreferences.fontSizeDate = res.getDimension(R.dimen.Pref_large_SizeDate);
                    AppPreferences.fontSizeEditText = res.getDimension(R.dimen.Pref_large_SizeEditText);
                    AppPreferences.fontSizeTextView = res.getDimension(R.dimen.Pref_large_SizeTextView);
                    AppPreferences.fontSizeCheckBox = res.getDimension(R.dimen.Pref_large_SizeCheckBox);
                    AppPreferences.fontSizeSpinner = res.getDimension(R.dimen.Pref_large_SizeSpinner);
                    AppPreferences.fontSizePrefGoodsTitle = res.getDimension(R.dimen.Pref_large_pref_goods_text_size);
                    AppPreferences.fontSizePrefComment = res.getDimension(R.dimen.Pref_large_pref_comment_size);
                    AppPreferences.fontSizePrefTitle = res.getDimension(R.dimen.Pref_large_pref_text_size);
                    break;
            }


        }
        displayWidth = getResources().getDisplayMetrics().widthPixels;




        fm = getSupportFragmentManager();
        callback = this;

        if(!Utils.hasInit) {
            containerCreateBL = (FrameLayout) findViewById(R.id.layCreateBL);
            containerCreateBLI = (FrameLayout) findViewById(R.id.layCreateBLI);
            containerMain = (FrameLayout) findViewById(R.id.layMainPage);
            containerPreference = (FrameLayout) findViewById(R.id.layPreference);
            containerBL = (FrameLayout) findViewById(R.id.layBL);



            animateSlideFragmentOpen = ObjectAnimator.ofFloat(containerCreateBL, "translationY", containerCreateBL.getTranslationY(), -displayWidth);
            animateSlideFragmentOpen.setDuration(300);

            animateSlideFragmentClose = ObjectAnimator.ofFloat(containerCreateBL, "translationY", containerCreateBL.getTranslationY(), 0);
            animateSlideFragmentClose.setDuration(300);
            Utils.init();
            DBHelper.initWritable(this);

            containerCreateBL.setTranslationX(-displayWidth);
            containerCreateBLI.setTranslationX(displayWidth);
            containerBL.setTranslationX(displayWidth);

            setTitleMainPage();

            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(AppPreferences.colorTheme_Actionbar));

            mainPageFragment = MainPageFragment.newInstance();
            buyListFragment = BuyListFragment.newInstance();
            fragmentCreateBuyList = FragmentCreateBuyList.newInstance();
            fragmentCreateBuyListItem = FragmentCreateBuyListItem.newInstance();

            setColorActionBar();

            startLoader(IntConst.LOADER_GET_COLOR, null);

            fm.beginTransaction().replace(R.id.layMainPage, mainPageFragment).commit();
        }


    }

    public void setColorActionBar(){
        int glowDrawableId = this.getResources().getIdentifier("overscroll_glow", "drawable", "android");
        Drawable androidGlow = this.getResources().getDrawable(glowDrawableId);
        androidGlow.setColorFilter(AppPreferences.colorTheme_ListViewShadow, PorterDuff.Mode.SRC_IN);
    }

    public void startLoader(int id, Bundle b){
        getSupportLoaderManager().initLoader(id, b, callback).forceLoad();
    }

    //TODO onCreateLoader
    @Override
    public Loader<Long> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case IntConst.LOADER_GET_COLOR:
                return new LoaderCreateBuyListItem(this, IntConst.LOADER_GET_COLOR);
        }
        Utils.loaderStarted = true;
        return null;
    }
    @Override
    public void onLoadFinished(Loader<Long> loader, Long rowId) {
        switch(loader.getId()){
            case IntConst.LOADER_GET_COLOR:
                getLoaderManager().destroyLoader(IntConst.LOADER_GET_COLOR);
                break;
        }
        Utils.loaderStarted = false;
    }
    @Override
    public void onLoaderReset(Loader<Long> loader) {
        Utils.loaderStarted = false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        //Toast.makeText(this,"",Toast.LENGTH_LONG).show();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void resetGoodsList(){
        mainPageFragment.resetGoodsList();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {

            case KeyEvent.KEYCODE_MENU:
                switch (Utils.currentFragment){
                    case IntConst.FRAGMENT_MAIN:
                        mainPageFragment.clickPrefBtn();
                        break;
                }

                break;

            default:
                break;
        }
        return super.onKeyDown(keyCode, event);
    }




    //НАЖАТИЕ НА КНОПКУ СОЗДАТЬ СПИСОК ПОКУПОК
    @Override
    public void clickOnBtnCreateBuyList() {
        setTitleCreateBL();
        Utils.currentFragment = IntConst.FRAGMENT_CREATE_BL;
        fm.beginTransaction().replace(R.id.layCreateBL, fragmentCreateBuyList).commit();

        if(animateSlideFragmentClose.isStarted()){
            animateSlideFragmentClose.cancel();
        }
        if(!animateSlideFragmentOpen.isStarted()) {
            animateSlideFragmentOpen = ObjectAnimator.ofFloat(containerCreateBL, "translationX", containerCreateBL.getTranslationX(), 0);
            animateSlideFragmentOpen.setDuration(300);
            animateSlideFragmentOpen.start();
        }

        animateSlideFragmentOpen.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                fragmentCreateBuyList.showSoftKeyboard();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }


    //НАЖАТИЕ НА КНОПКУ ДОБАВИТЬ ТОВАР
    @Override
    public void changeTabToBL(BuyListItem buyListItem) {
        Utils.currentFragment = IntConst.FRAGMENT_BL;
        setTitleGoodsList();
        buyListFragment.setData(buyListItem);
        closeFragmentCreateBLI();
    }

    @Override
    public void clickOnBuyListViewItem() {
        Utils.currentFragment = IntConst.FRAGMENT_BL;
        containerBL.setTranslationX(displayWidth);
        fm.beginTransaction().replace(R.id.layBL, buyListFragment).commit();
        buyListFragment.openFragment();

        //pager.setCurrentItem(2);
    }

    @Override
    public void changeTabToBL() {
        setTitleGoodsList();
        if(animateSlideFragmentClose.isStarted()){
            animateSlideFragmentClose.cancel();
        }
        if(!animateSlideFragmentOpen.isStarted()) {
            animateSlideFragmentOpen = ObjectAnimator.ofFloat(containerBL, "translationX", containerBL.getTranslationX(), 0);
            animateSlideFragmentOpen.setDuration(300);
            animateSlideFragmentOpen.start();
        }
    }


    //НАЖАТИЕ ДВОЙНОЕ НА ПУНКТ ТОВАРА В СПИСКЕ ПОКУПОК ДЛЯ РЕДАКТИРОВАНИЯ
    @Override
    public void changeTabToEditBLI(BuyListItem listItem) {
        Utils.currentFragment = IntConst.FRAGMENT_CREATE_BLI;
        setTitleEditBLI();

        containerCreateBLI.setTranslationX(displayWidth);
        fragmentCreateBuyListItem.setEditMode(listItem);
        fm.beginTransaction().replace(R.id.layCreateBLI, fragmentCreateBuyListItem).commit();


    }

    @Override
    public void openFragmentCreateBLI(){
        if(animateSlideFragmentClose.isStarted()){
            animateSlideFragmentClose.cancel();
        }
        if(!animateSlideFragmentOpen.isStarted()) {
            animateSlideFragmentOpen = ObjectAnimator.ofFloat(containerCreateBLI, "translationX", containerCreateBLI.getTranslationX(), 0);
            animateSlideFragmentOpen.setDuration(300);
            animateSlideFragmentOpen.start();
        }
    }

    public void closeFragmentCreateBLI(){
        if(animateSlideFragmentOpen.isStarted()){
            animateSlideFragmentOpen.cancel();
        }
        if(!animateSlideFragmentClose.isStarted()) {
            animateSlideFragmentClose = ObjectAnimator.ofFloat(containerCreateBLI, "translationX", containerCreateBLI.getTranslationX(), displayWidth);
            animateSlideFragmentClose.setDuration(300);
            animateSlideFragmentClose.start();
        }
        animateSlideFragmentClose.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                fm.beginTransaction().remove(fragmentCreateBuyListItem).commit();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    //РЕДАКТИРОВАНИЕ ТОВАРА ЗАВЕРШЕНО
    @Override
     public void editFinished() {
        Utils.currentFragment = IntConst.FRAGMENT_BL;
        setTitleGoodsList();
        buyListFragment.sortData();
        buyListFragment.updateBtmPanelTextInfo();
        closeFragmentCreateBLI();
    }



    //НАЖАТИЕ НА КНОПКУ СОЗДАТЬ ТОВАР
    @Override
    public void onBtnCreateNewPressed() {
        setTitleCreateBLI();
        Utils.currentFragment = IntConst.FRAGMENT_CREATE_BLI;
        containerCreateBLI.setTranslationX(displayWidth);
        fm.beginTransaction().replace(R.id.layCreateBLI, fragmentCreateBuyListItem).commit();
        if(animateSlideFragmentClose.isStarted()){
            animateSlideFragmentClose.cancel();
        }
        if(!animateSlideFragmentOpen.isStarted()) {
            animateSlideFragmentOpen = ObjectAnimator.ofFloat(containerCreateBLI, "translationX", containerCreateBLI.getTranslationX(), 0);
            animateSlideFragmentOpen.setDuration(300);
            animateSlideFragmentOpen.start();
        }

    }


    //НАЖАТИЕ НА ПУНКТ РЕДАКТИРОВАТЬ СПИСОК ПОКУПОК
    public void openEditBuyList(BuyList bl, int groupPos, int childPos){

        setTitleEditBL();
        Utils.currentFragment = IntConst.FRAGMENT_EDIT_BL;
        fragmentCreateBuyList.editItem(bl, groupPos, childPos);
        fm.beginTransaction().replace(R.id.layCreateBL, fragmentCreateBuyList).commit();

        if(animateSlideFragmentClose.isStarted()){
            animateSlideFragmentClose.cancel();
        }
        if(!animateSlideFragmentOpen.isStarted()) {
            animateSlideFragmentOpen = ObjectAnimator.ofFloat(containerCreateBL, "translationX", containerCreateBL.getTranslationX(), 0);
            animateSlideFragmentOpen.setDuration(300);
            animateSlideFragmentOpen.start();
        }

        animateSlideFragmentOpen.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                fragmentCreateBuyList.showSoftKeyboard();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }


    //ФУНКЦИЯ, ВЫЗЫВАЕМАЯ ПРИ ЗАВЕРШЕНИИ СОЗДАНИЯ ИЛИ РЕДАКТИРОВАНИЯ СПИСКА ПОКУПОК
    @Override
    public void changeTab(BuyList buyList, boolean add, boolean hadChange, int groupPos, int childPos) {
        if(add){
            mainPageFragment.hideEmptyTV();
            mainPageFragment.addNewItem(buyList);
        } else {
            if(hadChange) {
                boolean old = buyList.type == 2 ? true : false;
                mainPageFragment.invalidate(old, groupPos, childPos, hadChange);
            } else {
                mainPageFragment.invalidate(false, groupPos, childPos, false);
            }
        }
        if(animateSlideFragmentOpen.isStarted()){
            animateSlideFragmentOpen.cancel();
        }
        if(!animateSlideFragmentClose.isStarted()) {
            animateSlideFragmentClose = ObjectAnimator.ofFloat(containerCreateBL, "translationX", containerCreateBL.getTranslationX(), -displayWidth);
            animateSlideFragmentClose.setDuration(300);
            animateSlideFragmentClose.start();
        }

        animateSlideFragmentClose.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                fm.beginTransaction().remove(fragmentCreateBuyList).commit();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        Utils.currentFragment = IntConst.FRAGMENT_MAIN;
        setTitleMainPage();

    }


    @Override
    public void categoryMustBeNotified() {
        buyListFragment.notifyCategoryHadAdded();
    }

    public void setTitleCreateBL(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_create_bl)+"</b></font>"));
    }
    public void setTitleCreateBLI(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_create_bli)+"</b></font>"));
    }
    public void setTitleEditBL(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_edit_bl)+"</b></font>"));
    }
    public void setTitleEditBLI(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_edit_bli)+"</b></font>"));
    }
    public void setTitleDeleteMode(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_preference_delete_mode)+"</b></font>"));
    }
    public void setTitleMainPage(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_main_page)+"</b></font>"));
    }
    public void setTitlePreferences(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_preference)+"</b></font>"));
    }
    public void setTitlePrefGoods(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_preference_goods)+"</b></font>"));
    }
    public void setTitlePrefCategories(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#"+Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2,8)
                                +"'><b>"+this.getResources().getString(R.string.title_preference_categories)+"</b></font>"));
    }
    public void setTitleGoodsList(){
        getSupportActionBar().setTitle(
                Html.fromHtml(
                        "<font color='#" + Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2, 8)
                                + "'><b>" + this.getResources().getString(R.string.title_buy_list) + "</b></font>"));
    }





    @Override
    public void onBackPressed() {
        if(!Utils.loaderStarted) {
            if (Utils.deleteMode) {
                mainPageFragment.offDeleteMode();
                switch (Utils.levelPosition) {
                    case IntConst.CATEGORIES:
                        setTitlePrefCategories();
                        break;
                    case IntConst.GOODS:
                        setTitlePrefGoods();
                        break;
                }
                return;
            }

            if (Utils.buyListItemDeleteMode) {
                setTitleGoodsList();
                buyListFragment.cancelDeleteMode();
                return;
            }

            if (Utils.isPrefLvlSecondOpened) {
                Utils.currentFragment = IntConst.FRAGMENT_PREF;
                mainPageFragment.closePrefSecondLevel();
                setTitlePreferences();
                return;
            }

            if (Utils.preferencesVisible) {
                mainPageFragment.closePreference();
                Utils.currentFragment = IntConst.FRAGMENT_MAIN;
                setTitleMainPage();
                return;
            }

            switch (Utils.currentFragment) {
                case IntConst.FRAGMENT_EDIT_BL:
                    if (back_pressed + 2000 > System.currentTimeMillis()) {
                        fragmentCreateBuyList.clearAll();
                        if (animateSlideFragmentOpen.isStarted()) {
                            animateSlideFragmentOpen.cancel();
                        }
                        if (!animateSlideFragmentClose.isStarted()) {
                            animateSlideFragmentClose = ObjectAnimator.ofFloat(containerCreateBL, "translationX", containerCreateBL.getTranslationX(), -displayWidth);
                            animateSlideFragmentClose.setDuration(300);
                            animateSlideFragmentClose.start();
                        }

                        animateSlideFragmentClose.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                fm.beginTransaction().remove(fragmentCreateBuyList).commit();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });

                        setTitleMainPage();
                        Utils.currentFragment = IntConst.FRAGMENT_MAIN;

                    } else {
                        // Toast.makeText(getBaseContext(), getApplicationContext()
                        //         .getString(R.string.double_press_exit_create), Toast.LENGTH_SHORT).show();
                    }
                    back_pressed = System.currentTimeMillis();
                case IntConst.FRAGMENT_CREATE_BL:
                    if (back_pressed + 2000 > System.currentTimeMillis()) {
                        fragmentCreateBuyList.clearAll();
                        if (animateSlideFragmentOpen.isStarted()) {
                            animateSlideFragmentOpen.cancel();
                        }
                        if (!animateSlideFragmentClose.isStarted()) {
                            animateSlideFragmentClose = ObjectAnimator.ofFloat(containerCreateBL, "translationX", containerCreateBL.getTranslationX(), -displayWidth);
                            animateSlideFragmentClose.setDuration(300);
                            animateSlideFragmentClose.start();
                        }

                        animateSlideFragmentClose.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                fm.beginTransaction().remove(fragmentCreateBuyList).commit();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });

                        setTitleMainPage();
                        Utils.currentFragment = IntConst.FRAGMENT_MAIN;

                    } else {
                        // Toast.makeText(getBaseContext(), getApplicationContext()
                        //         .getString(R.string.double_press_exit_create), Toast.LENGTH_SHORT).show();
                    }
                    back_pressed = System.currentTimeMillis();

                    break;
                case IntConst.FRAGMENT_MAIN:
                    if (back_pressed + 2000 > System.currentTimeMillis()) {
                        Utils.hasInit = false;
                        super.onBackPressed();
                    } else {
                        Toast.makeText(getBaseContext(), getApplicationContext()
                                .getString(R.string.double_press_exit), Toast.LENGTH_SHORT).show();
                    }
                    back_pressed = System.currentTimeMillis();
                    break;
                case IntConst.FRAGMENT_BL:
                    Utils.currentFragment = IntConst.FRAGMENT_MAIN;
                    setTitleMainPage();
                    mainPageFragment.notifyDataSetChanged();
                    if (animateSlideFragmentOpen.isStarted()) {
                        animateSlideFragmentOpen.cancel();
                    }
                    if (!animateSlideFragmentClose.isStarted()) {
                        animateSlideFragmentClose = ObjectAnimator.ofFloat(containerBL, "translationX", containerBL.getTranslationX(), displayWidth);
                        animateSlideFragmentClose.setDuration(300);
                        animateSlideFragmentClose.start();
                    }
                    animateSlideFragmentClose.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            fm.beginTransaction().remove(buyListFragment).commit();
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });


                    break;
                case IntConst.FRAGMENT_CREATE_BLI:
                    if (!Utils.buyListItemEditStarted) {
                        Utils.currentFragment = IntConst.FRAGMENT_BL;
                        setTitleGoodsList();
                        fragmentCreateBuyListItem.clearAll();
                        closeFragmentCreateBLI();
                    } else {
                        new MyAlertDialog(IntConst.ID_DIALOG_EDIT_BLI).show(getSupportFragmentManager(), "ID_DIALOG_EDIT_BLI");
                    }
                    break;
            }
        }
    }

    @Override
    public void onBackNeedPress() {

    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        DBHelper.close();
    }

    // TODO MyAlertDialog
    public class MyAlertDialog extends DialogFragment {

        private int dialogId;
        public MyAlertDialog(){}
        public MyAlertDialog(int dialogId){
            this.dialogId = dialogId;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.alert_dialog, container, false);

            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.alertDialogRoot);
            TextView title = (TextView) v.findViewById(R.id.alertDialogTitle);
            Button accept = (Button) v.findViewById(R.id.alertDialogAccept);
            Button cancel = (Button) v.findViewById(R.id.alertDialogCancel);

            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            if(IntConst.ID_DIALOG_CREATE_BL == dialogId){
                title.setText(R.string.create_buy_list_close);
            } else {
                title.setText(R.string.mp_popup_edit_bli);
            }

            title.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefTitle);

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());



            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(IntConst.ID_DIALOG_CREATE_BL == dialogId){
                        if(!Utils.buyListEditStarted) {
                            fm.beginTransaction().remove(fragmentCreateBuyList).commit();
                            Utils.currentFragment = IntConst.FRAGMENT_MAIN;
                            setTitleMainPage();
                        }
                    }

                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (dialogId){
                        case IntConst.ID_DIALOG_CREATE_BL:
                            Utils.buyListCreateStarted = false;
                            fragmentCreateBuyList.clearAll();
                            fm.beginTransaction().remove(fragmentCreateBuyList).commit();
                            Utils.currentFragment = IntConst.FRAGMENT_MAIN;
                            setTitleMainPage();
                        case IntConst.ID_DIALOG_EDIT_BL:
                            Utils.buyListEditStarted = false;
                            fragmentCreateBuyList.clearAll();
                            fm.beginTransaction().remove(fragmentCreateBuyList).commit();
                            Utils.currentFragment = IntConst.FRAGMENT_MAIN;
                            setTitleMainPage();
                            break;
                        case IntConst.ID_DIALOG_EDIT_BLI:
                            Utils.currentFragment = IntConst.FRAGMENT_BL;
                            Utils.buyListItemEditStarted = false;
                            setTitleGoodsList();
                            fragmentCreateBuyListItem.clearAll();
                            closeFragmentCreateBLI();
                            break;
                    }



                    getDialog().dismiss();
                }
            });

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }


}
