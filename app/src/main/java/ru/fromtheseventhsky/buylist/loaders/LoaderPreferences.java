package ru.fromtheseventhsky.buylist.loaders;


import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.AsyncTaskLoader;

import ru.fromtheseventhsky.buylist.utils.DBHelper;
import ru.fromtheseventhsky.buylist.utils.Utils;

public class LoaderPreferences extends AsyncTaskLoader {

    int id;
    int number;
    Context context;

    public LoaderPreferences(Context context, int id, int number) {
        super(context);
        this.context = context;
        this.id = id;
        this.number = number;
    }

    public void removeCategory(){
        SQLiteDatabase db = DBHelper.getDb();
        db.delete("categories","id = ?", new String[]{""+Utils.goodsCategories.get(number).getId()});
    }


    public void addCategory(){
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        cv.put("name", Utils.goodsCategories.get(Utils.goodsCategories.size()-1).getName());
        cv.put("sort", Utils.goodsCategories.get(Utils.goodsCategories.size()-1).getSort());

        db.insert("categories", null, cv);
    }

    @Override
    public Object loadInBackground() {
        return null;
    }
}
