package ru.fromtheseventhsky.buylist.loaders;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.Calendar;

import ru.fromtheseventhsky.buylist.utils.BuyListItem;
import ru.fromtheseventhsky.buylist.utils.ColorCircleContainer;
import ru.fromtheseventhsky.buylist.utils.DBHelper;
import ru.fromtheseventhsky.buylist.utils.GoodsCategory;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.ProductInfo;
import ru.fromtheseventhsky.buylist.utils.TwiceColorCircleContainer;
import ru.fromtheseventhsky.buylist.utils.Utils;

public class LoaderCreateBuyListItem extends AsyncTaskLoader<Long> {

    public LoaderCreateBuyListItem(Context context) {
        super(context);
    }

    Context context;
    String name;
    String comment;
    long date;
    int colorText;
    int colorBck;
    int length;
    Bundle b;
    long result;
    boolean alreadyHave;

    ProductInfo productInfo;
    int productInfoPos;
    ArrayList<Integer> deleteList;

    BuyListItem buyListItem;

    int type;
    int id;
    int color;
    int colorSecond;

    boolean photoChanged;

    long idGoods;
    long idBuyList;

    public LoaderCreateBuyListItem(Context context, long idBuyList, String name, String comment, long date, int colorBck, int colorText, int length, int id) {
        super(context);
        this.idBuyList = idBuyList;
        this.context = context;
        this.name = name;
        this.comment = comment;
        this.date = date;
        this.colorText = colorText;
        this.colorBck = colorBck;
        this.length = length;
        this.id = id;
        result = -1L;
    }

    public LoaderCreateBuyListItem(Context context, BuyListItem buyListItem, long idBuyList, int id) {
        super(context);
        this.buyListItem = buyListItem;
        this.idBuyList = idBuyList;
        this.context = context;
        this.id = id;
    }

    public LoaderCreateBuyListItem(Context context, BuyListItem buyListItem, boolean photoChanged, long idBuyList, int id) {
        super(context);
        this.buyListItem = buyListItem;
        this.idBuyList = idBuyList;
        this.context = context;
        this.photoChanged = photoChanged;
        this.id = id;
    }

    public void uploadBuyListItem(){
        SQLiteDatabase db = DBHelper.getDb();

        ContentValues cv = new ContentValues();
        cv.put("id_buy_list", idBuyList);
        cv.put("id_goods", buyListItem.getId());
        cv.put("comment", buyListItem.getComment());
        cv.put("type", buyListItem.isBought());
        cv.put("colorBck", buyListItem.getColorBck());
        cv.put("colorTxt", buyListItem.getColorTxt());
        cv.put("colorTxt", buyListItem.getColorTxt());
        cv.put("unitCount", buyListItem.getWeight());
        cv.put("dateAdd", buyListItem.getDateAdd());
        cv.put("photo", buyListItem.getPath());
        db.insert("relations", null, cv);

        cv.clear();
        cv.put("bought", Utils.boughtInCurrentList);
        cv.put("cost", Utils.costInCurrentList);
        cv.put("positions", Utils.positionsInCurrentList);
        db.update("buy_list",cv, "id = ?", new String[]{"" + idBuyList});

    }

    public void updateBuyListItem(){
        SQLiteDatabase db = DBHelper.getDb();

        ContentValues cv = new ContentValues();
        cv.put("comment", buyListItem.getComment());
        cv.put("type", buyListItem.isBought());
        cv.put("colorBck", buyListItem.getColorBck());
        cv.put("colorTxt", buyListItem.getColorTxt());
        cv.put("unitCount", buyListItem.getWeight());
        cv.put("dateAdd", buyListItem.getDateAdd());
        if(photoChanged) {
            cv.put("photo", buyListItem.getPath());
        }
        db.update("relations", cv, "id_buy_list = ? and id_goods = ?", new String[]{String.valueOf(idBuyList), String.valueOf(buyListItem.getId())});

        cv.clear();
        cv.put("cost", buyListItem.getCost());
        cv.put("unit", buyListItem.getWeightType());
        cv.put("category", buyListItem.getCategory());
        cv.put("categoryId", buyListItem.getCategoryId());
        db.update("goods", cv, "id = ? ", new String[]{String.valueOf(buyListItem.getId())});

        cv.clear();
        cv.put("cost", Utils.costInCurrentList);
        db.update("buy_list",cv, "id = ?", new String[]{"" + idBuyList});

    }

    public void editBuyListItem(){
        SQLiteDatabase db = DBHelper.getDb();

        ContentValues cv = new ContentValues();
        cv.put("id_buy_list", idBuyList);
        cv.put("id_goods", buyListItem.getId());
        cv.put("comment", buyListItem.getComment());
        cv.put("type", buyListItem.isBought());
        cv.put("colorBck", buyListItem.getColorBck());
        cv.put("colorTxt", buyListItem.getColorTxt());
        cv.put("unitCount", buyListItem.getWeight());

        db.insert("relations", null, cv);
    }

    public LoaderCreateBuyListItem(Context context, int color, int colorSecond, int type, int id) {
        super(context);
        this.id = id;
        this.color = color;
        this.colorSecond = colorSecond;
        this.type = type;
    }



    public LoaderCreateBuyListItem(Context context, int id) {
        super(context);
        this.id = id;
    }

    public LoaderCreateBuyListItem(Context context, long idGoods, long idBuyList, int id) {
        super(context);
        this.id = id;
        this.idBuyList = idBuyList;
        this.idGoods = idGoods;
    }

    public void uploadColor(){
        SQLiteDatabase db = DBHelper.getDb();

        if(type!=3) {
            ContentValues cv = new ContentValues();
            cv.put("color", color);
            cv.put("type", type);

            db.insert("colors", null, cv);
        } else {
            ContentValues cv = new ContentValues();
            cv.put("colorFirst", color);
            cv.put("colorSecond", colorSecond);

            db.insert("colors_temp", null, cv);
        }
    }
    public void getColorArray(){
        SQLiteDatabase db = DBHelper.getDb();
        Cursor cursor = db.query("colors", null, null, null, null, null, null);

        cursor.moveToFirst();

        int type;
        int color;
        int color2;

        if(cursor.getCount()>0){
            do{
                type = cursor.getInt(cursor.getColumnIndex("type"));
                color = cursor.getInt(cursor.getColumnIndex("color"));
                if(type==1)
                    Utils.customBck.add(new ColorCircleContainer(false, color, false));
                else
                    Utils.customTxt.add(new ColorCircleContainer(false, color, false));
            } while(cursor.moveToNext());
        }
        cursor.close();

        cursor = db.query("colors_temp", null, null, null, null, null, null);

        cursor.moveToFirst();

        if(cursor.getCount()>0){
            do{
                color2 = cursor.getInt(cursor.getColumnIndex("colorSecond"));
                color = cursor.getInt(cursor.getColumnIndex("colorFirst"));
                Utils.customTemp.add(new TwiceColorCircleContainer(color, color2, false));
            } while(cursor.moveToNext());
        }

    }
    public void removeColor(){
        SQLiteDatabase db = DBHelper.getDb();
        if(type == 3){
            db.delete("colors_temp", "colorFirst = ? and colorSecond = ?", new String[]{ String.valueOf(color), String.valueOf(colorSecond)});
        } else {
            db.delete("colors", "color = ? and type = ?", new String[]{ String.valueOf(color), String.valueOf(type)});
        }
    }


    public Long uploadCreateBuyListItem(){

        SQLiteDatabase db = DBHelper.getDb();
        long date_end = -1;
        if(length!=-1)
            date_end = Calendar.getInstance().getTimeInMillis()+length*86400000;
        else
            date_end = Calendar.getInstance().getTimeInMillis()+length*86400000*1000;

        long id_goods = Utils.idOfSelectedProduct;
        long id_buy_list = Utils.idOfCalledBuyList;
        ContentValues cv = new ContentValues();
        if(!alreadyHave) {

            cv.put("name", name);
            cv.put("cost", comment);
            cv.put("currency", date);
            cv.put("unit", colorText);
            cv.put("unitCount", colorBck);
            id_goods = db.insert("goods", null, cv);
        }
        cv.clear();

        cv.put("id_buy_list", id_buy_list);
        cv.put("id_goods", id_goods);
        cv.put("comment", date);
        cv.put("type", colorText);
        cv.put("colorBck", colorBck);
        cv.put("colorTxt", colorBck);

        Long result = db.insert("relations", null, cv);
        Utils.idOfSelectedProduct = -1;
        return result;
    }


    //CALL AFTER PUSH "CREATE BUY LIST" BUTTON
    public Long uploadCreateBuyList(){

        SQLiteDatabase db = DBHelper.getDb();
        long date_end = -1;
        if(length!=-1)
            date_end = date+length*86400000;
        else
            date_end = -1;

        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("comment", comment);
        cv.put("date_start", date);
        cv.put("colorTxt", colorText);
        cv.put("colorBck", colorBck);
        cv.put("date_end", date_end);
        cv.put("type", 1);

        cv.put("bought", 0);
        cv.put("cost", 0);
        cv.put("positions", 0);

        Long result = db.insert("buy_list", null, cv);

        return result;
    }


    //CALL AFTER PUSH "EDIT BUY LIST" BUTTON
    public void uploadEditBuyList(){

        SQLiteDatabase db = DBHelper.getDb();
        long date_end = -1;
        if(length!=-1)
            date_end = date+length*86400000;

        ContentValues cv = new ContentValues();
        cv.put("name", name);
        cv.put("comment", comment);
        cv.put("date_start", date);
        cv.put("colorTxt", colorText);
        cv.put("colorBck", colorBck);
        cv.put("date_end", date_end);

        db.update("buy_list",cv, "id = ?", new String[]{"" + idBuyList});
    }


    public LoaderCreateBuyListItem(Context context, ArrayList<Integer> deleteList, int id) {
        super(context);
        this.id = id;
        this.deleteList = deleteList;
    }
    public LoaderCreateBuyListItem(Context context, int productInfoPos, int id) {
        super(context);
        this.id = id;
        this.productInfoPos = productInfoPos;
    }

    public void addCommodity(){//Commodity - товар
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        productInfo = Utils.availableProducts.get(Utils.productNames.get(Utils.productNames.size()-1));
        cv.put("name", productInfo.getName());
        cv.put("cost", productInfo.getCost());
        cv.put("unit", productInfo.getUnit());
        cv.put("category", productInfo.getCategory());
        cv.put("categoryId", productInfo.getCategoryId());
        cv.put("lastUseDate", productInfo.getLastUsed());
        Utils.availableProducts.get(productInfo.getName()).setId((int) db.insert("goods", null, cv));
    }

    public void addCommodityToBL(){//Commodity - товар
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        productInfo = Utils.availableProducts.get(Utils.productNames.get(Utils.productNames.size()-1));
        cv.put("name", productInfo.getName());
        cv.put("cost", productInfo.getCost());
        cv.put("unit", productInfo.getUnit());
        cv.put("category", productInfo.getCategory());
        cv.put("categoryId", productInfo.getCategoryId());
        cv.put("lastUseDate", productInfo.getLastUsed());
        Utils.availableProducts.get(productInfo.getName()).setId((int) db.insert("relations", null, cv));
    }


    public void removeCommodity(){//Commodity - товар
        SQLiteDatabase db = DBHelper.getDb();
        for (int i = deleteList.size()-1; i >= 0; i--) {
            int idDel = Utils.availableProducts.get(Utils.productNames.get(deleteList.get(i))).getId();
            Utils.availableProducts.remove(Utils.productNames.get(deleteList.get(i)));
            Utils.productNames.remove((int)deleteList.get(i));
            db.delete("goods", "id = ?", new String[]{"" + idDel});
        }

    }
    public void updateCommodityDate(){//Commodity - товар
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        productInfo = Utils.availableProducts.get(Utils.productNames.get(productInfoPos));
        cv.put("lastUseDate", productInfo.getLastUsed());
        db.update("goods", cv, "lastUseDate = ?",new String[] { ""+productInfo.getLastUsed() });
    }
    public void updateCommodity(){//Commodity - товар
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        productInfo = Utils.availableProducts.get(Utils.productNames.get(productInfoPos));
        cv.put("name", productInfo.getName());
        cv.put("cost", productInfo.getCost());
        cv.put("unit", productInfo.getUnit());
        cv.put("category", productInfo.getCategory());
        cv.put("categoryId", productInfo.getCategoryId());
        cv.put("lastUseDate", productInfo.getLastUsed());
        db.update("goods", cv, "id = ?",new String[] { ""+productInfo.getId() });
    }





    public void addCategory(){
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        cv.put("name", Utils.goodsCategories.get(Utils.goodsCategories.size()-2).getName());
        cv.put("sort", Utils.goodsCategories.get(Utils.goodsCategories.size()-2).getSort());
        Utils.goodsCategories.get(Utils.goodsCategories.size()-2).setId((int) db.insert("categories", null, cv));
    }
    public void removeCategory(){
        SQLiteDatabase db = DBHelper.getDb();
        for (int i = deleteList.size()-1; i >= 0; i--) {
            int idDel = Utils.goodsCategories.get(deleteList.get(i)).getId();
            Utils.goodsCategories.remove((int)deleteList.get(i));
            db.delete("categories", "id = ?", new String[]{"" + idDel});
            ContentValues cv = new ContentValues();
            cv.put("category", 0);
            cv.put("categoryId", -1);
            db.update("goods", cv, "categoryId = ?", new String[]{"" + idDel});
        }

    }

    public LoaderCreateBuyListItem(Context context, Bundle b, int id) {
        super(context);
        this.id = id;
        this.b = b;
    }


    public void updateCategoryName(){
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        cv.put("name", Utils.goodsCategories.get(b.getInt("selectedPosition")).getName());
        db.update("categories", cv, "id = ?", new String[]{"" + Utils.goodsCategories.get(b.getInt("selectedPosition")).getId()});
    }



    public void updateCategorySort(){
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        int gcUp, gcBottom;
        gcUp=b.getInt("gcUp");
        gcBottom = b.getInt("gcBottom");

        boolean isIncrement = b.getBoolean("dimUp");
        int increment;
        if(isIncrement){
            increment = -1;
        } else {
            increment = 1;
        }
        GoodsCategory gc;
        gc = Utils.goodsCategories.get(gcBottom);
        cv.put("sort", gc.getSort());
        db.update("categories", cv, "id = ?", new String[]{"" + gc.getId()});
        Cursor c = db.query("goods", null, "categoryId = ?", new String[]{"" + gc.getId()}, null, null, null);//БРАТЬ ЗНАЧЕНИЯ ИЗ ТОВАРОВИ ИНКРЕМЕНТИРОВТЬ ИЛИ ДЕКРЕМЕНТИРОВАТЬ
        cv.clear();
        c.moveToFirst();
        if(c.getCount()>0) {
            cv.put("category", c.getInt(c.getColumnIndex("category")) + increment);
            db.update("goods", cv, "categoryId = ?", new String[]{"" + gc.getId()});
        }
        cv.clear();
        gc = Utils.goodsCategories.get(gcUp);
        cv.put("sort", gc.getSort());
        db.update("categories", cv, "id = ?", new String[]{"" + gc.getId()});
        c = db.query("goods", null, "categoryId = ?", new String[]{"" + gc.getId()}, null, null, null);//БРАТЬ ЗНАЧЕНИЯ ИЗ ТОВАРОВИ ИНКРЕМЕНТИРОВТЬ ИЛИ ДЕКРЕМЕНТИРОВАТЬ
        cv.clear();
        c.moveToFirst();

        if(c.getCount()>0) {
            cv.put("category", c.getInt(c.getColumnIndex("category")) - increment);
            db.update("goods", cv, "categoryId = ?", new String[]{"" + gc.getId()});
        }
    }




    @Override
    public void deliverResult(Long data) {
        if (isReset()) {
            // The Loader has been reset; ignore the result and invalidate the data.
            return;
        }

        if (isStarted()) {
            // If the Loader is in a started state, deliver the results to the
            // client. The superclass method does this for us.
            super.deliverResult(data);
        }

    }

    @Override
     public Long loadInBackground() {

        switch(id){
            case IntConst.LOADER_CREATE_BL:
                result = uploadCreateBuyList();
                break;
            case IntConst.LOADER_UPDATE_BL:
                uploadEditBuyList();
                break;
            case IntConst.LOADER_GET_COLOR:
                getColorArray();
                break;
            case IntConst.LOADER_CREATE_COLOR:
                uploadColor();
                break;
            case IntConst.LOADER_DELETE_COLOR:
                removeColor();
                break;
            case IntConst.LOADER_ADD_COMMODITY:
                addCommodity();
                break;
            case IntConst.LOADER_REMOVE_COMMODITY:
                removeCommodity();
                break;
            case IntConst.LOADER_UPDATE_COMMODITY:
                updateCommodity();
                break;
            case IntConst.LOADER_UPDATE_COMMODITY_LAST_USED_DATE:
                updateCommodityDate();
                break;
            case IntConst.LOADER_UPDATE_CATEGORY_NAME:
                updateCategoryName();
                break;
            case IntConst.LOADER_UPDATE_CATEGORY_SORT:
                updateCategorySort();
                break;
            case IntConst.LOADER_ADD_CATEGORY:
                addCategory();
                break;
            case IntConst.LOADER_REMOVE_CATEGORY:
                removeCategory();
                break;

            case IntConst.LOADER_CREATE_BLI:
                uploadBuyListItem();
                break;
            case IntConst.LOADER_UPDATE_BLI:
                updateBuyListItem();
                break;

        }
        return result;
    }
}
