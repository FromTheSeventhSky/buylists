package ru.fromtheseventhsky.buylist.loaders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.HashMap;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyListItem;
import ru.fromtheseventhsky.buylist.utils.DBHelper;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class LoaderBuyList extends AsyncTaskLoader {

    private long id;
    private int LoaderId;
    private int commodityId;
    private ArrayList<Integer> deleteList;

    public LoaderBuyList(Context context, long id, int LoaderId) {
        super(context);
        this.id = id;
        this.LoaderId = LoaderId;
    }
    public LoaderBuyList(Context context, long id, int commodityId, int LoaderId) {
        super(context);
        this.id = id;
        this.LoaderId = LoaderId;
        this.commodityId = commodityId;
    }

    public LoaderBuyList(Context context, long id, ArrayList<Integer> deleteList, int LoaderId) {
        super(context);
        this.id = id;
        this.LoaderId = LoaderId;
        this.deleteList = deleteList;
    }

    public void removeSelected(){
        SQLiteDatabase db = DBHelper.getDb();

        String whereClause = "id_buy_list = ? and id_goods = ?";
        String[] whereArgs = {String.valueOf(id), ""};

        int tmpId;
        for (int i = 0; i < deleteList.size(); i++) {
            tmpId = deleteList.get(i);
            whereArgs[1] = String.valueOf(tmpId);
            db.delete("relations", whereClause, whereArgs);
        }

        ContentValues cv = new ContentValues();
        cv.put("positions", Utils.positionsInCurrentList);
        cv.put("cost", Utils.costInCurrentList);
        cv.put("bought", Utils.boughtInCurrentList);
        db.update("buy_list",cv, "id = ?", new String[]{"" + id});
    }


    public void moveFromBought(){
        SQLiteDatabase db = DBHelper.getDb();

        String whereClause = "id_buy_list = ? and id_goods = ?";
        String[] whereArgs = {String.valueOf(id), String.valueOf(commodityId)};

        ContentValues cv = new ContentValues();
        cv.put("type", false);

        db.update("relations", cv, whereClause, whereArgs);

        cv.clear();
        Utils.boughtInCurrentList--;
        cv.put("bought", Utils.boughtInCurrentList);
        db.update("buy_list",cv, "id = ?", new String[]{"" + id});
    }
    public void moveToBought(){
        SQLiteDatabase db = DBHelper.getDb();

        String whereClause = "id_buy_list = ? and id_goods = ?";
        String[] whereArgs = {String.valueOf(id), String.valueOf(commodityId)};

        ContentValues cv = new ContentValues();
        cv.put("type", true);

        db.update("relations", cv, whereClause, whereArgs);

        cv.clear();
        Utils.boughtInCurrentList++;
        cv.put("bought", Utils.boughtInCurrentList);
        db.update("buy_list",cv, "id = ?", new String[]{"" + id});
    }

    public HashMap<Integer, ArrayList<BuyListItem>> getList(){
        SQLiteDatabase db = DBHelper.getDb();
        String selectionGoods = "id = ?";
        String[] selectionArgs = {String.valueOf(id)};

        String selection = "id_buy_list = ?";

        Cursor relations = db.query("relations", null, selection, selectionArgs, null, null, null);


        relations.moveToFirst();


        HashMap<Integer, ArrayList<BuyListItem>> buyListExp = new HashMap<>();

        if(AppPreferences.categoryAvailable) {
            for (int i = 0; i < Utils.goodsCategories.size(); i++) {
                buyListExp.put(Utils.goodsCategories.get(i).getId(), new ArrayList<BuyListItem>());
                Utils.arrListsExpanded.put(Utils.goodsCategories.get(i).getId(), false);
            }
            buyListExp.put(IntConst.ID_BOUGHT_CAT_ON, new ArrayList<BuyListItem>());
            Utils.arrListsExpanded.put(IntConst.ID_BOUGHT_CAT_ON, false);
        } else {
            buyListExp.put(IntConst.ID_NOT_BOUGHT_CAT_OFF, new ArrayList<BuyListItem>());
            Utils.arrListsExpanded.put(IntConst.ID_NOT_BOUGHT_CAT_OFF, false);
            buyListExp.put(IntConst.ID_BOUGHT_CAT_OFF, new ArrayList<BuyListItem>());
            Utils.arrListsExpanded.put(IntConst.ID_BOUGHT_CAT_OFF, false);
        }


        if(relations.getCount()>0){
            do{


                BuyListItem bliTmp = new BuyListItem();
                bliTmp.setId(relations.getInt(relations.getColumnIndex("id_goods")));

                String[] selectionArgsGoods = {String.valueOf(bliTmp.getId())};

                Cursor goods = db.query("goods", null, selectionGoods, selectionArgsGoods, null, null, null);

                goods.moveToFirst();

                Utils.availableProductsInCurrentList.put(goods.getString(goods.getColumnIndex("name")).toLowerCase(), null);
                bliTmp.setDateAdd(relations.getLong(relations.getColumnIndex("dateAdd")));
                bliTmp.setName(goods.getString(goods.getColumnIndex("name")));
                bliTmp.setComment(relations.getString(relations.getColumnIndex("comment")));
                bliTmp.setCost(goods.getInt(goods.getColumnIndex("cost")));
                bliTmp.setWeight(relations.getInt(relations.getColumnIndex("unitCount")));
                bliTmp.setWeightType(goods.getInt(goods.getColumnIndex("unit")));
                bliTmp.setCategory(goods.getInt(goods.getColumnIndex("category")));
                bliTmp.setCategoryId(goods.getInt(goods.getColumnIndex("categoryId")));
                bliTmp.setBought(relations.getInt(relations.getColumnIndex("type"))==0 ? false : true);
                bliTmp.setColorBck(relations.getInt(relations.getColumnIndex("colorBck")));
                bliTmp.setColorTxt(relations.getInt(relations.getColumnIndex("colorTxt")));
                bliTmp.setSync(false);
                bliTmp.setAccountId(1);

                bliTmp.setPath(relations.getString(relations.getColumnIndex("photo")));

                if(!bliTmp.getPath().equalsIgnoreCase("")) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(bliTmp.getPath(), options);
                    bitmap = Bitmap.createScaledBitmap(bitmap, (int) getContext().getResources().getDimension(R.dimen.Photo_prev_size), (int) getContext().getResources().getDimension(R.dimen.Photo_prev_size), true);
                    bliTmp.setBmpPrev(bitmap);
                } else {
                    bliTmp.setBmpPrev(null);
                }

                if(!bliTmp.isBought()) {
                    if (AppPreferences.categoryAvailable) {
                        buyListExp.get(bliTmp.getCategoryId()).add(bliTmp);
                    } else {
                        buyListExp.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).add(bliTmp);
                    }
                } else {
                    if (AppPreferences.categoryAvailable) {
                        buyListExp.get(IntConst.ID_BOUGHT_CAT_ON).add(bliTmp);
                    } else {
                        buyListExp.get(IntConst.ID_BOUGHT_CAT_OFF).add(bliTmp);
                    }
                }

                goods.close();

            } while(relations.moveToNext());


        }


        relations.close();

        return buyListExp;
    }



    @Override
    public HashMap<Integer, ArrayList<BuyListItem>> loadInBackground() {
        switch (LoaderId){
            case IntConst.LOADER_GET_BLI:
                return getList();
            case IntConst.LOADER_SET_BOUGHT_TRUE:
                moveToBought();
                break;
            case IntConst.LOADER_SET_BOUGHT_FALSE:
                moveFromBought();
                break;
            case IntConst.LOADER_REMOVE_COMMODITY:
                removeSelected();
                break;
        }
        return null;
    }
}
