package ru.fromtheseventhsky.buylist.loaders;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.HashMap;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyList;
import ru.fromtheseventhsky.buylist.utils.BuyListItem;
import ru.fromtheseventhsky.buylist.utils.DBHelper;
import ru.fromtheseventhsky.buylist.utils.GoodsCategory;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.ProductInfo;
import ru.fromtheseventhsky.buylist.utils.Utils;

public class LoaderMainPage extends AsyncTaskLoader {

    ArrayList<BuyList> buyListToUpdate;
    int id;
    Context context;

    int idBuyList;

    public LoaderMainPage(Context context, ArrayList<BuyList> buyListToUpdate, int id) {
        super(context);
        this.id = id;
        this.buyListToUpdate = buyListToUpdate;
    }

    public LoaderMainPage(Context context, int id) {
        super(context);
        this.context = context;
        this.id = id;
    }
    public LoaderMainPage(Context context, int idBuyList, int id) {
        super(context);
        this.context = context;
        this.idBuyList = idBuyList;
        this.id = id;
    }

    public Cursor getList(){
        SQLiteDatabase db = DBHelper.getDb();
        return db.query("buy_list", null, null, null, null, null, "date_start");

    }
    public void updateType(){
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        int size = buyListToUpdate.size();
        for(int i = 0; i < size; i++) {
            cv.clear();
            cv.put("type", buyListToUpdate.get(i).type);
            db.update("buy_list", cv, "id = ?",new String[] { ""+buyListToUpdate.get(i).id });
        }
    }


    public void updateRecover(){
        SQLiteDatabase db = DBHelper.getDb();
        ContentValues cv = new ContentValues();
        int size = buyListToUpdate.size();
        for(int i = 0; i < size; i++) {
            cv.clear();
            cv.put("type", buyListToUpdate.get(i).type);
            cv.put("date_end", -1);
            db.update("buy_list", cv, "id = ?",new String[] { ""+buyListToUpdate.get(i).id });
        }
    }

    public void getCategoryList(){
        SQLiteDatabase db = DBHelper.getDb();
        Cursor c = db.query("categories", null, null, null, null, null, "sort");
        c.moveToFirst();
        Utils.goodsCategories.add(new GoodsCategory(-1, -1, context.getString(R.string.category_out)));
        if(c.getCount()>0) {
            do {
                Utils.goodsCategories.add(new GoodsCategory(
                        c.getInt(c.getColumnIndex("id")),
                        c.getInt(c.getColumnIndex("sort")),
                        c.getString(c.getColumnIndex("name"))
                ));
            } while (c.moveToNext());
        }

        Utils.goodsCategories.add(new GoodsCategory(-2, 900000000, context.getString(R.string.category_add)));

    }

    public void getGoodsList(){
        SQLiteDatabase db = DBHelper.getDb();
        Cursor c = db.query("goods", null, null, null, null, null, "lastUseDate");
        c.moveToFirst();

        if(c.getCount()>0) {
            do {
                ProductInfo pi = new ProductInfo(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("name")),
                        c.getInt(c.getColumnIndex("cost")),
                        c.getInt(c.getColumnIndex("unit")),
                        c.getInt(c.getColumnIndex("category")),
                        c.getInt(c.getColumnIndex("categoryId")),
                        c.getLong(c.getColumnIndex("lastUseDate"))
                        );
                Utils.availableProducts.put(pi.getName(), pi);
                Utils.productNames.add(pi.getName());

            } while (c.moveToNext());
        }
    }

    public void getBuyLists(){
        SQLiteDatabase db = DBHelper.getDb();

        String selectionGoods = "id = ?";

        String selection = "id_buy_list = ?";

        Cursor buyLists = db.query("buy_list", null, null, null, null, null, null);
        buyLists.moveToFirst();

        do{
            int idBuyList = buyLists.getInt(buyLists.getColumnIndex("id"));
            String[] selectionArgs = {String.valueOf(idBuyList)};
            Cursor relations = db.query("relations", null, selection, selectionArgs, null, null, null);
            relations.moveToFirst();

            HashMap<Integer, ArrayList<BuyListItem>> buyListExp = new HashMap<>();

            if(AppPreferences.categoryAvailable) {
                for (int i = 0; i < Utils.goodsCategories.size(); i++) {
                    buyListExp.put(Utils.goodsCategories.get(i).getId(), new ArrayList<BuyListItem>());
                }
                buyListExp.put(IntConst.ID_BOUGHT_CAT_ON, new ArrayList<BuyListItem>());
            } else {
                buyListExp.put(IntConst.ID_NOT_BOUGHT_CAT_OFF, new ArrayList<BuyListItem>());
                buyListExp.put(IntConst.ID_BOUGHT_CAT_OFF, new ArrayList<BuyListItem>());
            }

            if(relations.getCount()>0){
                do{


                    BuyListItem bliTmp = new BuyListItem();
                    bliTmp.setId(relations.getInt(relations.getColumnIndex("id_goods")));

                    String[] selectionArgsGoods = {String.valueOf(bliTmp.getId())};

                    Cursor goods = db.query("goods", null, selectionGoods, selectionArgsGoods, null, null, null);

                    goods.moveToFirst();

                    bliTmp.setName(goods.getString(goods.getColumnIndex("name")));
                    bliTmp.setComment(relations.getString(relations.getColumnIndex("comment")));
                    bliTmp.setCost(goods.getInt(goods.getColumnIndex("cost")));
                    bliTmp.setWeight(relations.getInt(relations.getColumnIndex("unitCount")));
                    bliTmp.setWeightType(goods.getInt(goods.getColumnIndex("unit")));
                    bliTmp.setCategory(goods.getInt(goods.getColumnIndex("category")));
                    bliTmp.setCategoryId(goods.getInt(goods.getColumnIndex("categoryId")));
                    bliTmp.setBought(relations.getInt(relations.getColumnIndex("type"))==0 ? false : true);
                    bliTmp.setColorBck(relations.getInt(relations.getColumnIndex("colorBck")));
                    bliTmp.setColorTxt(relations.getInt(relations.getColumnIndex("colorTxt")));
                    bliTmp.setSync(false);
                    bliTmp.setAccountId(1);

                    if(!bliTmp.isBought()) {
                        if (AppPreferences.categoryAvailable) {
                            buyListExp.get(bliTmp.getCategoryId()).add(bliTmp);
                        } else {
                            buyListExp.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).add(bliTmp);
                        }
                    } else {
                        if (AppPreferences.categoryAvailable) {
                            buyListExp.get(IntConst.ID_BOUGHT_CAT_ON).add(bliTmp);
                        } else {
                            buyListExp.get(IntConst.ID_BOUGHT_CAT_OFF).add(bliTmp);
                        }
                    }

                    goods.close();

                } while(relations.moveToNext());


            }


            relations.close();

        } while (buyLists.moveToNext());


    }

    public void removeBuyList(){
        SQLiteDatabase db = DBHelper.getDb();
        String whereClause = "id_buy_list = ?";
        String whereClauseBL = "id = ?";

        String[] whereArgs = new String[]{String.valueOf(idBuyList)};
        db.delete("relations", whereClause, whereArgs);
        db.delete("buy_list", whereClauseBL, whereArgs);
    }

    @Override
    public Cursor loadInBackground() {
        switch (id){
            case IntConst.LOADER_GET_MAIN_PAGE_GOODS_LIST:
                return getList();
            case 1:
                updateType();
                break;
            case 2:
                updateRecover();
                break;
            case IntConst.LOADER_GET_CATEGORIES_LIST:
                getCategoryList();
                break;
            case IntConst.LOADER_GET_GOODS_LIST:
                getGoodsList();
                break;
            case IntConst.LOADER_GET_BUY_LIST_WITH_ITEMS:
                getBuyLists();
                break;
            case IntConst.LOADER_REMOVE_BL:
                removeBuyList();
                break;
        }
        return null;
    }
}
