package ru.fromtheseventhsky.buylist.broadcast;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.fromtheseventhsky.buylist.service.NotificationService;
import ru.fromtheseventhsky.buylist.utils.DBHelper;


public class MyBroadcastReceiver extends BroadcastReceiver {

    private final String BOOT_ACTION = "android.intent.action.BOOT_COMPLETED";
    private final String TIME_ACTION = "android.intent.action.TIME_SET";
    private final String DATE_ACTION = "android.intent.action.DATE_CHANGED";

    @Override
    public void onReceive(Context context, Intent intent) {

        String action="";
        if(intent.getAction()!=null) {
            action = intent.getAction();
        }
        if (action.equalsIgnoreCase(BOOT_ACTION)||action.equalsIgnoreCase(TIME_ACTION)||action.equalsIgnoreCase(DATE_ACTION)) {
            DBHelper.initWritable(context);

            setOnetimeTimer(context, 9, 9);

            return;
        }

        Intent notificationService = new Intent(context, NotificationService.class);
        notificationService.putExtra("calledAction", action);
        context.startService(notificationService);
    }

    public void setOnetimeTimer(Context context, long nextTime, int idPendingIntent){
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, MyBroadcastReceiver.class);
        intent.setAction("ACTION_NOTIFY");
        PendingIntent pi = PendingIntent.getBroadcast(context, idPendingIntent, intent, 0);
        am.set(AlarmManager.RTC_WAKEUP, nextTime, pi);
    }

}
