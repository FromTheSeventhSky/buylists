package ru.fromtheseventhsky.buylist.custom.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.fromtheseventhsky.buylist.fragments.BuyListPager;
import ru.fromtheseventhsky.buylist.fragments.FragmentCreateBuyList;
import ru.fromtheseventhsky.buylist.fragments.MainPageFragment;


public class ViewPagerCustomAdapter extends FragmentPagerAdapter {
    private MainPageFragment mainPageFragment;
    private BuyListPager buyListPager;
    FragmentCreateBuyList fragmentCreateBuyList;

    private int count = 3;

    public ViewPagerCustomAdapter(FragmentManager fm, MainPageFragment mainPageFragment, BuyListPager buyListPager, FragmentCreateBuyList fragmentCreateBuyList) {
        super(fm);
        this.mainPageFragment = mainPageFragment;
        this.buyListPager  = buyListPager;
        this.fragmentCreateBuyList = fragmentCreateBuyList;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return fragmentCreateBuyList;
            case 1:
                return mainPageFragment;
            case 2:
                return buyListPager;
        }
        return null;
    }



    @Override
    public int getCount() {
        return count;
    }


}