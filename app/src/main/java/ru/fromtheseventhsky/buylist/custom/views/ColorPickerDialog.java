package ru.fromtheseventhsky.buylist.custom.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Bundle;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;

public class ColorPickerDialog extends Dialog {

    public interface OnColorChangedListener {
        void colorChanged(int color, boolean bck);
    }
    int h;
    int w;



    ColorPickerView colorPickerView;

    private OnColorChangedListener mListener;
    private int mInitialColor;

    private static class ColorPickerView extends View {
        private Paint mPaint;
        private Paint mCenterPaint;
        private Paint mCenterPaintText;
        private final int[] mColors;
        private OnColorChangedListener mListener;
        private static int centerRadius;
        private static int width;
        private static int strokeWidth;
        private static int strokeWidthMin;

        private static boolean isBck;
        private static int colorBck;
        private static int  colorTxt;

        private float unitGlob = 0.0f;

        private static int CENTER_X;//100;
        private static int CENTER_Y;//100;
        private static int CENTER_RADIUS;//32;

        ColorPickerView(Context c, OnColorChangedListener l, int color, int h, int w) {
            super(c);

            strokeWidthMin = (int) (w/50);
            centerRadius = (int) (w/8);
            strokeWidth = (int) (w/7);
            width = (int) (w/2.5);

            CENTER_X = width;
            CENTER_Y = width;
            CENTER_RADIUS = centerRadius;

            mListener = l;


            mColors = new int[]{
                    0xFFFF0000, 0xFFFF00FF, 0xFF0000FF, 0xFF00FFFF, 0xFF00FF00,
                    0xFFFFFF00, 0xFFFF0000
            };

            Shader s = new SweepGradient(0, 0, mColors, null);



            mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mPaint.setShader(s);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(strokeWidth);


            mCenterPaintText = new Paint(Paint.ANTI_ALIAS_FLAG);
            mCenterPaintText.setColor(Color.LTGRAY);
            mCenterPaintText.setStrokeWidth(strokeWidthMin);
            mCenterPaintText.setTextSize(centerRadius*1.7f);

            mCenterPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
            mCenterPaint.setColor(color);
            mCenterPaint.setStrokeWidth(strokeWidthMin);
        }



        public void setColors(int c1, int c2, int c3, int c4, int c5, int c6, int c7){
            mColors[0] = c1;
            mColors[1] = c2;
            mColors[2] = c3;
            mColors[3] = c4;
            mColors[4] = c5;
            mColors[5] = c6;
            mColors[6] = c7;
            Shader s = new SweepGradient(0, 0, mColors, null);
            mPaint.setShader(s);
            mCenterPaint.setColor(interpColor(mColors, unitGlob));
            invalidate();
        }



        private boolean mTrackingCenter;
        private boolean mHighlightCenter;

        @Override
        protected void onDraw(Canvas canvas) {
            float r = CENTER_X - mPaint.getStrokeWidth() * 0.5f;

            canvas.translate(CENTER_X, CENTER_X);

            canvas.drawOval(new RectF(-r, -r, r, r), mPaint);
            canvas.drawCircle(0, 0, CENTER_RADIUS, mCenterPaint);
            canvas.drawCircle(0, 0, CENTER_RADIUS/3, mCenterPaintText);

            if (mTrackingCenter) {
                int c = mCenterPaint.getColor();
                mCenterPaint.setStyle(Paint.Style.STROKE);

                if (mHighlightCenter) {
                    mCenterPaint.setAlpha(0xFF);
                } else {
                    mCenterPaint.setAlpha(0x00);
                }
                canvas.drawCircle(0, 0,
                        CENTER_RADIUS + mCenterPaint.getStrokeWidth(),
                        mCenterPaint);

                mCenterPaintText.setStyle(Paint.Style.FILL);
                mCenterPaint.setStyle(Paint.Style.FILL);
                mCenterPaint.setColor(c);

            }
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(CENTER_X * 2, CENTER_Y * 2);
        }



        private int floatToByte(float x) {
            int n = java.lang.Math.round(x);
            return n;
        }

        private int pinToByte(int n) {
            if (n < 0) {
                n = 0;
            } else if (n > 255) {
                n = 255;
            }
            return n;
        }

        private int ave(int s, int d, float p) {
            return s + java.lang.Math.round(p * (d - s));
        }

        private int interpColor(int colors[], float unit) {
            if (unit <= 0) {
                return colors[0];
            }
            if (unit >= 1) {
                return colors[colors.length - 1];
            }

            float p = unit * (colors.length - 1);
            int i = (int) p;
            p -= i;

            // now p is just the fractional part [0...1) and i is the index
            int c0 = colors[i];
            int c1 = colors[i + 1];
            int a = ave(Color.alpha(c0), Color.alpha(c1), p);
            int r = ave(Color.red(c0), Color.red(c1), p);
            int g = ave(Color.green(c0), Color.green(c1), p);
            int b = ave(Color.blue(c0), Color.blue(c1), p);

            return Color.argb(a, r, g, b);
        }
        public void setBck(boolean bck, int colorBck, int colorTxt){
            this.isBck = bck;
            this.colorBck = colorBck;
            this.colorTxt = colorTxt;
            if(isBck) {
                mCenterPaintText.setColor(colorTxt);
            } else {
                mCenterPaintText.setColor(colorBck);

            }


        }

        private int rotateColor(int color, float rad) {
            float deg = rad * 180 / 3.1415927f;
            int r = Color.red(color);
            int g = Color.green(color);
            int b = Color.blue(color);

            ColorMatrix cm = new ColorMatrix();
            ColorMatrix tmp = new ColorMatrix();

            cm.setRGB2YUV();
            tmp.setRotate(0, deg);
            cm.postConcat(tmp);
            tmp.setYUV2RGB();
            cm.postConcat(tmp);

            final float[] a = cm.getArray();

            int ir = floatToByte(a[0] * r + a[1] * g + a[2] * b);
            int ig = floatToByte(a[5] * r + a[6] * g + a[7] * b);
            int ib = floatToByte(a[10] * r + a[11] * g + a[12] * b);

            return Color.argb(Color.alpha(color), pinToByte(ir),
                    pinToByte(ig), pinToByte(ib));
        }

        private static final float PI = 3.1415926f;

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX() - CENTER_X;
            float y = event.getY() - CENTER_Y;
            boolean inCenter = java.lang.Math.sqrt(x * x + y * y) <= CENTER_RADIUS;

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mTrackingCenter = inCenter;
                    if (inCenter) {
                        mHighlightCenter = true;
                        invalidate();
                        break;
                    }
                case MotionEvent.ACTION_MOVE:
                    if (mTrackingCenter) {
                        if (mHighlightCenter != inCenter) {
                            mHighlightCenter = inCenter;
                            invalidate();
                        }
                    } else {
                        float angle = (float) java.lang.Math.atan2(y, x);
                        // need to turn angle [-PI ... PI] into unit [0....1]
                        float unit = angle / (2 * PI);

                        if (unit < 0) {
                            unit += 1;
                        }
                        unitGlob = unit;

                        mCenterPaint.setColor(interpColor(mColors, unit));

                        invalidate();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (mTrackingCenter) {
                        if (inCenter) {
                            mListener.colorChanged(mCenterPaint.getColor(), isBck);
                        }
                        mTrackingCenter = false;    // so we draw w/o halo
                        invalidate();
                    }
                    break;
            }
            return true;
        }
    }

    public void setBck(boolean isBck, int colorBck, int colorTxt){
        colorPickerView.setBck(isBck, colorBck, colorTxt);
    }

    public ColorPickerDialog(Context context,
                             OnColorChangedListener listener,
                             int initialColor, int h, int w) {
        super(context);

        this.h = h;
        this.w = w;



        mListener = listener;
        mInitialColor = initialColor;
    }
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnColorChangedListener l = new OnColorChangedListener() {
            public void colorChanged(int color, boolean bck) {
                mListener.colorChanged(color, bck);
                dismiss();
            }
        };

        setContentView(R.layout.color_picker_popup);
        LinearLayout root = (LinearLayout) findViewById(R.id.colorPickerRoot);
        title = (TextView) findViewById(R.id.ColorPopupTV);



        SeekBar seek = (SeekBar) findViewById(R.id.colorPickerSeek);


        title.setTextColor(AppPreferences.colorTheme_FontColor);
        seek.getProgressDrawable().setColorFilter(AppPreferences.colorTheme_Actionbar, PorterDuff.Mode.SRC_IN);

        //seek.getThumb().setColorFilter(AppPreferences.colorTheme_Actionbar, PorterDuff.Mode.SRC_IN);

        colorPickerView = new ColorPickerView(getContext(), l, mInitialColor, h, w);
        root.addView(colorPickerView);

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String tmp = "FF";
                String tmp2 = "00";

                if(progress<256) {
                    if (progress > 15) {
                        tmp = Integer.toHexString(progress);
                    } else {
                        tmp = "0" + Integer.toHexString(progress);
                    }
                } else {
                    if (progress-256 > 15) {
                        tmp2 = Integer.toHexString(progress-256);
                    } else {
                        tmp2 = "0" + Integer.toHexString(progress-256);
                    }
                }



                String color1 = "#FF"+tmp+tmp2+tmp2;
                String color2 = "#FF"+tmp+tmp2+tmp;
                String color3 = "#FF"+tmp2+tmp2+tmp;
                String color4 = "#FF"+tmp2+tmp+tmp;
                String color5 = "#FF"+tmp2+tmp+tmp2;
                String color6 = "#FF"+tmp+tmp+tmp2;



                float progressPercent = progress/255;

                colorPickerView.setColors(
                        Color.parseColor(color1),
                        Color.parseColor(color2),
                        Color.parseColor(color3),
                        Color.parseColor(color4),
                        Color.parseColor(color5),
                        Color.parseColor(color6),
                        Color.parseColor(color1));


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        setTitle(
                Html.fromHtml(
                        "<font color='#" + Integer.toHexString(AppPreferences.colorTheme_FontColor).substring(2, 8)
                                + "'>" + getContext().getResources().getString(R.string.color_popup_title) + "</font>"));
        //setTitle(R.string.color_popup_title);
    }
}