package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.loaders.LoaderCreateBuyListItem;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.GoodsCategory;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class PreferencesCategoriesAdapter extends BaseAdapter implements LoaderManager.LoaderCallbacks<Long> {


    private LayoutInflater mInflater;
    private LoaderManager loaderManager;
    private LoaderManager.LoaderCallbacks<Long> callbacks;
    private Context context;

    private boolean removeMode;
    private boolean[] checked;

    public PreferencesCategoriesAdapter(LayoutInflater mInflater, LoaderManager loaderManager, Context context){
        callbacks = this;
        this.mInflater = mInflater;
        this.loaderManager = loaderManager;
        this.context = context;
    }

    public void setRemoveMode(boolean removeMode, int pos){
        this.removeMode = removeMode;
        checked = new boolean[Utils.goodsCategories.size()-1];
        for (int i = 0; i < Utils.goodsCategories.size()-1; i++) {
            if(pos == i)
                checked[i] = true;
            else
                checked[i] = false;
        }
        notifyDataSetChanged();
    }

    public void fillDeleteList(ArrayList<Integer> deleteList){
        deleteList.clear();
        for (int i = 0; i < Utils.goodsCategories.size()-1; i++) {
            if(checked[i]==true)
                deleteList.add(i);
        }
    }


    @Override
    public int getCount() {
        return Utils.goodsCategories.size()-1;
    }

    @Override
    public GoodsCategory getItem(int position) {
        return Utils.goodsCategories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null){
            v = mInflater.inflate(R.layout.preference_category_item, parent, false);

            TextView title = (TextView) v.findViewById(R.id.PrefCat_title);
            Button up = (Button) v.findViewById(R.id.PrefCat_up);
            Button down = (Button) v.findViewById(R.id.PrefCat_down);
            CheckBox chb = (CheckBox)v.findViewById(R.id.PrefCat_chb);

            chb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checked[position] = isChecked;
                }
            });
            up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoodsCategory gcBottom = getItem(position-1);
                    GoodsCategory gcUp = getItem(position);
                    int tmpSort = gcBottom.getSort();
                    gcBottom.setSort(gcUp.getSort());
                    gcUp.setSort(tmpSort);
                    sortData();
                    notifyDataSetChanged();
                    Bundle b = new Bundle();
                    b.putInt("gcBottom", position-1);
                    b.putInt("gcUp", position);
                    b.putBoolean("dimUp", true);
                    loaderManager.initLoader(IntConst.LOADER_UPDATE_CATEGORY_SORT, b, callbacks).forceLoad();
                }
            });
            down.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoodsCategory gcBottom = getItem(position);
                    GoodsCategory gcUp = getItem(position+1);
                    int tmpSort = gcBottom.getSort();
                    gcBottom.setSort(gcUp.getSort());
                    gcUp.setSort(tmpSort);
                    sortData();
                    notifyDataSetChanged();
                    Bundle b = new Bundle();
                    b.putInt("gcBottom", position);
                    b.putInt("gcUp", position+1);
                    b.putBoolean("dimUp", false);
                    loaderManager.initLoader(IntConst.LOADER_UPDATE_CATEGORY_SORT, b, callbacks).forceLoad();

                }
            });

            ViewContainer vk = new ViewContainer(title, up, down, chb);
            v.setTag(vk);
        }

        GoodsCategory gc = getItem(position);

        ViewContainer vk = (ViewContainer) v.getTag();
        TextView title = vk.getTitle();
        Button up = vk.getUp();
        Button down = vk.getDown();
        CheckBox chb = vk.getChb();

        if(position == 1){
            up.setEnabled(false);
        } else {
            up.setEnabled(true);
        }
        if(position == getCount()-1){
            down.setEnabled(false);
        } else {
            down.setEnabled(true);
        }


        if(removeMode){
            up.setVisibility(View.GONE);
            down.setVisibility(View.GONE);
            chb.setVisibility(View.VISIBLE);
            chb.setChecked(checked[position]);
        } else {
            up.setVisibility(View.VISIBLE);
            down.setVisibility(View.VISIBLE);
            up.setBackgroundDrawable(Utils.getStateList());
            down.setBackgroundDrawable(Utils.getStateList());
            chb.setVisibility(View.GONE);
        }
        if(gc.getId()==-1){
            up.setVisibility(View.GONE);
            down.setVisibility(View.GONE);
            chb.setVisibility(View.GONE);
        }


        title.setTextColor(AppPreferences.colorTheme_FontColor);
        title.setText(gc.getName());



        return v;
    }

    @Override
    public Loader<Long> onCreateLoader(int i, Bundle bundle) {
        return new LoaderCreateBuyListItem(context, bundle, IntConst.LOADER_UPDATE_CATEGORY_SORT);
    }

    @Override
    public void onLoadFinished(Loader<Long> longLoader, Long aLong) {
        loaderManager.destroyLoader(IntConst.LOADER_UPDATE_CATEGORY_SORT);
    }

    @Override
    public void onLoaderReset(Loader<Long> longLoader) {

    }

    public class ViewContainer{
        private TextView title;
        private Button up;
        private Button down;
        private CheckBox chb;
        ViewContainer(TextView title, Button up, Button down, CheckBox chb){
            this.title = title;
            this.up = up;
            this.down = down;
            this.chb = chb;
        }

        public TextView getTitle() {
            return title;
        }

        public Button getUp() {
            return up;
        }

        public Button getDown() {
            return down;
        }

        public CheckBox getChb() {
            return chb;
        }
    }

    void sortData() {
        Collections.sort(Utils.goodsCategories, new Comparator<GoodsCategory>() {
            @Override
            public int compare(GoodsCategory obj1, GoodsCategory ob2) {
                if (obj1.getSort() > ob2.getSort()) {
                    return 1;
                }
                if (obj1.getSort() < ob2.getSort()) {
                    return -1;
                }
                return 0;
            }
        });
    }
}
