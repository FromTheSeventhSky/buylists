package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.CalendarCell;


public class CalendarGridAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private int widthOfItem;
    private int heightOfItem;
    private int heightOfText;

    private int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    private ArrayList<CalendarCell> days;

    private ArrayList<View> views;


    private int curDayNum;
    private int curMonthNum;

    private int originalDayNum;
    private int originalMonthNum;



    public CalendarGridAdapter(Context context, int widthOfItem) {
        this.widthOfItem = (int) (widthOfItem-context.getResources().getDisplayMetrics().density*2)/7;
        heightOfItem = (int) (widthOfItem-context.getResources().getDisplayMetrics().density*2)/8;
        heightOfText = (int) (widthOfItem-context.getResources().getDisplayMetrics().density*2)/14;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Calendar c = Calendar.getInstance();

        days = new ArrayList<>();
        views = new ArrayList<>();

        c.setFirstDayOfWeek(Calendar.MONDAY);

        int weekDay = c.get(Calendar.DAY_OF_WEEK);

        GregorianCalendar calendar = new GregorianCalendar();
        if(calendar.isLeapYear(calendar.get(GregorianCalendar.YEAR))){
            daysOfMonth[1] = 29;
        }

        int russianDayWeek;
        if(weekDay==Calendar.SUNDAY){
            russianDayWeek = 7;
        } else {
            russianDayWeek = weekDay - 1;
        }
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int curMonth = calendar.get(Calendar.MONTH);
        for(int i = russianDayWeek; i > 1; i--){
            --dayOfMonth;
            if(dayOfMonth==0){
                if(--curMonth == -1){
                    curMonth = 11;
                }
                dayOfMonth = daysOfMonth[curMonth];
            }
            days.add(0, new CalendarCell(dayOfMonth, curMonth));
        }
        int forCtr = 43 - russianDayWeek;
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        curMonth = calendar.get(Calendar.MONTH);
        for (int i = 0; i < forCtr; i++) {
            days.add(new CalendarCell(dayOfMonth, curMonth));
            if(++dayOfMonth>daysOfMonth[curMonth]){
                dayOfMonth = 1;
                if(++curMonth == 11){
                    curMonth = 0;
                }
            }
        }
        originalDayNum = calendar.get(Calendar.DAY_OF_MONTH);
        originalMonthNum = calendar.get(Calendar.MONTH);
        curDayNum = calendar.get(Calendar.DAY_OF_MONTH);
        curMonthNum = calendar.get(Calendar.MONTH);
    }

    @Override
    public int getCount() {
        return 42;
    }

    @Override
    public CalendarCell getItem(int position) {
        return days.get(position);
    }
    public boolean isClickable(int position){
        CalendarCell tmp = getItem(position);
        return !(tmp.getMonth() <= originalMonthNum && tmp.getDay() < originalDayNum);
    }

    public void setDate(int position){
        curDayNum = getItem(position).getDay();
        curMonthNum = getItem(position).getMonth();
        //notifyDataSetChanged();
        notifyDataSetInvalidated();
    }
    public int getOrigMonth(){
        return originalMonthNum;
    }

    public int getSelectedMonth(){
        return curMonthNum;
    }
    public int getSelectedDay(){
        return curDayNum;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(views.size()!=42){
            convertView = mInflater.inflate(R.layout.calendar_item, parent, false);
            convertView.setLayoutParams(new AbsListView.LayoutParams(widthOfItem, heightOfItem));
            TextView tv = (TextView) convertView.findViewById(R.id.calendarText);
            View v = convertView.findViewById(R.id.calendarToday);
            convertView.setTag(new ViewContainer(tv, v));
        } else {
            convertView = views.get(position);
        }

        ViewContainer cv = (ViewContainer) convertView.getTag();
        TextView tv = cv.tv;
        View v = cv.v;

        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, heightOfText);

        CalendarCell tmp = getItem(position);
        if(tmp.getMonth() != curMonthNum || (tmp.getMonth() == originalMonthNum && tmp.getDay() < originalDayNum)){
            tv.setBackgroundColor(Color.LTGRAY);
            tv.setTextColor(Color.DKGRAY);
        } else {
            tv.setBackgroundColor(Color.WHITE);
            tv.setTextColor(Color.BLACK);
        }
        v.setVisibility(View.GONE);
        if(tmp.getDay()==originalDayNum && tmp.getMonth()==originalMonthNum){
            v.setVisibility(View.VISIBLE);
            v.setBackgroundResource(R.drawable.calendar_main_day);
        }
        if(tmp.getDay()==curDayNum && tmp.getMonth() == curMonthNum){
            v.setVisibility(View.VISIBLE);
            v.setBackgroundResource(R.drawable.calendar_current_day);
        }


        tv.setText(String.valueOf(tmp.getDay()));

        return convertView;
    }
    public class ViewContainer {
        public TextView tv;
        public View v;
        ViewContainer(TextView tv, View v){
            this.tv = tv;
            this.v = v;
        }
    }
}
