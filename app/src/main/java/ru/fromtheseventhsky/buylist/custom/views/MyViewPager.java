package ru.fromtheseventhsky.buylist.custom.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;


public class MyViewPager extends ViewPager {


    public MyViewPager(final Context _context, final AttributeSet attrs) {
        super(_context, attrs);
    }
    public MyViewPager(Context _context) {
        super(_context);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }
}