package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;

public class PrefListViewDialogAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private String[] arr;
    private int clickPosition;

    public PrefListViewDialogAdapter(Context context, String[] arr, int clickPosition){
        this.context = context;
        this.arr = arr;
        this.clickPosition = clickPosition;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arr.length;
    }

    @Override
    public String getItem(int position) {
        return arr[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.pref_lv_tem, parent, false);
        }

        TextView tv = (TextView) convertView;
        tv.setText(getItem(position));
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        tv.setTextColor(AppPreferences.colorTheme_FontColor);
        if(clickPosition == position){
            tv.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);
        } else {
            tv.setBackgroundColor(AppPreferences.colorTheme_DeselectedBtnColor);
        }
        return convertView;
    }
}
