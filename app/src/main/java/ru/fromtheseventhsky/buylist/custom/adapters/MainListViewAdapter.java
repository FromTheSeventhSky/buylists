package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.BuyList;

public class MainListViewAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private Context context;

    private ArrayList<BuyList> originalBuyList;
    private ArrayList<BuyList> buyList;
    private DisplayMetrics dm;

    public MainListViewAdapter(Context context, ArrayList<BuyList> buyList, DisplayMetrics dm) {
        this.context = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.buyList = buyList;
        originalBuyList = buyList;
        this.dm = dm;
    }

    public ArrayList<BuyList> getBuyList() {
        return buyList;
    }

    public void setNewList(ArrayList<BuyList> buyList){
        this.buyList = buyList;
        originalBuyList = buyList;
        notifyDataSetChanged();
    }

    public void addNewItem(BuyList item){
        buyList.add(0, item);
        originalBuyList = buyList;
    }

    @Override
    public int getCount() {
        return buyList.size();
    }

    @Override
    public Object getItem(int position) {
        return buyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return buyList.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.item_of_list_of_buy_list, parent, false);
            RelativeLayout darkRoot = (RelativeLayout) view.findViewById(R.id.list_of_buy_list_item_dark_back);
            RelativeLayout root = (RelativeLayout) view.findViewById(R.id.list_of_buy_list_item_root);
            RelativeLayout rootSecond = (RelativeLayout) view.findViewById(R.id.list_of_buy_list_item_root_second);
            TextView TVold = (TextView) view.findViewById(R.id.list_of_buy_list_item_text_old);
            TextView TVname = (TextView) view.findViewById(R.id.list_of_buy_list_item_name);
            TextView TVcomment = (TextView) view.findViewById(R.id.list_of_buy_list_item_comment);
            TextView TVlastSync = (TextView) view.findViewById(R.id.list_of_buy_list_item_comment);
            ImageView TVimgNew = (ImageView) view.findViewById(R.id.list_of_buy_list_item_image_new);
            Button TVsyncBtn = (Button) view.findViewById(R.id.list_of_buy_list_item_sync);
            ViewContainer viewContainer = new ViewContainer(darkRoot, root, rootSecond, TVold, TVname, TVcomment, TVlastSync, TVimgNew, TVsyncBtn);

            view.setTag(viewContainer);
        }

        ViewContainer viewContainer = (ViewContainer) view.getTag();

        BuyList bl = (BuyList) getItem(position);

        RelativeLayout darkRoot = viewContainer.darkRoot;
        RelativeLayout root = viewContainer.root;
        RelativeLayout rootSecond = viewContainer.rootSecond;
        TextView TVold = viewContainer.TVold;
        TextView TVname = viewContainer.TVname;
        TextView TVcomment = viewContainer.TVcomment;
        TextView TVlastSync = viewContainer.TVlastSync;
        ImageView TVimgNew = viewContainer.TVimgNew;
        Button TVsyncBtn = viewContainer.TVsyncBtn;



        root.setTag(true);

        TVname.setText(bl.name+"");
        TVcomment.setText(bl.comment+"");

        rootSecond.setVisibility(View.GONE);
        root.setX(0);

        if(bl.type==2){
            TVold.setVisibility(View.VISIBLE);
            darkRoot.setVisibility(View.VISIBLE);
        } else {
            TVold.setVisibility(View.GONE);
            darkRoot.setVisibility(View.GONE);
        }


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.getDefault());
        TVlastSync.setText(sdf.format(new Date(bl.date)));
        TVimgNew.setImageDrawable(context.getResources()
                .getDrawable(R.drawable.image_new_tag));
        TVsyncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "OLOLO", Toast.LENGTH_SHORT).show();
            }
        });

        //root.setBackgroundResource(R.drawable.selector_item);
        root.setBackgroundColor(bl.colorBck);

        TVname.setTextColor(bl.colorTxt);
        TVcomment.setTextColor(bl.colorTxt);
        TVlastSync.setTextColor(bl.colorTxt);


        return view;
    }

    private class ViewContainer{
        RelativeLayout root;
        RelativeLayout rootSecond;
        TextView TVold;
        TextView TVname;
        TextView TVcomment;
        TextView TVlastSync;
        ImageView TVimgNew;
        Button TVsyncBtn;
        RelativeLayout darkRoot;
        ViewContainer(
                RelativeLayout darkRoot,
                RelativeLayout root,
                RelativeLayout rootSecond,
                TextView TVold,
                TextView TVname,
                TextView TVcomment,
                TextView TVlastSync,
                ImageView TVimgNew,
                Button TVsyncBtn){
            this.TVold = TVold;
            this.darkRoot = darkRoot;
            this.root = root;
            this.rootSecond = rootSecond;
            this.TVname = TVname;
            this.TVcomment = TVcomment;
            this.TVlastSync = TVlastSync;
            this.TVimgNew = TVimgNew;
            this.TVsyncBtn = TVsyncBtn;
        }
    }
}
