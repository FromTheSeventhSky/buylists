package ru.fromtheseventhsky.buylist.custom.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

import ru.fromtheseventhsky.buylist.fragments.FragmentCreateBuyListItem;
import ru.fromtheseventhsky.buylist.fragments.PreferenceGoodsFragment;
import ru.fromtheseventhsky.buylist.utils.Utils;

/**
 * Created by 123 on 14.07.2015.
 */
public class CustomSpinner extends Spinner {
    private FragmentCreateBuyListItem bl;
    private PreferenceGoodsFragment pgf;
    private boolean category;
    private boolean isPgf;

    public CustomSpinner(Context context) {
        super(context);
    }

    public CustomSpinner(Context context, int mode) {
        super(context, mode);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    @Override
    public void setSelection(int position) {
        if(category) {
            if (position == Utils.goodsCategories.size() - 1) {
                bl.openSelectionDialog();
                return;
            }
        }
        if(isPgf) {
            if (position == Utils.goodsCategories.size() - 1) {
                pgf.openSelectionDialog();
                return;
            }
        }
        super.setSelection(position);
    }


    public void setBl(FragmentCreateBuyListItem bl) {
        category = true;
        this.bl = bl;
    }
    public void setPGF(PreferenceGoodsFragment pgf) {
        isPgf = true;
        this.pgf = pgf;
    }
}
