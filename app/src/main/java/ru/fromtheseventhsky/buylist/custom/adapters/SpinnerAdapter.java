package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.GoodsCategory;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class SpinnerAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context context;

    public SpinnerAdapter(LayoutInflater mInflater, Context context){
        this.mInflater = mInflater;
        this.context = context;
    }

    @Override
    public int getCount() {
        return Utils.goodsCategories.size();
    }

    @Override
    public GoodsCategory getItem(int position) {
        return Utils.goodsCategories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.spinner_tv, parent, false);
        }
        TextView tv = (TextView) convertView;
        tv.setTextColor(AppPreferences.colorTheme_FontColor);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        tv.setText(getItem(position).getName());

        return tv;
    }
}