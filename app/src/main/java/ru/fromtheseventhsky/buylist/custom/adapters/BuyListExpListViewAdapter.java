package ru.fromtheseventhsky.buylist.custom.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyListItem;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class BuyListExpListViewAdapter extends BaseExpandableListAdapter {

    private HashMap<Integer, ArrayList<BuyListItem>> arrLists;
    private HashMap<Integer, Boolean> arrListsExpanded;

    private ArrayList<Integer> groups;

    private ArrayList<ArrayList<Boolean>> checked;

    private String[] weightTypeTitleArray;
    private String[] weightTypeArray;
    private String cost;

    private boolean deleteMode;

    private FragmentManager fm;

    private LayoutInflater mInflater;
    private Context context;

    public BuyListExpListViewAdapter(HashMap<Integer, ArrayList<BuyListItem>> arrLists, ArrayList<Integer> groups, Context context, FragmentManager fm){
        this.groups = groups;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arrLists = arrLists;
        this.context = context;
        this.fm = fm;
        checked = new ArrayList<>();
        weightTypeTitleArray = context.getResources().getStringArray(R.array.weight_type_title);
        weightTypeArray = context.getResources().getStringArray(R.array.weight_type);
        cost = context.getString(R.string.cost);
    }

    public void setNewGroup(HashMap<Integer, ArrayList<BuyListItem>> arrLists, ArrayList<Integer> groups){
        this.arrLists = arrLists;
        this.groups = groups;
        this.arrListsExpanded = Utils.arrListsExpanded;
        notifyDataSetChanged();
    }

    public void setDeleteModeOn(int groupPosition, int childPosition){
        for(int i = 0; i < groups.size(); i++){
            ArrayList<Boolean> tmp = new ArrayList<Boolean>();
            int chc = arrLists.get(groups.get(i)).size();
            for (int j = 0; j < chc; j++) {
                tmp.add(false);
            }
            checked.add(tmp);
        }
        checked.get(groupPosition).set(childPosition, true);
        deleteMode = true;
        notifyDataSetChanged();
    }

    public void setDeleteModeOff(){
        checked.clear();
        deleteMode = false;
        notifyDataSetChanged();
    }

    public void setDeletePosition(int groupPosition, int childPosition, boolean isChecked){
        checked.get(groupPosition).set(childPosition, isChecked);
    }

    public void fillDeleteList(ArrayList<Integer> deleteList){
        for(int i = 0; i < groups.size(); i++){
            int chc = arrLists.get(groups.get(i)).size();

            for (int j = 0; j < chc; j++) {

                if(checked.get(i).get(j)==true){
                    deleteList.add(getChild(i,j).getId());

                    BuyListItem tmpBLI = getChild(i,j);
                    if(tmpBLI.isBought()){
                        Utils.boughtInCurrentList--;
                    }
                    Utils.positionsInCurrentList--;
                    Utils.costInCurrentList-=tmpBLI.getCost();
                }
            }
        }
    }

    public void deleteObjInDeleteList(){
        for(int i = 0; i < groups.size(); i++){
            int chc = arrLists.get(groups.get(i)).size();
            for (int j = chc-1; j >= 0; j--) {
                if(checked.get(i).get(j)==true){
                    arrLists.get(groups.get(i)).remove(j);
                    if(arrLists.get(groups.get(i)).size()==0) {
                        groups.remove(i);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public void removeBuyList(int groupPosition, int childPosition){
        arrLists.get(groups.get(groupPosition)).remove(childPosition);

        notifyDataSetChanged();
    }


    public boolean isGroupExpanded(int groupPosition){
        return arrListsExpanded.get(groups.get(groupPosition));
    }
    public void setGroupExpanded(int groupPosition){
        arrListsExpanded.put(groups.get(groupPosition), true);
    }
    public void setGroupCollapsed(int groupPosition){
        arrListsExpanded.put(groups.get(groupPosition), false);
    }

    public void clean(){
        arrLists.clear();
        groups.clear();
        notifyDataSetChanged();
    }

    public void addNewCategory(){
        arrLists.put(Utils.goodsCategories.get(Utils.goodsCategories.size()-2).getId(), new ArrayList<BuyListItem>());
        notifyDataSetChanged();
    }

    public void moveIfNeed(){
        if (AppPreferences.categoryAvailable) {
            int tmpPos;
            for (int position = 0; position < groups.size(); position++) {
                ArrayList<BuyListItem> tmp = arrLists.get(groups.get(position));
                tmpPos = groups.get(position);
                for (int i = 0; i < tmp.size(); i++) {
                    BuyListItem bliTmp = tmp.get(i);
                    if(!bliTmp.isBought()) {
                        if (bliTmp.getCategoryId() != tmpPos) {
                            arrLists.get(bliTmp.getCategoryId()).add(bliTmp);
                            tmp.remove(bliTmp);
                        }
                    }
                }

            }
        }
    }

    public void sortList(int position){
        ArrayList<BuyListItem> tmp = arrLists.get(position);

        Collections.sort(tmp, new Comparator<BuyListItem>() {
            @Override
            public int compare(BuyListItem buyListItem_1, BuyListItem buyListItem_2) {
                if (buyListItem_1.getDateAdd() > buyListItem_2.getDateAdd()) {
                    return -1;
                }
                if (buyListItem_1.getDateAdd() < buyListItem_2.getDateAdd()) {
                    return 1;
                }
                return 0;
            }
        });
        notifyDataSetChanged();
    }
    public void sortLists(){
        for (int position = 0; position < groups.size(); position++) {
            ArrayList<BuyListItem> tmp = arrLists.get(groups.get(position));

            Collections.sort(tmp, new Comparator<BuyListItem>() {
                @Override
                public int compare(BuyListItem buyListItem_1, BuyListItem buyListItem_2) {
                    if (buyListItem_1.getDateAdd() > buyListItem_2.getDateAdd()) {
                        return 1;
                    }
                    if (buyListItem_1.getDateAdd() < buyListItem_2.getDateAdd()) {
                        return -1;
                    }
                    return 0;
                }
            });
        }

    }




    public void rebuildGroups(){
        groups.clear();
        if(AppPreferences.categoryAvailable){
            for (int i = 0; i < Utils.goodsCategories.size()-1; i++) {
                if(arrLists.get(Utils.goodsCategories.get(i).getId()).size()>0){
                    groups.add(Utils.goodsCategories.get(i).getId());
                }
            }
            if(arrLists.get(IntConst.ID_BOUGHT_CAT_ON).size()>0) {
                groups.add(IntConst.ID_BOUGHT_CAT_ON);
            }
        } else {
            if(arrLists.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).size()>0) {
                groups.add(IntConst.ID_NOT_BOUGHT_CAT_OFF);
            }
            if(arrLists.get(IntConst.ID_BOUGHT_CAT_OFF).size()>0) {
                groups.add(IntConst.ID_BOUGHT_CAT_OFF);
            }
        }
    }

    public int getGroupIdForExpand(BuyListItem bli){
        if(AppPreferences.categoryAvailable){
            for (int i = 0; i < groups.size(); i++) {
                if(bli.getCategoryId()==Utils.goodsCategories.get(i).getId()){
                    return i;
                }
            }
        }
        return 0;
    }

    public void moveToBought(BuyListItem bli){
        if (AppPreferences.categoryAvailable) {
            arrLists.get(bli.getCategoryId()).remove(bli);
            if(arrLists.get(IntConst.ID_BOUGHT_CAT_ON).size()==0){
                arrListsExpanded.put(IntConst.ID_BOUGHT_CAT_ON, true);
            }
            arrLists.get(IntConst.ID_BOUGHT_CAT_ON).add(bli);
            rebuildGroups();
        } else {
            arrLists.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).remove(bli);

            if (arrLists.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).size() == 0) {
                groups.remove(0);
            }
            if(arrLists.get(IntConst.ID_BOUGHT_CAT_OFF).size()==0){
                groups.add(1, IntConst.ID_BOUGHT_CAT_OFF);
                arrListsExpanded.put(IntConst.ID_BOUGHT_CAT_OFF, true);
            }
            arrLists.get(IntConst.ID_BOUGHT_CAT_OFF).add(bli);
        }
        sortLists();
        notifyDataSetChanged();
    }

    public void moveFromBought(BuyListItem bli) {
        if (AppPreferences.categoryAvailable) {
            arrLists.get(IntConst.ID_BOUGHT_CAT_ON).remove(bli);
            if(arrLists.get(bli.getCategoryId()).size()==0){
                arrListsExpanded.put(bli.getCategoryId(), true);
            }
            arrLists.get(bli.getCategoryId()).add(bli);
            rebuildGroups();
        } else {
            arrLists.get(IntConst.ID_BOUGHT_CAT_OFF).remove(bli);

            if (arrLists.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).size() == 0) {
                arrListsExpanded.put(IntConst.ID_NOT_BOUGHT_CAT_OFF, true);
                groups.add(0, IntConst.ID_NOT_BOUGHT_CAT_OFF);
            }
            if(arrLists.get(IntConst.ID_BOUGHT_CAT_OFF).size()==0){
                groups.remove(1);
            }
            arrLists.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).add(bli);
        }
        sortLists();
        notifyDataSetChanged();
    }

    public void addCommodity(BuyListItem bli){
        if(AppPreferences.categoryAvailable){
            if(bli.isBought()){
                if(arrLists.get(IntConst.ID_BOUGHT_CAT_ON).size()==0){
                    groups.add(IntConst.ID_BOUGHT_CAT_ON);
                }
                arrLists.get(IntConst.ID_BOUGHT_CAT_ON).add(bli);
            } else {
                if(arrLists.get(bli.getCategoryId()).size()==0){
                    arrLists.get(bli.getCategoryId()).add(bli);
                    rebuildGroups();
                } else {
                    arrLists.get(bli.getCategoryId()).add(bli);
                }
            }
        } else {
            if(bli.isBought()){
                if(arrLists.get(IntConst.ID_BOUGHT_CAT_OFF).size()==0){
                    groups.add(IntConst.ID_BOUGHT_CAT_OFF);
                }
                arrLists.get(IntConst.ID_BOUGHT_CAT_OFF).add(bli);
            } else {
                if(arrLists.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).size()==0){
                    groups.add(0, IntConst.ID_NOT_BOUGHT_CAT_OFF);
                }
                arrLists.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).add(bli);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return arrLists.get(groups.get(groupPosition)).size();
    }

    @Override
    public ArrayList<BuyListItem> getGroup(int groupPosition) {
        return arrLists.get(groups.get(groupPosition));
    }

    @Override
    public BuyListItem getChild(int groupPosition, int childPosition) {
        return arrLists.get(groups.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView==null) {
            convertView = mInflater.inflate(R.layout.list_view_divider, null);
        }
        if(AppPreferences.categoryAvailable) {
            if(groups.get(groupPosition) != IntConst.ID_BOUGHT_CAT_ON) {
                ((TextView) convertView).setText(Utils.getTitle(arrLists.get(groups.get(groupPosition)).get(0).getCategoryId(), context));
            } else {
                ((TextView) convertView).setText(Utils.getTitle(IntConst.ID_BOUGHT_CAT_ON, context));
            }
        } else {
            ((TextView) convertView).setText(Utils.getTitle(groups.get(groupPosition), context));
        }
        ((TextView) convertView).setTextColor(AppPreferences.colorTheme_FontColor);
        convertView.setBackgroundColor(AppPreferences.colorTheme_Actionbar);

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.item_of_buy_list_item, parent, false);
            RelativeLayout root = (RelativeLayout) view.findViewById(R.id.Item_BLI_main_root);
            LinearLayout secondRoot = (LinearLayout) view.findViewById(R.id.Item_BLI_third_root);
            TextView TV_old = (TextView) view.findViewById(R.id.Item_BLI_thirdLayTitle);



            TextView TV_name = (TextView) view.findViewById(R.id.Item_BLI_name);
            TextView TV_comment = (TextView) view.findViewById(R.id.Item_BLI_comment);
            TextView TV_cost = (TextView) view.findViewById(R.id.Item_BLI_additional);
            ImageView IV_imgNew = (ImageView) view.findViewById(R.id.Item_BLI_iv_new);
            ImageView IV_imgPhoto = (ImageView) view.findViewById(R.id.Item_BLI_photo);
            RelativeLayout darkRoot = (RelativeLayout) view.findViewById(R.id.Item_BLI_shadow);
            TextView TV_bought = (TextView) view.findViewById(R.id.Item_BLI_bought);
            CheckBox removeChB = (CheckBox) view.findViewById(R.id.Item_BLI_delete_chb);

            ChildViewContainer viewContainer = new ChildViewContainer(root, secondRoot, TV_bought, TV_name, TV_comment, TV_cost, IV_imgNew, IV_imgPhoto, darkRoot, TV_old, removeChB);

            view.setTag(viewContainer);
        }

        ChildViewContainer viewContainer = (ChildViewContainer) view.getTag();

        final BuyListItem buyListItem = getChild(groupPosition, childPosition);

        RelativeLayout root = viewContainer.root;
        LinearLayout secondRoot = viewContainer.secondRoot;

        TextView TV_name = viewContainer.TV_name;
        TextView TV_comment = viewContainer.TV_comment;
        TextView TV_cost = viewContainer.TV_cost;
        ImageView IV_imgNew = viewContainer.IV_imgNew;
        ImageView IV_imgPhoto = viewContainer.IV_imgPhoto;

        TextView TV_secondRoot = viewContainer.TV_old;

        CheckBox removeChB = viewContainer.removeChB;

        removeChB.setVisibility(deleteMode ? View.VISIBLE : View.GONE);

        if(deleteMode){
            removeChB.setChecked(checked.get(groupPosition).get(childPosition));
        }

        IV_imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PreviewImageDialog(buyListItem.getPath()).show(fm, "PreviewImageDialog");
            }
        });

        if(buyListItem.getBmpPrev()!=null){
            IV_imgPhoto.setVisibility(View.VISIBLE);
            IV_imgPhoto.setImageBitmap(buyListItem.getBmpPrev());
        } else {
            IV_imgPhoto.setVisibility(View.GONE);
        }




        RelativeLayout darkRoot = viewContainer.darkRoot;
        TextView TV_bought = viewContainer.TV_bought;

        root.setTag(true);

        TV_name.setText(buyListItem.getName()+"");
        if(!buyListItem.getComment().equalsIgnoreCase("")) {
            TV_comment.setVisibility(View.VISIBLE);
            TV_comment.setText(buyListItem.getComment() + "");
        } else {
            TV_comment.setVisibility(View.GONE);
        }



        String tmp = "";

        tmp = tmp.concat(weightTypeTitleArray[buyListItem.getWeightType()])
                .concat(": ")
                .concat(String.valueOf(buyListItem.getWeight()))
                .concat(" ")
                .concat(weightTypeArray[buyListItem.getWeightType()]);

        if (AppPreferences.costAvailable && buyListItem.getCost() != 0) {
            tmp = tmp.concat("   ")
                    .concat(cost).concat(" ")
                    .concat(String.valueOf(buyListItem.getCost()))
                    .concat(" ")
                    .concat(AppPreferences.currency);
        }

        TV_cost.setText(tmp);


        secondRoot.setVisibility(View.GONE);
        root.setX(0);

        if(buyListItem.isBought()){
            TV_bought.setVisibility(View.VISIBLE);
            darkRoot.setVisibility(View.VISIBLE);
        } else {
            TV_bought.setVisibility(View.GONE);
            darkRoot.setVisibility(View.GONE);
        }
        TV_bought.setText(R.string.buy_list_title_bought);

        //SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.getDefault());
        //SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM", Locale.getDefault());
        //TV_lastSync.setText(sdf.format(new Date(buyListItem.date)));
        IV_imgNew.setImageDrawable(context.getResources()
                .getDrawable(R.drawable.image_new_tag));

        IV_imgNew.setVisibility(View.GONE);

        root.setBackgroundColor(buyListItem.getColorBck());

        TV_name.setTextColor(buyListItem.getColorTxt());
        TV_comment.setTextColor(buyListItem.getColorTxt());
        TV_cost.setTextColor(buyListItem.getColorTxt());
        return view;
    }

    private class ChildViewContainer {
        private RelativeLayout root;
        private LinearLayout secondRoot;
        private TextView TV_bought;
        private TextView TV_name;
        private TextView TV_comment;
        private TextView TV_cost;
        private ImageView IV_imgNew;
        private ImageView IV_imgPhoto;
        private RelativeLayout darkRoot;
        private TextView TV_old;
        private CheckBox removeChB;

        //Button TVsyncBtn;

        ChildViewContainer(RelativeLayout root, LinearLayout secondRoot, TextView tv_bought, TextView tv_name, TextView tv_comment, TextView tv_cost, ImageView iv_imgNew, ImageView iv_imgPhoto, RelativeLayout darkRoot, TextView tv_old, CheckBox removeChB){
            this.root = root;
            this.secondRoot = secondRoot;
            TV_bought = tv_bought;
            TV_name = tv_name;
            TV_comment = tv_comment;
            TV_cost = tv_cost;
            IV_imgNew = iv_imgNew;
            IV_imgPhoto = iv_imgPhoto;
            this.darkRoot = darkRoot;
            TV_old = tv_old;
            this.removeChB = removeChB;
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    //TODO PreviewImageDialog
    public class PreviewImageDialog extends DialogFragment {
        String path;
        PreviewImageDialog(String path){
            this.path = path;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.dialog_preview_image, container, false);
            View root = v.findViewById(R.id.DPI_btn_container);
            root.setVisibility(View.GONE);
            ImageView iv = (ImageView) v.findViewById(R.id.DPI_iv);


            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(path, options);
            int w = getResources().getDisplayMetrics().widthPixels;
            w-=w/10;
            bitmap = bitmap.createScaledBitmap(bitmap, w, w, false);
            iv.setImageBitmap(bitmap);

          //  getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {

            // the content
            final RelativeLayout root = new RelativeLayout(getActivity());
            root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            // creating the fullscreen dialog
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(root);

            return dialog;
        }
    }
}
