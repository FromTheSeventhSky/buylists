package ru.fromtheseventhsky.buylist.custom.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;


public class ColorCircle extends View {

    private Paint paint;
    private Paint linePaint;
    private Paint linePaintCyan;
    private Paint linePaintWhite;
    private int color;
    private int h;
    private int w;
    private boolean selected = false;
    private boolean add = false;

    public ColorCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
        linePaint = new Paint();
        linePaintCyan = new Paint();
        linePaintWhite = new Paint();
        color = Color.GRAY;
    }

    public ColorCircle(Context context) {
        super(context);
        paint = new Paint();
        linePaint = new Paint();
        linePaintCyan = new Paint();
        linePaintWhite = new Paint();
        color = Color.GREEN;
    }

    public ColorCircle(Context context, int h, int w) {
        super(context);
        paint = new Paint();
        linePaint = new Paint();
        linePaintCyan = new Paint();
        linePaintWhite = new Paint();
        color = Color.GREEN;
        this.h = h;
        this.w = w;
    }

    public ColorCircle(Context context, int color) {
        super(context);
        paint = new Paint();
        linePaint = new Paint();
        this.color = color;
    }

    public void setSelectedCC(boolean selected){
        this.selected = selected;

        invalidate();
        requestLayout();
    }

    public boolean isSelectedCC(){
        return selected;
    }

    public void setVisibleAdd(boolean add){
        this.add = add;

        invalidate();
        requestLayout();
    }

    public boolean isVisibleAdd(){
        return add;
    }

    public void setColor(int color){
        this.color = color;
        invalidate();
        requestLayout();
    }


    @Override
    public void onDraw(Canvas canvas) {
        paint.setColor(color);
        paint.setAntiAlias(true);

        float halfHeight = this.getMeasuredHeight() / 2;
        float halfWidth = this.getMeasuredWidth() / 2;

        float radius = 0;
        if(halfHeight > halfWidth)
            radius = halfWidth;
        else
            radius = halfHeight;


        canvas.drawCircle(halfWidth, halfHeight, radius-halfWidth/10, paint);

        if(selected && !add){
            linePaintCyan.setColor(Color.BLACK);
            linePaintCyan.setAntiAlias(true);
            linePaintCyan.setStrokeWidth(6);
            linePaintCyan.setStyle(Paint.Style.STROKE);
            linePaintWhite.setColor(Color.WHITE);
            linePaintWhite.setAntiAlias(true);
            linePaintWhite.setStrokeWidth(2);
            linePaintWhite.setStyle(Paint.Style.STROKE);
            canvas.drawCircle(halfWidth, halfHeight, radius-halfWidth/10, linePaintCyan);
            canvas.drawCircle(halfWidth, halfHeight, radius-halfWidth/10, linePaintWhite);
        }

        if(add){
            linePaint.setColor(Color.BLACK);
            linePaint.setAntiAlias(true);
            float mHeight = halfHeight/3;
            float mWidth = halfWidth/3;
            linePaint.setStrokeWidth(halfWidth/7);
            canvas.drawLine(halfHeight,halfHeight-mHeight,halfHeight,halfHeight+mHeight,linePaint);
            canvas.drawLine(halfHeight - mHeight, halfHeight, halfHeight + mHeight, halfHeight, linePaint);
        }

    }
}
