package ru.fromtheseventhsky.buylist.custom.views;


import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class BuyListViewPager  extends ViewPager {


    public BuyListViewPager(final Context _context, final AttributeSet attrs) {
        super(_context, attrs);
    }
    public BuyListViewPager(Context _context) {
        super(_context);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if(getCurrentItem()==0) {
            return false;
        } else {
            return super.onInterceptTouchEvent(event);
        }
    }
}
