package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.views.ColorCircle;
import ru.fromtheseventhsky.buylist.custom.views.TwoColorsCircle;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.ColorCircleContainer;
import ru.fromtheseventhsky.buylist.utils.TwiceColorCircleContainer;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class ColorGridAdapter extends BaseAdapter {

    private LayoutInflater mInflater;


    private boolean isBck;

    boolean isTwice;

    private ColorCircleContainer add;
    private int selectedPositionBck;
    private int selectedPositionTxt;
    private int selectedPositionTmp;
    private Context context;

    public void setSelectedPosition(int selectedPosition) {
        if(!isTwice) {
            if (isBck)
                this.selectedPositionBck = selectedPosition;
            else
                this.selectedPositionTxt = selectedPosition;
        } else {
            selectedPositionTmp = selectedPosition;
        }
    }
    public void setSelectedPositionTemplate(int selectedPosition) {
        selectedPositionTmp = selectedPosition;
    }
    public void setSelectedPositionBck(int selectedPosition, boolean isBck) {
        if (isBck)
            this.selectedPositionBck = selectedPosition;
        else
            this.selectedPositionTxt = selectedPosition;
    }

    public void setSelectedPositionBoth(int bck, int text, int both) {
        this.selectedPositionBck = bck;
        this.selectedPositionTxt = text;
        this.selectedPositionTmp = both;
        notifyDataSetChanged();
    }

    public int getSelectedPositionBck() {
        return this.selectedPositionBck;
    }
    public int getSelectedPositionTxt() {
        return this.selectedPositionTxt;
    }
    public int getSelectedPositionTemp() {
        return this.selectedPositionTmp;
    }

    public int getSelectedColor(boolean bck) {
        boolean bckTmp = isBck;
        int color;
        isBck = bck;
        if(bck)
            color = ((ColorCircleContainer)getItem(selectedPositionBck)).getColor();
        else
            color = ((ColorCircleContainer)getItem(selectedPositionTxt)).getColor();
        isBck = bckTmp;
        return color;

    }
    public int getSelectedColorTemplate(boolean first) {
        if(first) {
            return getItemTemp(selectedPositionTmp).getFirstColor();
        } else {
            return getItemTemp(selectedPositionTmp).getSecondColor();
        }
    }
    public TwiceColorCircleContainer getItemTemp(int position){
        if (position < Utils.orig.size()) {
            return Utils.origTemp.get(position);
        } else {
            return Utils.customTemp.get(position - Utils.origTemp.size());
        }
    }

    public void setBck(boolean isBck) {
        this.isBck = isBck;
    }
    public void setTemplate(boolean template) {
        this.isTwice = template;
    }

    public ColorGridAdapter(Context ctx) {
        context = ctx;
        isBck = true;
        isTwice = false;
        this.selectedPositionBck =  AppPreferences.defaultBckColor;
        this.selectedPositionTxt =  AppPreferences.defaultTextColor;
        this.selectedPositionTmp =  AppPreferences.defaultTempColor;
        mInflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        add = new ColorCircleContainer(true, Color.LTGRAY, true);
    }


    public void reset(){
        this.selectedPositionBck = AppPreferences.defaultBckColor;
        this.selectedPositionTxt = AppPreferences.defaultTextColor;
        this.selectedPositionTmp =  AppPreferences.defaultTempColor;
        isBck = true;
        isTwice = false;
        notifyDataSetChanged();
    }

    public void removeColor(int pos, boolean isBck, boolean isTwice){
        if(!isTwice) {
            if (isBck) {
                if (selectedPositionBck == pos) {
                    selectedPositionBck = AppPreferences.defaultBckColor;
                }
            } else {
                if (selectedPositionTxt == pos) {
                    selectedPositionTxt = AppPreferences.defaultTextColor;
                }
            }
        } else {
            if (selectedPositionTmp == pos) {
                selectedPositionTmp = AppPreferences.defaultTempColor;
            }
        }
    }


    @Override
    public int getCount() {
        if(!isTwice) {
            if (isBck) {
                return Utils.orig.size() + Utils.customBck.size() + 1;
            } else {
                return Utils.orig.size() + Utils.customTxt.size() + 1;
            }
        } else {
            return Utils.origTemp.size() + Utils.customTemp.size();
        }
    }

    @Override
    public Object getItem(int position) {
        if(!isTwice) {
            if (position < Utils.orig.size())
                return Utils.orig.get(position);
            if (isBck) {
                if (position >= Utils.orig.size() && position - Utils.orig.size() < Utils.customBck.size())
                    return Utils.customBck.get(position - Utils.orig.size());
            } else {
                if (position >= Utils.orig.size() && position - Utils.orig.size() < Utils.customTxt.size())
                    return Utils.customTxt.get(position - Utils.orig.size());
            }
            return add;
        } else {
            if (position < Utils.orig.size()) {
                return Utils.origTemp.get(position);
            } else {
                return Utils.customTemp.get(position - Utils.origTemp.size());
            }
        }
    }



    public void selectLastItem() {
        if(isBck)
            selectedPositionBck = Utils.orig.size()+Utils.customBck.size()+1;
        else
            selectedPositionTxt = Utils.orig.size()+Utils.customTxt.size()+1;
    }



    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(!isTwice) {
            ColorCircleContainer ccc = (ColorCircleContainer) getItem(position);
            view = mInflater.inflate(R.layout.color_circle, parent, false);


            int selectedPosition;


            if (isBck) {
                selectedPosition = selectedPositionBck;
            } else {
                selectedPosition = selectedPositionTxt;
            }


            if (selectedPosition == position) {
                ((ColorCircle) view).setSelectedCC(true);
            } else {
                if (((ColorCircle) view).isSelectedCC())
                    ((ColorCircle) view).setSelectedCC(false);
            }


            if (ccc.getIsAdd() == true) {
                ((ColorCircle) view).setVisibleAdd(true);
            } else {
                if (((ColorCircle) view).isVisibleAdd())
                    ((ColorCircle) view).setVisibleAdd(false);
            }

            ((ColorCircle) view).setColor(ccc.getColor());
        } else {
            TwiceColorCircleContainer tccc = (TwiceColorCircleContainer) getItem(position);
            view = mInflater.inflate(R.layout.twice_color_circle, parent, false);

            if (selectedPositionTmp == -1) {
                ((TwoColorsCircle) view).setSelectedCC(false);
            } else {
                if (selectedPositionTmp == position) {
                    ((TwoColorsCircle) view).setSelectedCC(true);
                } else {
                    if (((TwoColorsCircle) view).isSelectedCC()) {
                        ((TwoColorsCircle) view).setSelectedCC(false);
                    }
                }

            }
            ((TwoColorsCircle) view).setColor(tccc.getFirstColor(), tccc.getSecondColor());

        }
        return view;
    }
}
