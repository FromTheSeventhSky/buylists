package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.ProductInfo;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class PrefGoodsLVAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private String costTitle;
    private String categoryTitle;
    private String lastUsedTitle;
    private String unitTitle;
    private SimpleDateFormat sdf;
    private String[] weightTypeArray;
    private boolean[] checked;

    private boolean removeMode;

    public PrefGoodsLVAdapter(Context context){
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        costTitle = context.getString(R.string.prefs_cost_title);
        categoryTitle = context.getString(R.string.prefs_categories_title);
        lastUsedTitle = context.getString(R.string.prefs_last_used_title);
        unitTitle = context.getString(R.string.prefs_unit_title);
        //sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.getDefault());
        sdf = new SimpleDateFormat("dd MMMM", Locale.getDefault());
        weightTypeArray = context.getResources().getStringArray(R.array.weight_type);
        removeMode = false;
        checked = new boolean[Utils.productNames.size()];
        for (int i = 0; i < Utils.productNames.size(); i++) {
            checked[i] = false;
        }


    }



    public void setRemoveMode(boolean removeMode, int pos){
        this.removeMode = removeMode;
        checked = new boolean[Utils.productNames.size()];
        for (int i = 0; i < Utils.productNames.size(); i++) {
            if(pos == i)
                checked[i] = true;
            else
                checked[i] = false;
        }
        notifyDataSetChanged();
    }

    public void fillDeleteList(ArrayList<Integer> deleteList){
        deleteList.clear();
        for (int i = 0; i < Utils.productNames.size(); i++) {
            if(checked[i]==true)
                deleteList.add(i);
        }
    }

    public void setDeletePosition(int position, boolean isChecked){
        checked[position] = isChecked;
    }

    @Override
    public int getCount() {
        return Utils.productNames.size();
    }

    @Override
    public ProductInfo getItem(int position) {
        return Utils.availableProducts.get(Utils.productNames.get(position));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.pref_goods_lv_item, parent, false);
            convertView.setTag(new ViewContainer(
                    (TextView) convertView.findViewById(R.id.PrefGoodsLvItem_title),
                    (TextView) convertView.findViewById(R.id.PrefGoodsLvItem_cost),
                    (TextView) convertView.findViewById(R.id.PrefGoodsLvItem_unit),
                    (TextView) convertView.findViewById(R.id.PrefGoodsLvItem_category),
                    (TextView) convertView.findViewById(R.id.PrefGoodsLvItem_lastUsed),
                    (CheckBox) convertView.findViewById(R.id.PrefGoodsLvItem_chb)));
        }
        ViewContainer vk = (ViewContainer) convertView.getTag();
        ProductInfo pi = getItem(position);
        TextView name = vk.getName();
        TextView cost = vk.getCost();
        TextView unit = vk.getUnit();
        TextView category = vk.getCategory();
        TextView lastUsed = vk.getLastUsed();
        CheckBox remove = vk.getRemove();


        if(!AppPreferences.costAvailable){
            cost.setVisibility(View.GONE);
        } else {
            cost.setTextColor(AppPreferences.colorTheme_FontColor);
            cost.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefGoodsTitle);
            cost.setText(costTitle.concat(String.valueOf(pi.getCost())).concat(" ").concat(AppPreferences.currency));
        }
        if(!AppPreferences.categoryAvailable){
            category.setVisibility(View.GONE);
        } else {
            category.setTextColor(AppPreferences.colorTheme_FontColor);
            category.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefGoodsTitle);
            category.setText(categoryTitle.concat(Utils.goodsCategories.get(pi.getCategory()).getName()));
        }


        remove.setVisibility(removeMode ? View.VISIBLE : View.GONE);
        if(Utils.deleteMode) {
            remove.setChecked(checked[position]);
        }


        name.setTextColor(AppPreferences.colorTheme_FontColor);
        name.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefTitle);
        name.setText(pi.getName());


        unit.setTextColor(AppPreferences.colorTheme_FontColor);
        unit.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefGoodsTitle);
        unit.setText(unitTitle.concat(weightTypeArray[pi.getUnit()]));

        lastUsed.setTextColor(AppPreferences.colorTheme_FontColor);
        lastUsed.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefGoodsTitle);
        lastUsed.setText(lastUsedTitle.concat(String.valueOf(sdf.format(new Date(pi.getLastUsed())))));


        return convertView;
    }

    private class ViewContainer {
        private TextView name;
        private TextView cost;
        private TextView unit;
        private TextView category;
        private TextView lastUsed;
        private CheckBox remove;
        ViewContainer(TextView name, TextView cost, TextView unit, TextView category, TextView lastUsed, CheckBox remove){
            this.name = name;
            this.cost = cost;
            this.unit = unit;
            this.category = category;
            this.lastUsed = lastUsed;
            this.remove = remove;
        }


        public TextView getName() {
            return name;
        }

        public TextView getCost() {
            return cost;
        }

        public TextView getUnit() {
            return unit;
        }

        public TextView getCategory() {
            return category;
        }

        public TextView getLastUsed() {
            return lastUsed;
        }

        public CheckBox getRemove() {
            return remove;
        }
    }
}
