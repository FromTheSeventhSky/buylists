package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.IntConst;


public class PrefsListViewAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context context;

    boolean added;


    ArrayList<View> views;
    private int itemCount;

    public PrefsListViewAdapter(Context context){
        views = new ArrayList<View>();
        itemCount = 18;
        this.context = context;
        added = false;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    public void changeDividerColor(int position, int color){
        View view = views.get(position);
        TextView pref_divider = (TextView) view.findViewById(R.id.PREF_divider);
        pref_divider.setBackgroundColor(color);
    }

    public void changeChbComment(int position, boolean state){
        View view = views.get(position);
        TextView comment = (TextView) view.findViewById(R.id.PREF_chb_comment);
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.PREF_chb_chb);
        switch (position) {
            case IntConst.USE_COST: {
                if(!state) {
                    AppPreferences.costAvailable = true;
                    checkBox.setChecked(true);
                    comment.setText(R.string.prefs_cost_enable);
                } else {
                    AppPreferences.costAvailable = false;
                    checkBox.setChecked(false);
                    comment.setText(R.string.prefs_cost_disable);
                }
                notifyDataSetChanged();
            }
            break;
            case IntConst.USE_CATEGORIES: {
                if(!state) {
                    AppPreferences.categoryAvailable = true;
                    checkBox.setChecked(true);
                    comment.setText(R.string.prefs_categories_enable);
                } else {
                    AppPreferences.categoryAvailable = false;
                    checkBox.setChecked(false);
                    comment.setText(R.string.prefs_categories_disable);
                }
                notifyDataSetChanged();
            }
            break;
            case IntConst.VIBRATION: {
                if(!state) {
                    AppPreferences.vibrationEnabled = true;
                    checkBox.setChecked(true);
                    comment.setText(R.string.prefs_vibration_on);
                } else {
                    AppPreferences.vibrationEnabled = false;
                    checkBox.setChecked(false);
                    comment.setText(R.string.prefs_vibration_off);
                }
            }
            break;
            case IntConst.REMEMBER: {
                if(!state) {
                    AppPreferences.remember = true;
                    checkBox.setChecked(true);
                    comment.setText(R.string.prefs_remember_goods_yes);
                } else {
                    AppPreferences.remember = false;
                    checkBox.setChecked(false);
                    comment.setText(R.string.prefs_remember_goods_no);
                }
            }
            break;
            case IntConst.LIGHT_SCREEN: {
                if(!state) {
                    AppPreferences.screenLightAlways = true;
                    checkBox.setChecked(true);
                    comment.setText(R.string.prefs_dont_off_display_yes);
                } else {
                    AppPreferences.screenLightAlways = false;
                    checkBox.setChecked(false);
                    comment.setText(R.string.prefs_dont_off_display_no);
                }
            }
            break;
        }
    }

    public void changePopupComment(int position, String text){
        View view = views.get(position);
        TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);
        comment.setText(text);
    }


    private void addViewsToContainer(ViewGroup parent){
        for(int position = 0; position < itemCount; position++) {
            View view = null;
            switch (position) {
                case IntConst.DIVIDER_1: {
                    view = mInflater.inflate(R.layout.pref_item_divider, parent, false);
                    TextView pref_divider = (TextView) view.findViewById(R.id.PREF_divider);

                    pref_divider.setBackgroundColor(AppPreferences.colorTheme_Actionbar);

                    pref_divider.setText(R.string.prefs_title_appearance);
                }
                break;
                case IntConst.COLOR_THEME: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);
                    String[] arr = context.getResources().getStringArray(R.array.color_themes);
                    name.setText(R.string.prefs_color_theme);
                    comment.setText(arr[AppPreferences.colorThemeNumber]);
                }
                break;
                case IntConst.FONT_SIZE: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);
                    String[] arr = context.getResources().getStringArray(R.array.font_sizes);
                    name.setText(R.string.prefs_font_size);
                    comment.setText(arr[AppPreferences.fontSize]);
                }
                break;

                case IntConst.DIVIDER_2: {
                    view = mInflater.inflate(R.layout.pref_item_divider, parent, false);
                    TextView pref_divider = (TextView) view.findViewById(R.id.PREF_divider);

                    pref_divider.setBackgroundColor(AppPreferences.colorTheme_Actionbar);
                    pref_divider.setText(R.string.prefs_title_buy_list);
                }
                break;
                case IntConst.USE_CATEGORIES: {
                    view = mInflater.inflate(R.layout.pref_item_checkbox, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_chb_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_chb_comment);
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.PREF_chb_chb);

                    checkBox.setChecked(AppPreferences.categoryAvailable);

                    name.setText(R.string.prefs_categories);
                    if(AppPreferences.categoryAvailable)
                        comment.setText(R.string.prefs_categories_enable);
                    else
                        comment.setText(R.string.prefs_categories_disable);
                }
                break;
                case IntConst.CATEGORIES: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);

                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);

                    name.setText(R.string.prefs_categories_edit);
                    comment.setVisibility(View.GONE);
                    if (AppPreferences.categoryAvailable) {
                        view.setEnabled(AppPreferences.categoryAvailable);
                    } else {
                        view.setEnabled(AppPreferences.categoryAvailable);
                    }
                }
                break;
                case IntConst.USE_COST: {
                    view = mInflater.inflate(R.layout.pref_item_checkbox, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_chb_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_chb_comment);
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.PREF_chb_chb);

                    checkBox.setChecked(AppPreferences.costAvailable);

                    name.setText(R.string.prefs_cost);
                    if(AppPreferences.costAvailable)
                        comment.setText(R.string.prefs_cost_enable);
                    else
                        comment.setText(R.string.prefs_cost_disable);
                }
                break;
                case IntConst.CURRENCY: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);

                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);
                    name.setText(R.string.prefs_currency);
                    comment.setText(AppPreferences.currency);
                    if (AppPreferences.costAvailable) {
                        view.setEnabled(AppPreferences.costAvailable);
                    } else {
                        view.setEnabled(AppPreferences.costAvailable);
                    }
                }
                break;
                case IntConst.COLOR_DEFAULT: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);

                    name.setText(R.string.prefs_default_color);
                    comment.setVisibility(View.GONE);
                }
                break;
                case IntConst.DIVIDER_3: {
                    view = mInflater.inflate(R.layout.pref_item_divider, parent, false);
                    TextView pref_divider = (TextView) view.findViewById(R.id.PREF_divider);

                    pref_divider.setBackgroundColor(AppPreferences.colorTheme_Actionbar);
                    pref_divider.setText(R.string.prefs_title_application);
                }
                break;
                case IntConst.VIBRATION: {
                    view = mInflater.inflate(R.layout.pref_item_checkbox, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_chb_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_chb_comment);
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.PREF_chb_chb);

                    checkBox.setChecked(AppPreferences.vibrationEnabled);

                    name.setText(R.string.prefs_vibration);
                    if(AppPreferences.vibrationEnabled)
                        comment.setText(R.string.prefs_vibration_on);
                    else
                        comment.setText(R.string.prefs_vibration_off);
                }
                break;
                case IntConst.REMEMBER: {
                    view = mInflater.inflate(R.layout.pref_item_checkbox, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_chb_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_chb_comment);
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.PREF_chb_chb);

                    checkBox.setChecked(AppPreferences.remember);

                    name.setText(R.string.prefs_remember_goods);
                    if(AppPreferences.remember)
                        comment.setText(R.string.prefs_remember_goods_yes);
                    else
                        comment.setText(R.string.prefs_remember_goods_no);
                }
                break;
                case IntConst.REMOVE_OLD: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);

                    name.setText(R.string.prefs_remove_old);
                    comment.setText(R.string.prefs_remove_old_no);
                }
                break;
                case IntConst.LIGHT_SCREEN: {
                    view = mInflater.inflate(R.layout.pref_item_checkbox, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_chb_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_chb_comment);
                    CheckBox checkBox = (CheckBox) view.findViewById(R.id.PREF_chb_chb);


                    checkBox.setChecked(AppPreferences.screenLightAlways);
                    name.setText(R.string.prefs_dont_off_display);
                    if(AppPreferences.screenLightAlways)
                        comment.setText(R.string.prefs_dont_off_display_yes);
                    else
                        comment.setText(R.string.prefs_dont_off_display_no);
                }
                break;
                case IntConst.DIVIDER_4: {
                    view = mInflater.inflate(R.layout.pref_item_divider, parent, false);
                    TextView pref_divider = (TextView) view.findViewById(R.id.PREF_divider);

                    pref_divider.setBackgroundColor(AppPreferences.colorTheme_Actionbar);

                    pref_divider.setText(R.string.prefs_title_backup);
                }
                break;
                case IntConst.BACKUP_CREATE: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);

                    name.setText(R.string.prefs_create_copy);
                    comment.setVisibility(View.GONE);
                }
                break;
                case IntConst.BACKUP_RECOVER: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);
                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);

                    name.setText(R.string.prefs_use_copy);
                    comment.setVisibility(View.GONE);
                }
                break;
                case IntConst.GOODS: {
                    view = mInflater.inflate(R.layout.pref_item_popup, parent, false);

                    TextView name = (TextView) view.findViewById(R.id.PREF_popup_name);
                    TextView comment = (TextView) view.findViewById(R.id.PREF_popup_comment);

                    name.setText(R.string.prefs_goods_list);
                    comment.setVisibility(View.GONE);
                }
                break;
            }
            views.add(view);
        }
        added = true;
    }


    @Override
    public int getCount() {
        return itemCount;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(!added) {
            addViewsToContainer(parent);
        }
        View view = views.get(position);

        if(position==IntConst.CATEGORIES){
            view.setEnabled(AppPreferences.categoryAvailable);
            view.setBackgroundColor(AppPreferences.categoryAvailable ? Color.TRANSPARENT : Color.LTGRAY);
        }
        if(position==IntConst.CURRENCY){
            view.setEnabled(AppPreferences.costAvailable);
            view.setBackgroundColor(AppPreferences.costAvailable ? Color.TRANSPARENT : Color.LTGRAY);
        }
        return view;
    }
}
