package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;


public class MainPageLongClickPopupAdapter extends BaseAdapter {

    private LayoutInflater mInflater;

    public MainPageLongClickPopupAdapter(Context context){
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv = (TextView) mInflater.inflate(R.layout.main_page_longclick_popup_item, parent, false);
        tv.setTextColor(AppPreferences.colorTheme_FontColor);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefTitle);
        switch (position){
            case 0:
                tv.setText(R.string.mp_popup_edit);
                break;
            case 1:
                tv.setText(R.string.mp_popup_delete);
                break;
        }


        return tv;
    }
}
