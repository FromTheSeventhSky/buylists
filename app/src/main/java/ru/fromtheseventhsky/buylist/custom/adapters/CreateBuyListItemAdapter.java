package ru.fromtheseventhsky.buylist.custom.adapters;


import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.BuyListItem;

public class CreateBuyListItemAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context context;

    private ArrayList<BuyListItem> originalBuyListItems;
    private ArrayList<BuyListItem> buyListItems;
    private DisplayMetrics dm;

    public CreateBuyListItemAdapter(Context context, ArrayList<BuyListItem> buyListItems, DisplayMetrics dm) {
        this.context = context;
        mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.buyListItems = buyListItems;
        originalBuyListItems = buyListItems;
        this.dm = dm;
    }

    public void setNewList(ArrayList<BuyListItem> buyListItems){
        this.buyListItems = buyListItems;
        originalBuyListItems = buyListItems;
        notifyDataSetChanged();
    }

    public void addNewItem(BuyListItem item){
        buyListItems.add(0, item);
        originalBuyListItems = buyListItems;
    }

    @Override
    public int getCount() {
        return buyListItems.size();
    }

    @Override
    public Object getItem(int position) {
        return  buyListItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.item_of_buy_list_item, parent, false);

           /* RelativeLayout root = (RelativeLayout) view.findViewById(R.id.Item_BLI_main_root);
            RelativeLayout rootSecond = (RelativeLayout) view.findViewById(R.id.Item_BLI_second_root);
            RelativeLayout rootThird = (RelativeLayout) view.findViewById(R.id.Item_BLI_third_root);

            TextView TVname = (TextView) view.findViewById(R.id.Item_BLI_name);
            TextView TVcomment = (TextView) view.findViewById(R.id.Item_BLI_comment);
            TextView TVcost = (TextView) view.findViewById(R.id.Item_BLI_cost);
            TextView TVcurrency = (TextView) view.findViewById(R.id.Item_BLI_currency);
            TextView TVweight = (TextView) view.findViewById(R.id.Item_BLI_weight);
            TextView TVweightType = (TextView) view.findViewById(R.id.Item_BLI_weightType);
            TextView TVdate = (TextView) view.findViewById(R.id.Item_BLI_date);


            TextView TVtitleChangeCurrency = (TextView) view.findViewById(R.id.Item_BLI_cost_title);
            TextView TVtitleChangeWeight = (TextView) view.findViewById(R.id.Item_BLI_weight_title);
            EditText ETchangeCost = (EditText) view.findViewById(R.id.Item_BLI_edit_cost);
            EditText ETchangeWeight = (EditText) view.findViewById(R.id.Item_BLI_edit_weight);
            Spinner SpinnerCurrency = (Spinner) view.findViewById(R.id.Item_BLI_currency_choose);
            Spinner SpinnerWeight = (Spinner) view.findViewById(R.id.Item_BLI_weight_choose);
            Button BTNaccept = (Button) view.findViewById(R.id.Item_BLI_button_accept);

            TextView TVtitleThirdLay = (TextView) view.findViewById(R.id.Item_BLI_thirdLayTitle);

            ViewContainer viewContainer = new ViewContainer(
                    root,
                    rootSecond,
                    rootThird,
                    TVname,
                    TVcomment,
                    TVcost,
                    TVcurrency,
                    TVweight,
                    TVweightType,
                    TVdate,
                    TVtitleChangeCurrency,
                    TVtitleChangeWeight,
                    ETchangeCost,
                    ETchangeWeight,
                    SpinnerCurrency,
                    SpinnerWeight,
                    BTNaccept,
                    TVtitleThirdLay
            );

            view.setTag(viewContainer);*/
        }

        ViewContainer viewContainer = (ViewContainer) view.getTag();

        BuyListItem bl = (BuyListItem) getItem(position);

        RelativeLayout root = viewContainer.root;
        RelativeLayout rootSecond = viewContainer.rootSecond;
        RelativeLayout rootThird = viewContainer.rootThird;
        TextView TVname = viewContainer.TVname;
        TextView TVcomment = viewContainer.TVcomment;
        TextView TVcost = viewContainer.TVcost;
        TextView TVcurrency = viewContainer.TVcurrency;
        TextView TVweight = viewContainer.TVweight;
        TextView TVweightType = viewContainer.TVweightType;
        TextView TVdate = viewContainer.TVdate;

        TextView TVtitleChangeCurrency = viewContainer.TVtitleChangeCurrency;
        TextView TVtitleChangeWeight = viewContainer.TVtitleChangeWeight;
        EditText ETchangeCost = viewContainer.ETchangeCost;
        EditText ETchangeWeight = viewContainer.ETchangeWeight;
        Spinner SpinnerCurrency = viewContainer.SpinnerCurrency;
        Spinner SpinnerWeight = viewContainer.SpinnerWeight;
        Button BTNaccept = viewContainer.BTNaccept;



        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, context.getResources().getStringArray(R.array.weight_type));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        SpinnerCurrency.setAdapter(adapter);
        SpinnerWeight.setAdapter(adapter);

        TextView TVtitleThirdLay = viewContainer.TVtitleThirdLay;


        return view;
    }

    private class ViewContainer{
       RelativeLayout root;
       RelativeLayout rootSecond;
       RelativeLayout rootThird;
       TextView TVname;
       TextView TVcomment;
       TextView TVcost;
       TextView TVcurrency;
       TextView TVweight;
       TextView TVweightType;
       TextView TVdate;

       TextView TVtitleChangeCurrency;
       TextView TVtitleChangeWeight;
       EditText ETchangeCost;
       EditText ETchangeWeight;
       Spinner SpinnerCurrency;
       Spinner SpinnerWeight;
       Button BTNaccept;


       TextView TVtitleThirdLay;


        private ViewContainer(RelativeLayout root, RelativeLayout rootSecond, RelativeLayout rootThird, TextView tVname, TextView tVcomment, TextView tVcost, TextView tVcurrency, TextView tVweight, TextView tVweightType, TextView tVdate, TextView tVtitleChangeCurrency, TextView tVtitleChangeWeight, EditText eTchangeCost, EditText eTchangeWeight, Spinner spinnerCurrency, Spinner spinnerWeight, Button btNaccept, TextView tVtitleThirdLay) {
            this.root = root;
            this.rootSecond = rootSecond;
            this.rootThird = rootThird;
            TVname = tVname;
            TVcomment = tVcomment;
            TVcost = tVcost;
            TVcurrency = tVcurrency;
            TVweight = tVweight;
            TVweightType = tVweightType;
            TVdate = tVdate;

            TVtitleChangeCurrency = tVtitleChangeCurrency;
            TVtitleChangeWeight = tVtitleChangeWeight;
            ETchangeCost = eTchangeCost;
            ETchangeWeight = eTchangeWeight;
            SpinnerCurrency = spinnerCurrency;
            SpinnerWeight = spinnerWeight;
            BTNaccept = btNaccept;
            TVtitleThirdLay = tVtitleThirdLay;
        }
    }
}
