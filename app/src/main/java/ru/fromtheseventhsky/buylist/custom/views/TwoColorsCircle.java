package ru.fromtheseventhsky.buylist.custom.views;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class TwoColorsCircle extends View {

    private Paint firstHalfCircle;
    private Paint secondHalfCircle;
    private Paint borderWhite;
    private Paint borderBlack;
    private int firstColor;
    private int secondColor;
    private int h;
    private int w;
    private boolean selected = false;


    public TwoColorsCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        firstHalfCircle = new Paint();
        secondHalfCircle = new Paint();
        borderWhite = new Paint();
        borderBlack = new Paint();
        firstColor = Color.GREEN;
        secondColor = Color.BLUE;
        borderWhite.setColor(Color.WHITE);
        borderBlack.setColor(Color.BLACK);

        borderWhite.setAntiAlias(true);
        borderBlack.setAntiAlias(true);
        firstHalfCircle.setAntiAlias(true);
        secondHalfCircle.setAntiAlias(true);
        firstHalfCircle.setColor(firstColor);
        secondHalfCircle.setColor(secondColor);
        borderBlack.setStrokeWidth(6);
        borderBlack.setStyle(Paint.Style.STROKE);
        borderWhite.setStrokeWidth(2);
        borderWhite.setStyle(Paint.Style.STROKE);
    }

    public TwoColorsCircle(Context context) {
        super(context);
        firstHalfCircle = new Paint();
        secondHalfCircle = new Paint();
        borderWhite = new Paint();
        borderBlack = new Paint();
        firstColor = Color.GREEN;
        secondColor = Color.BLUE;
        borderWhite.setColor(Color.WHITE);
        borderBlack.setColor(Color.BLACK);
        borderWhite.setAntiAlias(true);
        borderBlack.setAntiAlias(true);
        firstHalfCircle.setAntiAlias(true);
        secondHalfCircle.setAntiAlias(true);
        firstHalfCircle.setColor(firstColor);
        secondHalfCircle.setColor(secondColor);
        borderBlack.setStrokeWidth(6);
        borderBlack.setStyle(Paint.Style.STROKE);
        borderWhite.setStrokeWidth(2);
        borderWhite.setStyle(Paint.Style.STROKE);
    }

    public TwoColorsCircle(Context context, int h, int w) {
        super(context);
        firstHalfCircle = new Paint();
        secondHalfCircle = new Paint();
        borderWhite = new Paint();
        borderBlack = new Paint();
        firstColor = Color.GREEN;
        this.h = h;
        this.w = w;
        borderWhite.setColor(Color.WHITE);
        borderBlack.setColor(Color.BLACK);
        borderWhite.setAntiAlias(true);
        borderBlack.setAntiAlias(true);
        firstHalfCircle.setAntiAlias(true);
        secondHalfCircle.setAntiAlias(true);
        borderBlack.setStrokeWidth(6);
        borderBlack.setStyle(Paint.Style.STROKE);
        borderWhite.setStrokeWidth(2);
        borderWhite.setStyle(Paint.Style.STROKE);
    }

    public TwoColorsCircle(Context context, int firstColor, int secondColor, Integer r) {
        super(context);
        firstHalfCircle = new Paint();
        secondHalfCircle = new Paint();
        borderWhite = new Paint();
        borderBlack = new Paint();
        this.firstColor = firstColor;
        this.secondColor = secondColor;
        borderWhite.setColor(Color.WHITE);
        borderBlack.setColor(Color.BLACK);
        borderWhite.setAntiAlias(true);
        borderBlack.setAntiAlias(true);
        firstHalfCircle.setAntiAlias(true);
        secondHalfCircle.setAntiAlias(true);
        borderBlack.setStrokeWidth(6);
        borderBlack.setStyle(Paint.Style.STROKE);
        borderWhite.setStrokeWidth(2);
        borderWhite.setStyle(Paint.Style.STROKE);
    }

    public void setSelectedCC(boolean selected){
        this.selected = selected;

        invalidate();
        requestLayout();
    }

    public boolean isSelectedCC(){
        return selected;
    }


    public void setColor(int firstColor, int secondColor){
        this.firstColor = firstColor;
        this.secondColor = secondColor;

        firstHalfCircle.setColor(firstColor);
        secondHalfCircle.setColor(secondColor);

        invalidate();
        requestLayout();
    }


    @Override
    public void onDraw(Canvas canvas) {

        float halfHeight = this.getMeasuredHeight() / 2;
        float halfWidth = this.getMeasuredWidth() / 2;

        float radius = 0;
        if(halfHeight > halfWidth)
            radius = halfWidth;
        else
            radius = halfHeight;
        int tenth = (int) (radius/10);
        firstHalfCircle.setColor(firstColor);
        secondHalfCircle.setColor(secondColor);
        canvas.drawArc(new RectF(tenth, tenth, 2*radius - tenth, 2*radius -tenth), 270, 180, true, secondHalfCircle);
        canvas.drawArc(new RectF(tenth, tenth, 2*radius - tenth,  2*radius-tenth), 90, 180, true, firstHalfCircle);

        if(selected){
            canvas.drawCircle(halfWidth, halfHeight, radius-halfWidth/10, borderBlack);
            canvas.drawCircle(halfWidth, halfHeight, radius-halfWidth/10, borderWhite);
        }

    }
}
