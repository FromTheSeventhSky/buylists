package ru.fromtheseventhsky.buylist.custom.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ru.fromtheseventhsky.buylist.fragments.BuyListFragment;
import ru.fromtheseventhsky.buylist.fragments.FragmentCreateBuyListItem;


public class BuyListViewPagerCustomAdapter extends FragmentPagerAdapter {
    private FragmentCreateBuyListItem fragmentCreateBuyListItem;
    private BuyListFragment buyListFragment;

    private int count = 2;

    public BuyListViewPagerCustomAdapter(FragmentManager fm, BuyListFragment buyListFragment, FragmentCreateBuyListItem fragmentCreateBuyListItem) {
        super(fm);
        this.fragmentCreateBuyListItem = fragmentCreateBuyListItem;
        this.buyListFragment = buyListFragment;
    }

    @Override
    public Fragment getItem(int position) {
        //return null;
        if (position == 0)
            return buyListFragment;
        return fragmentCreateBuyListItem;
    }


    @Override
    public int getCount() {
        return count;
    }
}