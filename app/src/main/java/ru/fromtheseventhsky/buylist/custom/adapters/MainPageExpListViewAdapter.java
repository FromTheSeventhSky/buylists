package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyList;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class MainPageExpListViewAdapter extends BaseExpandableListAdapter {

    private ArrayList<ArrayList<BuyList>> arrLists;

    private ArrayList<Integer> groups;


    private LayoutInflater mInflater;
    private Context context;
    private int movedPosition;


    public MainPageExpListViewAdapter(ArrayList<ArrayList<BuyList>> arrLists, ArrayList<Integer> groups, Context context){
        this.groups = groups;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arrLists = arrLists;
        this.context = context;
        movedPosition = 0;

    }

    public void setNewLists(ArrayList<ArrayList<BuyList>> arrLists, ArrayList<Integer> groups){
        this.arrLists = arrLists;
        this.groups = groups;
    }





    public void addIndex(int num){
        if(num == 9) {
            groups.add(9);
            movedPosition = groups.size()-1;
            return;
        }
        if(num == 0) {
            groups.add(0, 0);
            movedPosition = 0;
            return;
        }
        if(groups.size()==1) {
            if (groups.get(0) > num) {
                groups.add(0, num);
                movedPosition = 0;
                return;
            }
        } else {
            for (int ctr = 0; ctr < groups.size() - 1; ctr++) {
                if (groups.get(ctr) < num && groups.get(ctr + 1) > num) {
                    movedPosition = ctr;
                    groups.add(ctr, num);
                    return;
                }
            }
        }
        groups.add(num);
    }

    public void addBuyList(int num, BuyList item){
        if(arrLists.get(num).size()==0) {
            addIndex(num);
        } else {
            for (int ctr = 0; ctr < groups.size(); ctr++) {
                if(groups.get(ctr)==num){
                    movedPosition = ctr;
                }
            }
        }
        arrLists.get(num).add(item);
        sortData(arrLists.get(num));
        notifyDataSetChanged();
    }

    public void removeBuyList(int groupPosition, int childPosition){
        arrLists.get(groups.get(groupPosition)).remove(childPosition);
        if(arrLists.get(groups.get(groupPosition)).size()==0)
            groups.remove(groupPosition);
        notifyDataSetChanged();
    }


    void sortData(ArrayList<BuyList> buyList) {
        Collections.sort(buyList, new Comparator<BuyList>() {
            @Override
            public int compare(BuyList obj1, BuyList ob2) {
                if (obj1.date > ob2.date) {
                    return -1;
                }
                if (obj1.date < ob2.date) {
                    return 1;
                }
                return 0;
            }
        });
    }

    public int moveFromOld(int groupPosition, int childPosition){
        Utils.reFillTodayAndWeekArrList();
        BuyList tmp = getChild(groupPosition, childPosition);
        int typeCtr;
        removeBuyList(groupPosition, childPosition);
        if (tmp.date < Utils.todayAndWeekArrList.get(8)) {
            addBuyList(8, tmp);
        } else {
            for (int i = 0; i < 8; i++) {
                if(tmp.date < Utils.todayAndWeekArrList.get(i) && tmp.date > Utils.todayAndWeekArrList.get(i+1)) {
                    addBuyList(i, tmp);
                    break;
                }
            }
        }
        return movedPosition;

    }

    public void moveToOld(int groupPosition, int childPosition){

        BuyList tmp = getChild(groupPosition, childPosition);
        addBuyList(9, tmp);
        removeBuyList(groupPosition, childPosition);
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return arrLists.get(groups.get(groupPosition)).size();
    }

    @Override
    public ArrayList<BuyList> getGroup(int groupPosition) {
        return arrLists.get(groupPosition);
    }
    public ArrayList<BuyList> getGroupRight(int groupPosition) {
        return arrLists.get(groups.get(groupPosition));
    }

    @Override
    public BuyList getChild(int groupPosition, int childPosition) {
        return arrLists.get(groups.get(groupPosition)).get(childPosition);
    }

    public void removeBL(int groupPosition, int childPosition){
        arrLists.get(groups.get(groupPosition)).remove(childPosition);
        if(arrLists.get(groups.get(groupPosition)).size()==0) {
            groups.remove(groupPosition);
        }

        notifyDataSetChanged();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if(convertView==null) {
            convertView = mInflater.inflate(R.layout.list_view_divider, null);
        }
        String text = Utils.getTitleWeek(groups.get(groupPosition), context);

        if(/*groupPosition > 1 && */groups.get(groupPosition) < 9){
            BuyList bl = getChild(groupPosition, 0);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM", Locale.getDefault());
            text = text.concat(" - ").concat(sdf.format(new Date(bl.date)));
        }
        ((TextView) convertView).setText(text);
        ((TextView) convertView).setTextColor(AppPreferences.colorTheme_FontColor);
        convertView.setBackgroundColor(AppPreferences.colorTheme_Actionbar);

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.item_of_list_of_buy_list, parent, false);
            RelativeLayout darkRoot = (RelativeLayout) view.findViewById(R.id.list_of_buy_list_item_dark_back);
            RelativeLayout root = (RelativeLayout) view.findViewById(R.id.list_of_buy_list_item_root);
            RelativeLayout rootSecond = (RelativeLayout) view.findViewById(R.id.list_of_buy_list_item_root_second);
            TextView TVold = (TextView) view.findViewById(R.id.list_of_buy_list_item_text_old);
            TextView TVname = (TextView) view.findViewById(R.id.list_of_buy_list_item_name);
            TextView TVcomment = (TextView) view.findViewById(R.id.list_of_buy_list_item_comment);
            TextView TVpositions = (TextView) view.findViewById(R.id.list_of_buy_list_item_positions);
            ImageView TVimgNew = (ImageView) view.findViewById(R.id.list_of_buy_list_item_image_new);
            Button TVsyncBtn = (Button) view.findViewById(R.id.list_of_buy_list_item_sync);
            ChildViewContainer viewContainer = new ChildViewContainer(darkRoot, root, rootSecond, TVold, TVname, TVcomment, TVpositions, TVimgNew, TVsyncBtn);

            view.setTag(viewContainer);
        }

        ChildViewContainer viewContainer = (ChildViewContainer) view.getTag();

        BuyList bl = getChild(groupPosition, childPosition);

        RelativeLayout darkRoot = viewContainer.darkRoot;
        RelativeLayout root = viewContainer.root;
        RelativeLayout rootSecond = viewContainer.rootSecond;
        TextView TVold = viewContainer.TVold;
        TextView TVname = viewContainer.TVname;
        TextView TVcomment = viewContainer.TVcomment;
        TextView TVpositions = viewContainer.TVpositions;
        ImageView TVimgNew = viewContainer.TVimgNew;
        Button TVsyncBtn = viewContainer.TVsyncBtn;



        root.setTag(true);

        TVname.setText(bl.name+"");
        if(bl.comment.equalsIgnoreCase("")){
            TVcomment.setVisibility(View.GONE);
        } else {
            TVcomment.setVisibility(View.VISIBLE);
            TVcomment.setText(bl.comment + "");
        }

        rootSecond.setVisibility(View.GONE);
        root.setX(0);

        if(bl.type==2){
            TVold.setVisibility(View.VISIBLE);
            darkRoot.setVisibility(View.VISIBLE);
        } else {
            TVold.setVisibility(View.GONE);
            darkRoot.setVisibility(View.GONE);
        }


        //SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale.getDefault());
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM", Locale.getDefault());
        String positions = context.getString(R.string.positions);
        String blCost = context.getString(R.string.cost);
        if(AppPreferences.costAvailable) {
            TVpositions.setText(positions
                    .concat(" ")
                    .concat(String.valueOf(bl.positions))
                    .concat("/")
                    .concat(String.valueOf(bl.bought))
                    .concat(" ")
                    .concat(blCost)
                    .concat(" ")
                    .concat(String.valueOf(bl.cost))
                    .concat(" ")
                    .concat(AppPreferences.currency));
        } else {
            TVpositions.setText(positions
                    .concat(" ")
                    .concat(String.valueOf(bl.positions))
                    .concat("/")
                    .concat(String.valueOf(bl.bought)));
        }
        TVimgNew.setImageDrawable(context.getResources()
                .getDrawable(R.drawable.image_new_tag));
        TVsyncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "OLOLO", Toast.LENGTH_SHORT).show();
            }
        });

        //root.setBackgroundResource(R.drawable.selector_item);
        root.setBackgroundColor(bl.colorBck);

        TVname.setTextColor(bl.colorTxt);
        TVcomment.setTextColor(bl.colorTxt);
        TVpositions.setTextColor(bl.colorTxt);


        return view;
    }



    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ChildViewContainer {
        RelativeLayout root;
        RelativeLayout rootSecond;
        TextView TVold;
        TextView TVname;
        TextView TVcomment;
        TextView TVpositions;
        ImageView TVimgNew;
        Button TVsyncBtn;
        RelativeLayout darkRoot;
        ChildViewContainer(
                RelativeLayout darkRoot,
                RelativeLayout root,
                RelativeLayout rootSecond,
                TextView TVold,
                TextView TVname,
                TextView TVcomment,
                TextView TVpositions,
                ImageView TVimgNew,
                Button TVsyncBtn){
            this.TVold = TVold;
            this.darkRoot = darkRoot;
            this.root = root;
            this.rootSecond = rootSecond;
            this.TVname = TVname;
            this.TVcomment = TVcomment;
            this.TVpositions = TVpositions;
            this.TVimgNew = TVimgNew;
            this.TVsyncBtn = TVsyncBtn;
        }
    }

}
