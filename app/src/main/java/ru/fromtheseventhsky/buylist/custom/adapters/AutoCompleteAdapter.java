package ru.fromtheseventhsky.buylist.custom.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class AutoCompleteAdapter extends BaseAdapter implements Filterable {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<String> productNames;

    public AutoCompleteAdapter(Context context){
        this.context = context;
        productNames = new ArrayList<String>();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return productNames.size();
    }

    @Override
    public String getItem(int position) {
        return productNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflater.inflate(R.layout.auto_complete_tv_lay, parent, false);
        }
        TextView tv = (TextView) convertView;

        tv.setTextColor(AppPreferences.colorTheme_FontColor);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        tv.setText(getItem(position));


        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();

                if (constraint == null || constraint.length() == 0) {
                    filterResults.values = Utils.productNames;
                    filterResults.count = Utils.productNames.size();
                }
                else {
                    ArrayList<String> productNames = new ArrayList<String>();
                    for (String data : Utils.productNames) {
                        if (data.toUpperCase().contains(constraint.toString().toUpperCase())) {
                            productNames.add(data);
                        }
                    }

                    filterResults.values = productNames;
                    filterResults.count = productNames.size();

                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                    productNames = (ArrayList<String>) results.values;
                    notifyDataSetChanged();
            }};

        return filter;
    }
}
