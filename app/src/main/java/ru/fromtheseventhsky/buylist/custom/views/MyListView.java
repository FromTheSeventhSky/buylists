package ru.fromtheseventhsky.buylist.custom.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.Toast;


public class MyListView extends ListView {

    private Context context;
    private int displayWidth;
    private int displayHeight;
    private OnTouchListener listener;

    void getDim(){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        displayWidth = size.x;
        displayHeight = size.y;
    }

    public void setOnSwipeTouchListener(){
        listener = new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()){
                    case MotionEvent.ACTION_MOVE:
                        Toast.makeText(context, "ACTION_MOVE", Toast.LENGTH_SHORT).show();
                        return true;
                    case MotionEvent.ACTION_DOWN:
                        Toast.makeText(context, "ACTION_DOWN", Toast.LENGTH_LONG).show();
                        return true;
                    case MotionEvent.ACTION_CANCEL:
                        Toast.makeText(context, "ACTION_CANCEL", Toast.LENGTH_LONG).show();
                        break;
                    case MotionEvent.ACTION_UP:
                        Toast.makeText(context, "ACTION_UP", Toast.LENGTH_LONG).show();
                        return true;
                    default:
                        break;
                }
                return true;
            }
        };
    }

    public MyListView(Context context) {
        super(context);
        this.context = context;
        getDim();
    }


    public MyListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        getDim();
    }

    public MyListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        getDim();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MyListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        getDim();
    }


    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
    }
}
