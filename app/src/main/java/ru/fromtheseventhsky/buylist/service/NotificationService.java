package ru.fromtheseventhsky.buylist.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


public class NotificationService extends Service {

    @Override
    public void onCreate() {
        // The service is being created
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // The service is starting, due to a call to startService()
        operateWithData(intent.getStringExtra("calledAction"));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void operateWithData(String arg){

    }

}
