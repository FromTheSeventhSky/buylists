package ru.fromtheseventhsky.buylist.fragments;


import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.fromtheseventhsky.buylist.MainActivity;
import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.adapters.PrefListViewDialogAdapter;
import ru.fromtheseventhsky.buylist.custom.adapters.PrefsListViewAdapter;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.Utils;

public class PreferencesFragment extends Fragment {

    PrefsListViewAdapter prefsListViewAdapter;
    RelativeLayout prefRoot;
    ListView lv;








    PreferenceCategoriesFragment preferenceCategoriesFragment;
    PreferenceGoodsFragment preferenceGoodsFragment;


    PreferenceGoodsFragment goodsFragment;

    public static PreferencesFragment newInstance() {
        PreferencesFragment fragment = new PreferencesFragment();
        return fragment;
    }

    public PreferencesFragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.preference_layout, container, false);

        prefRoot = (RelativeLayout) root.findViewById(R.id.PREF_root);

        lv = (ListView) root.findViewById(R.id.PREF_listview);
        prefsListViewAdapter = new PrefsListViewAdapter(getActivity());
        lv.setAdapter(prefsListViewAdapter);

        prefRoot.setBackgroundColor(AppPreferences.colorTheme_Background);
        lv.setCacheColorHint(AppPreferences.colorTheme_ListViewShadow);
        lv.setOverscrollHeader(new ColorDrawable(AppPreferences.colorTheme_ListViewShadow));

        preferenceGoodsFragment = PreferenceGoodsFragment.newInstance();
        preferenceCategoriesFragment = PreferenceCategoriesFragment.newInstance();
        goodsFragment = PreferenceGoodsFragment.newInstance();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0://divider
                        break;
                    case IntConst.COLOR_THEME:
                        new PreferencesListDialog(IntConst.COLOR_THEME).show(getFragmentManager(), "COLOR_THEME");
                        break;
                    case IntConst.FONT_SIZE:
                        new PreferencesListDialog(IntConst.FONT_SIZE).show(getFragmentManager(), "FONT_SIZE");
                        break;
                    case IntConst.DIVIDER_2:
                        break;
                    case IntConst.USE_CATEGORIES:
                        prefsListViewAdapter.changeChbComment(IntConst.USE_CATEGORIES, AppPreferences.categoryAvailable);
                        break;
                    case IntConst.CATEGORIES:
                        if(AppPreferences.categoryAvailable) {
                            Utils.levelPosition = IntConst.CATEGORIES;
                            ((MainActivity)getActivity()).setTitlePrefCategories();
                            openCategoriesFragment();
                        }
                        break;
                    case IntConst.CURRENCY:
                        if(AppPreferences.costAvailable)
                            new PreferencesEditTextDialog(IntConst.CURRENCY).show(getFragmentManager(), "CURRENCY");
                        break;
                    case IntConst.USE_COST:
                        prefsListViewAdapter.changeChbComment(IntConst.USE_COST, AppPreferences.costAvailable);
                        break;
                    case IntConst.COLOR_DEFAULT:
                        break;
                    case IntConst.DIVIDER_3:
                        break;
                    case IntConst.VIBRATION:
                        prefsListViewAdapter.changeChbComment(IntConst.VIBRATION, AppPreferences.vibrationEnabled);
                        break;
                    case IntConst.REMEMBER:
                        prefsListViewAdapter.changeChbComment(IntConst.REMEMBER, AppPreferences.remember);
                        break;
                    case IntConst.REMOVE_OLD:
                        break;
                    case IntConst.LIGHT_SCREEN:
                        prefsListViewAdapter.changeChbComment(IntConst.LIGHT_SCREEN, AppPreferences.screenLightAlways);
                        break;
                    case IntConst.DIVIDER_4:
                        break;
                    case IntConst.BACKUP_CREATE:
                        break;
                    case IntConst.BACKUP_RECOVER:
                        break;
                    case IntConst.GOODS:
                        ((MainActivity)getActivity()).setTitlePrefGoods();
                        Utils.levelPosition = IntConst.GOODS;
                        openGoodsFragment();
                        break;

                }
            }
        });

        return root;
    }

    public void openCategoriesFragment(){
        Utils.isPrefLvlSecondOpened = true;
        getFragmentManager()
                .beginTransaction()
                .add(R.id.PREF_fragmentContainer, preferenceCategoriesFragment)
                .commit();
        lv.setVisibility(View.GONE);
    }
    public void closeCategoriesFragment(){
        Utils.isPrefLvlSecondOpened = false;
        getFragmentManager()
                .beginTransaction()
                .remove(preferenceCategoriesFragment)
                .commit();
        lv.setVisibility(View.VISIBLE);
    }

    public void openGoodsFragment(){
        Utils.isPrefLvlSecondOpened = true;
        getFragmentManager()
                .beginTransaction()
                .add(R.id.PREF_fragmentContainer, preferenceGoodsFragment)
                .commit();
        lv.setVisibility(View.GONE);
    }
    public void closeGoodsFragment(){
        Utils.isPrefLvlSecondOpened = false;
        getFragmentManager()
                .beginTransaction()
                .remove(preferenceGoodsFragment)
                .commit();
        lv.setVisibility(View.VISIBLE);
    }

    public void closeSecondLevel(){
        switch(Utils.levelPosition){
            case IntConst.CATEGORIES:
                closeCategoriesFragment();
                ((MainActivity)getActivity()).resetGoodsList();
                break;
            case IntConst.GOODS:
                closeGoodsFragment();
                break;
        }
    }

    public void offDeleteMode(){
        switch(Utils.levelPosition){
            case IntConst.CATEGORIES:
               preferenceCategoriesFragment.switchDeleteMode(-1);
                break;
            case IntConst.GOODS:
                preferenceGoodsFragment.switchDeleteMode(-1);
                break;
        }
    }




    public class PreferencesListDialog extends DialogFragment {

        private int id;


        PreferencesListDialog(int id){
            this.id = id;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.pref_dialog_list, container, false);

            TextView title = (TextView) v.findViewById(R.id.pref_dialog_list_title);
            LinearLayout root = (LinearLayout) v.findViewById(R.id.pref_dialog_list_bck);
            ListView lv = (ListView) v.findViewById(R.id.pref_dialog_list_list);
            lv.setDrawSelectorOnTop(true);
            lv.setSelector(R.drawable.selector_item);

            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
            title.setTextColor(AppPreferences.colorTheme_FontColor);
            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            switch (id){
                case IntConst.COLOR_THEME: {
                    final String[] arr = getActivity().getResources().getStringArray(R.array.color_themes);
                    title.setText(R.string.color_theme_title);
                    lv.setAdapter(new PrefListViewDialogAdapter(getActivity(), arr, AppPreferences.colorThemeNumber));
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            prefsListViewAdapter.changePopupComment(IntConst.COLOR_THEME, arr[position]);
                            AppPreferences.colorThemeNumber = position;
                            getDialog().dismiss();
                        }
                    });
                }
                    break;
                case IntConst.FONT_SIZE: {
                    title.setText(R.string.font_title);
                    final String[] arr = getActivity().getResources().getStringArray(R.array.font_sizes);
                    lv.setAdapter(new PrefListViewDialogAdapter(getActivity(), arr, AppPreferences.fontSize));
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            prefsListViewAdapter.changePopupComment(IntConst.FONT_SIZE, arr[position]);
                            AppPreferences.fontSize = position;
                            getDialog().dismiss();
                        }
                    });
                }
                    break;
            }

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }

    public class PreferencesEditTextDialog extends DialogFragment {

        private int id;


        PreferencesEditTextDialog(int id){
            this.id = id;
        }
        PreferencesEditTextDialog(){}

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.pref_dialog_edittext, container, false);

            TextView title = (TextView) v.findViewById(R.id.PrefDialogET_title);
            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.PrefDialogET_root);
            final EditText editText = (EditText) v.findViewById(R.id.PrefDialogET_et);

            Button accept = (Button) v.findViewById(R.id.PrefDialogET_accept);
            Button cancel = (Button) v.findViewById(R.id.PrefDialogET_cancel);


            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
            title.setTextColor(AppPreferences.colorTheme_FontColor);

            editText.setTextColor(AppPreferences.colorTheme_FontColor);
            editText.setHintTextColor(AppPreferences.colorTheme_HintColor);
            editText.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    editText.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            switch (id){
                case IntConst.CURRENCY: {
                    title.setText(R.string.prefs_currency_title_popup);
                    editText.setHint(AppPreferences.currency);


                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getDialog().dismiss();
                        }
                    });
                    accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(editText.getText().toString().trim().equalsIgnoreCase("")) {
                                editText.setError(getActivity().getString(R.string.create_buy_list_title_error));
                                return;
                            }
                            prefsListViewAdapter.changePopupComment(IntConst.CURRENCY, editText.getText().toString());
                            AppPreferences.currency = editText.getText().toString().trim();
                            getDialog().dismiss();
                        }
                    });
                }
                break;
            }

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }

}
