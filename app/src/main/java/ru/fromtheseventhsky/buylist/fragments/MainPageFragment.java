package ru.fromtheseventhsky.buylist.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import ru.fromtheseventhsky.buylist.MainActivity;
import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.adapters.CalendarGridAdapter;
import ru.fromtheseventhsky.buylist.custom.adapters.MainPageExpListViewAdapter;
import ru.fromtheseventhsky.buylist.custom.adapters.MainPageLongClickPopupAdapter;
import ru.fromtheseventhsky.buylist.loaders.LoaderMainPage;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyList;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class MainPageFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private ImageButton btnCreateBuyList;
    private ImageButton btnOpenPreferences;

    private ExpandableListView expListView;
    private TextView emptyTv;

    private PreferencesFragment preferencesFragment;


    private ProgressBar progressBar;

    private MainPageFragmentListener mListener;

    RelativeLayout mainRoot;
    FrameLayout mainPageRoot;
    FrameLayout preferencesRoot;

    boolean canAnimateUpDown;

    Runnable btnPrefClose;
    Thread prefThr;
    boolean isThrRun;

    private float bottomForAnim;
    private float initY;

    BuyList blTmpTouch;

    private int displayWidth;

    private int groupPosition;
    private int childPosition;

    LoaderManager.LoaderCallbacks<Cursor> loaderCallbacks;


    ArrayList<BuyList> buyListToUpdate;

    MainPageExpListViewAdapter expLVAdapter;

    private ObjectAnimator animateBtnUp;
    private ObjectAnimator animateBtnDown;


    private ObjectAnimator animatePrefBtnUp;
    private ObjectAnimator animatePrefBtnDown;
    private Handler handler;

    public static MainPageFragment newInstance() {
        return new MainPageFragment();
    }

    public MainPageFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }





    public void openPreference(){
        if(!Utils.preferencesVisible) {
            Utils.preferencesVisible = true;
            Utils.currentFragment = IntConst.FRAGMENT_PREF;
            mainRoot.setVisibility(View.GONE);
            ((MainActivity)getActivity()).setTitlePreferences();
            getFragmentManager().beginTransaction().add(R.id.MP_preferences, preferencesFragment).commit();
        }
    }
    public void closePreference(){
        Utils.preferencesVisible = false;
        mainRoot.setVisibility(View.VISIBLE);
        getFragmentManager().beginTransaction().remove(preferencesFragment).commit();
    }

    public void closePrefSecondLevel(){
        preferencesFragment.closeSecondLevel();
    }
    public void offDeleteMode(){
        preferencesFragment.offDeleteMode();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }





    DisplayMetrics dm;
    int contentHeight;
    View content;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_page, container, false);

        displayWidth = getResources().getDisplayMetrics().widthPixels;

        // TODO Handler.Callback
        Handler.Callback hc = new Handler.Callback() {
            public boolean handleMessage(Message msg) {
                isThrRun = true;
                animateDownPref();
                return false;
            }
        };
        handler = new Handler(hc);

        prefThr = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    isThrRun = true;
                    handler.post(animateDownPref);
                    prefThr.interrupt();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        isThrRun = false;

        expListView = (ExpandableListView) view.findViewById(R.id.MP_expListView);
        emptyTv = (TextView) view.findViewById(R.id.MP_empty_list);
        btnCreateBuyList = (ImageButton)view.findViewById(R.id.btn_create_buy_list);
        btnOpenPreferences = (ImageButton)view.findViewById(R.id.MP_preference_btn);
        progressBar = (ProgressBar) view.findViewById(R.id.MP_progress);

        mainRoot = (RelativeLayout) view.findViewById(R.id.MP_root);
        preferencesRoot = (FrameLayout) view.findViewById(R.id.MP_preferences);
        mainPageRoot = (FrameLayout) view.findViewById(R.id.MP_root_lay);

        mainPageRoot.setBackgroundColor(AppPreferences.colorTheme_Background);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        dm = new DisplayMetrics();

        content = getActivity().getWindow().findViewById(Window.ID_ANDROID_CONTENT);
        contentHeight = -1;

        preferencesFragment = PreferencesFragment.newInstance();


        btnCreateBuyList.getBackground().setColorFilter(AppPreferences.colorTheme_DeselectedBtnColor, PorterDuff.Mode.MULTIPLY);
        btnOpenPreferences.getBackground().setColorFilter(AppPreferences.colorTheme_DeselectedBtnColor, PorterDuff.Mode.MULTIPLY);

        canAnimateUpDown = false;

        btnOpenPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPreference();
            }
        });

        display.getMetrics(dm);
        bottomForAnim = getResources().getDimension(R.dimen.translateX);


        loaderCallbacks = this;

        expListView.setSelector(R.drawable.selector_item);
        expListView.setDrawSelectorOnTop(true);

        animateBtnUp = ObjectAnimator.ofFloat(btnCreateBuyList, "translationY", btnCreateBuyList.getTranslationY(), bottomForAnim);
        animateBtnUp.setDuration(100);

        animateBtnDown = ObjectAnimator.ofFloat(btnCreateBuyList, "translationY", btnCreateBuyList.getTranslationY(), bottomForAnim);
        animateBtnDown.setDuration(100);

        animatePrefBtnUp = ObjectAnimator.ofFloat(btnOpenPreferences, "translationY", btnOpenPreferences.getTranslationY(), bottomForAnim);
        animatePrefBtnUp.setDuration(100);

        animatePrefBtnDown = ObjectAnimator.ofFloat(btnOpenPreferences, "translationY", btnOpenPreferences.getTranslationY(), bottomForAnim);
        animatePrefBtnDown.setDuration(100);

        btnPrefClose = new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        expListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(totalItemCount>0) {
                    canAnimateUpDown = totalItemCount > visibleItemCount;
                }
            }
        });

        expListView.setOnTouchListener(new View.OnTouchListener() {
            float xPos = 0, yPos = 0;
            float initX, initXSecond;
            int dimension = 0;

            int direction = 0;

            boolean animationStarted = false;


            boolean canSwipe = true;


            Button itemBtnAccept;
            Button itemBtnCancel;

            int selectedPositionWhileTouching;

            View v1;
            RelativeLayout mainRoot = null;
            RelativeLayout secondRoot;

            TextView secondRootTitle;

            int groupPos;
            int childPos;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ExpandableListView lv = (ExpandableListView) v;

                switch (event.getAction()){
                    // TODO ACTION_DOWN
                    case MotionEvent.ACTION_DOWN: {
                        Rect rect = new Rect();
                        int childCount = lv.getChildCount();
                        int[] listViewCoordinates = new int[2];
                        lv.getLocationOnScreen(listViewCoordinates);
                        int x = (int) event.getRawX() - listViewCoordinates[0];
                        int y = (int) event.getRawY() - listViewCoordinates[1];
                        View child;

                        for (int i = 0; i < childCount; i++) {
                            child = lv.getChildAt(i);
                            child.getHitRect(rect);
                            if (rect.contains(x, y)) {
                                selectedPositionWhileTouching = lv.getFirstVisiblePosition()+i;

                                if(selectedPositionWhileTouching==0) {
                                    canSwipe = false;
                                    break;
                                }

                                long packedPosition = lv.getExpandableListPosition(selectedPositionWhileTouching);


                                if(ExpandableListView.getPackedPositionType(packedPosition)==ExpandableListView.PACKED_POSITION_TYPE_GROUP){
                                    canSwipe = false;
                                    v1 = null;
                                    break;
                                }
                                groupPos = ExpandableListView.getPackedPositionGroup(packedPosition);
                                childPos = ExpandableListView.getPackedPositionChild(packedPosition);
                                blTmpTouch =  expLVAdapter.getChild(groupPos, childPos);

                                canSwipe = true;

                                v1 = child;

                                mainRoot = (RelativeLayout) v1.findViewById(R.id.list_of_buy_list_item_root);
                                secondRoot = (RelativeLayout) v1.findViewById(R.id.list_of_buy_list_item_root_second);
                                secondRootTitle = (TextView) v1.findViewById(R.id.list_of_buy_list_item_title_second_root);

                                itemBtnAccept = (Button) secondRoot.findViewById(R.id.list_of_buy_list_item_accept);
                                itemBtnCancel = (Button) secondRoot.findViewById(R.id.list_of_buy_list_item_cancel);

                                itemBtnAccept.setBackgroundDrawable(Utils.getStateList());
                                itemBtnCancel.setBackgroundDrawable(Utils.getStateList());

                                if(blTmpTouch.type == 1)
                                    secondRootTitle.setText(R.string.buy_list_old_ask);
                                else
                                    secondRootTitle.setText(R.string.buy_list_new_ask);
                                break;
                            }
                        }

                        xPos = event.getX();
                        yPos = event.getY();
                        dimension = 0;
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        if (dimension == 1) {
                            if(v1!=null) {
                                ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                transAnimation.setDuration(300);
                                transAnimation.setInterpolator(new OvershootInterpolator());
                                transAnimation.start();
                                v1.setPressed(false);
                            }
                        }
                        break;
                    }

                    //TODO ACTION_UP
                    case MotionEvent.ACTION_UP: {
                        if (dimension == 1) {
                            if(v1!=null) {

                                if(direction>0){
                                    if(mainRoot.getTranslationX()>dm.widthPixels/2.5){
                                        animateUp();
                                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                        transAnimation.setDuration(getAnimationTime(mainRoot.getTranslationX(), false));
                                        //transAnimation.setInterpolator(new OvershootInterpolator());
                                        transAnimation.start();

                                        ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), -dm.widthPixels);
                                        transAnimationSecond.setDuration(getAnimationTime(mainRoot.getTranslationX(), false));
                                        //transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                        transAnimationSecond.addListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                secondRoot.setVisibility(View.GONE);
                                                new MyCalendarDialog().show(getFragmentManager(), "MyCalendarDialog");
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });
                                        transAnimationSecond.start();

                                        v1.setPressed(false);

                                    } else {
                                        animateUp();
                                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                        transAnimation.setDuration(getAnimationTime(mainRoot.getTranslationX(), false));
                                        //transAnimation.setInterpolator(new OvershootInterpolator());
                                        transAnimation.start();

                                        ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), -dm.widthPixels);
                                        transAnimationSecond.setDuration(getAnimationTime(mainRoot.getTranslationX(), false));
                                        //transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                        transAnimationSecond.addListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                secondRoot.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });
                                        transAnimationSecond.start();

                                        v1.setPressed(false);
                                    }
                                } else {
                                    if(Math.abs(mainRoot.getTranslationX())>dm.widthPixels/2.5){
                                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), -dm.widthPixels);
                                        transAnimation.setDuration(getAnimationTime(mainRoot.getTranslationX(), true));
                                        //transAnimation.setInterpolator(new OvershootInterpolator());
                                        transAnimation.start();

                                        ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), 0);
                                        transAnimationSecond.setDuration(getAnimationTime(mainRoot.getTranslationX(), true));
                                        //transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                        transAnimationSecond.start();
                                        mainRoot.setTag(false);




                                        itemBtnAccept.setTag(R.id.BL_accept,selectedPositionWhileTouching);


                                        itemBtnAccept.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                animateUp();

                                                if(blTmpTouch.type == 1) {
                                                    if(expLVAdapter.getGroup(9).size()==0){
                                                        Utils.expandedBuyLists.add(true);
                                                    } else {
                                                        Utils.expandedBuyLists.set(Utils.expandedBuyLists.size() - 1, true);
                                                    }
                                                    if(expLVAdapter.getGroupRight(groupPos).size()==1){
                                                        Utils.expandedBuyLists.remove(groupPos);
                                                    }
                                                    expLVAdapter.moveToOld(groupPos, childPos);
                                                    blTmpTouch.type = 2;
                                                } else {
                                                    if(expLVAdapter.getGroupRight(groupPos).size()==1){
                                                        Utils.expandedBuyLists.remove(groupPos);
                                                    }
                                                    int position = expLVAdapter.moveFromOld(groupPos, childPos);
                                                    if(expLVAdapter.getGroupRight(position).size()==1) {
                                                        Utils.expandedBuyLists.add(position, true);
                                                    } else {
                                                        Utils.expandedBuyLists.set(position, true);
                                                    }
                                                    blTmpTouch.type = 1;
                                                }
                                                buyListToUpdate.clear();
                                                buyListToUpdate.add(blTmpTouch);

                                                if(expLVAdapter.getGroupRight(groupPos).size()==0){
                                                    Utils.expandedBuyLists.remove(groupPos);
                                                }

                                                expLVAdapter.notifyDataSetChanged();


                                                expandAllWasExpanded();
                                                startLoader(IntConst.LOADER_CHANGE_ITEM_TYPE, null);
                                            }
                                        });

                                        itemBtnCancel.setTag(R.id.CommodityLay_nothing,mainRoot);
                                        itemBtnCancel.setTag(R.id.BL_actuality,secondRoot);


                                        itemBtnCancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                animateUp();

                                                RelativeLayout mainRoot = (RelativeLayout) v.getTag(R.id.CommodityLay_nothing);
                                                final RelativeLayout secondRoot = (RelativeLayout) v.getTag(R.id.BL_actuality);

                                                ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                                transAnimation.setDuration(300);
                                               // transAnimation.setInterpolator(new OvershootInterpolator());
                                                transAnimation.start();

                                                ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), dm.widthPixels);
                                                transAnimationSecond.setDuration(300);
                                              //  transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                                transAnimationSecond.addListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        secondRoot.setVisibility(View.GONE);
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {

                                                    }
                                                });
                                                transAnimationSecond.start();
                                                mainRoot.setTag(true);
                                            }
                                        });

                                        v1.setPressed(false);
                                    } else {
                                        animateUp();
                                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                        transAnimation.setDuration(getAnimationTime(mainRoot.getTranslationX(), false));
                                       // transAnimation.setInterpolator(new OvershootInterpolator());
                                        transAnimation.start();

                                        ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), dm.widthPixels);
                                        transAnimationSecond.setDuration(getAnimationTime(mainRoot.getTranslationX(), false));
                                      //  transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                        transAnimationSecond.addListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                secondRoot.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });
                                        transAnimationSecond.start();

                                        v1.setPressed(false);
                                    }
                                }
                            }
                        }
                        if(v1!=null)
                            v1.setPressed(false);
                        break;
                    }

                    //TODO ACTION_MOVE

                    case MotionEvent.ACTION_MOVE: {
                        if (dimension == 0) {
                            if (Math.abs(Math.abs(xPos) - Math.abs(event.getX())) > 20)
                                dimension = 1;
                            if(v1!=null) {
                                initX = mainRoot.getTranslationX();
                                initXSecond = secondRoot.getTranslationX();
                            }

                            initY = event.getY();
                            //Log.d("initX", "" + initX);
                            if (Math.abs(Math.abs(yPos) - Math.abs(event.getY())) > 20)
                                dimension = 2;
                        }
                        if (dimension == 1 && !animationStarted && canSwipe) {
                            if(v1!=null) {
                                if((boolean)mainRoot.getTag()) {
                                    animateDown();
                                    mainRoot.setTranslationX(event.getX() - xPos);

                                    if(secondRoot.getVisibility()==View.GONE) {
                                        secondRoot.setVisibility(View.VISIBLE);
                                        v1.setPressed(false);
                                        secondRoot.setLayoutParams(new RelativeLayout.LayoutParams(mainRoot.getMeasuredWidth(), mainRoot.getMeasuredHeight()));
                                    }
                                    if (xPos - event.getX() < 0) {
                                        direction = 1;
                                    } else {
                                        direction = -1;
                                    }
                                    if (direction > 0) {
                                        //secondRootTitle.setText(R.string.buy_list_notif_ask);
                                        //itemBtnAccept.setVisibility(View.GONE);
                                        //itemBtnCancel.setVisibility(View.GONE);
                                        secondRoot.setTranslationX(event.getX() - xPos - dm.widthPixels);
                                    } else {
                                        //if(blTmpTouch.type == 1)
                                        //    secondRootTitle.setText(R.string.buy_list_old_ask);
                                        //else
                                        //    secondRootTitle.setText(R.string.buy_list_new_ask);
                                        //itemBtnAccept.setVisibility(View.VISIBLE);
                                        //itemBtnCancel.setVisibility(View.VISIBLE);
                                        secondRoot.setTranslationX(event.getX() - xPos + dm.widthPixels);
                                    }
                                }
                            }

                            event.setAction(MotionEvent.ACTION_CANCEL);
                        }
                        if (dimension == 2) {
                            if(canAnimateUpDown) {
                                //если листаем вверх
                                if (initY - event.getY() > 3) {
                                    initY = event.getY();
                                    animateDown();
                                }
                                //если листаем вниз
                                if (initY - event.getY() < -3) {
                                    initY = event.getY();
                                    animateUp();
                                }
                            }

                            return false;
                        }
                        return false;
                    }
                    //break;

                }
                return false;
            }
        });

        btnCreateBuyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnCreateBLVPressed();
            }
        });



        ArrayList<ArrayList<BuyList>> buyListExp = new ArrayList<>();
        ArrayList<Integer> dayHave = new ArrayList<>();

        Calendar yesterday = Calendar.getInstance();
        yesterday.set(Calendar.HOUR_OF_DAY, 23);
        yesterday.set(Calendar.MINUTE, 59);
        yesterday.set(Calendar.SECOND, 59);
        yesterday.set(Calendar.MILLISECOND, 59);

        int POSITIONS_IN_ARRLIST = 10;
        int TODAY_PLUS_WEEK = 8;

        for(int i =0; i < POSITIONS_IN_ARRLIST; i++){
            buyListExp.add(new ArrayList<BuyList>());
            if(i < TODAY_PLUS_WEEK+1) {
                Utils.todayAndWeekArrList.add(yesterday.getTimeInMillis());
                yesterday.add(Calendar.DATE, -1);
            } else {
                Utils.todayAndWeekArrList.add(1L);
            }
        }

        expLVAdapter = new MainPageExpListViewAdapter(buyListExp, dayHave, getActivity());

        expListView.setAdapter(expLVAdapter);


        expListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    new MainPageDialog(id).show(getFragmentManager(),"mp");
                    return true;
                }


                return false;
            }
        });
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Utils.expandedBuyLists.set(groupPosition, !expListView.isGroupExpanded(groupPosition));
                return false;
            }
        });

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                animateUp();
                blTmpTouch = expLVAdapter.getChild(groupPosition, childPosition);
                Utils.idOfCalledBuyList = blTmpTouch.id;
                Utils.costInCurrentList = blTmpTouch.cost;
                Utils.boughtInCurrentList = blTmpTouch.bought;
                Utils.positionsInCurrentList = blTmpTouch.positions;
                onButtonPressed();
                return false;
            }
        });

        // TODO init loaders

        startLoader(IntConst.LOADER_GET_MAIN_PAGE_GOODS_LIST, null);
        startLoader(IntConst.LOADER_GET_CATEGORIES_LIST, null);
        startLoader(IntConst.LOADER_GET_GOODS_LIST, null);
        return view;
    }


    public int getAnimationTime(float translationX, boolean continued) {
        int time = 0;
        int onePercent = displayWidth / 100;
        translationX = Math.abs(translationX);

        if (continued) {
            time = (int) (6 * (displayWidth - translationX) / onePercent);
        } else {
            time = (int) (6 * translationX / onePercent);
        }
        Log.d("time", String.valueOf(time));
        return time;
    }


    public void resetGoodsList(){
        Utils.availableProducts.clear();
        Utils.productNames.clear();
        startLoader(IntConst.LOADER_GET_GOODS_LIST, null);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }



    public void onButtonPressed() {
        if (mListener != null) {
            mListener.clickOnBuyListViewItem();
        }
    }

    void animateUp(){
        if(animateBtnUp.isStarted())
            animateBtnUp.cancel();

        if(!animateBtnDown.isStarted()) {
            animateBtnDown = ObjectAnimator
                    .ofFloat(btnCreateBuyList, "translationY",
                            btnCreateBuyList.getTranslationY(), 0);
            animateBtnDown.start();
        }

    }
    void animateDown(){
        if (animateBtnDown.isStarted())
            animateBtnDown.cancel();

        if (!animateBtnUp.isStarted()) {
            animateBtnUp = ObjectAnimator
                    .ofFloat(btnCreateBuyList, "translationY",
                            btnCreateBuyList.getTranslationY(), bottomForAnim);
            animateBtnUp.start();
        }


    }


    public void clickPrefBtn(){
        if(animatePrefBtnUp.isStarted() || btnOpenPreferences.getTranslationY()==0){
            animateDownPref();
        } else {
            animateUpPref();
        }
    }


    void animateDownPref(){
        if(isThrRun) {
            isThrRun = false;
            handler.removeMessages(2);
        }
        if(animatePrefBtnUp.isStarted())
            animatePrefBtnUp.cancel();

        if(!animatePrefBtnDown.isStarted()) {
            animatePrefBtnDown = ObjectAnimator
                    .ofFloat(btnOpenPreferences, "translationY",
                            btnOpenPreferences.getTranslationY(), bottomForAnim);
            animatePrefBtnDown.start();
        }

    }
    void animateUpPref(){
        if (animatePrefBtnDown.isStarted())
            animatePrefBtnDown.cancel();

        if (!animatePrefBtnUp.isStarted()) {
            animatePrefBtnUp = ObjectAnimator
                    .ofFloat(btnOpenPreferences, "translationY",
                            btnOpenPreferences.getTranslationY(), 0);

            animatePrefBtnUp.start();
        }
        animatePrefBtnUp.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                handler.sendEmptyMessageDelayed(2, 3000);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void startLoader(int id, Bundle bundle){
        getLoaderManager().initLoader(id, bundle, this).forceLoad();
    }

    Runnable animateDownPref = new Runnable() {
        public void run() {
            animateDownPref();
        }
    };

    public void onBtnCreateBLVPressed() {
        if (mListener != null) {
            mListener.clickOnBtnCreateBuyList();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MainPageFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    // TODO onCreateLoader
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        Utils.loaderStarted = true;
        switch (id){
            case IntConst.LOADER_GET_MAIN_PAGE_GOODS_LIST:
                return new LoaderMainPage(getActivity(), IntConst.LOADER_GET_MAIN_PAGE_GOODS_LIST);
            case IntConst.LOADER_CHANGE_TYPE_IF_OLD:
                return new LoaderMainPage(getActivity(), buyListToUpdate, IntConst.LOADER_CHANGE_TYPE_IF_OLD);
            case IntConst.LOADER_CHANGE_ITEM_TYPE:
                return new LoaderMainPage(getActivity(), buyListToUpdate, 2);
            case IntConst.LOADER_GET_CATEGORIES_LIST:
                return new LoaderMainPage(getActivity(), IntConst.LOADER_GET_CATEGORIES_LIST);
            case IntConst.LOADER_GET_GOODS_LIST:
                return new LoaderMainPage(getActivity(), IntConst.LOADER_GET_GOODS_LIST);
            case IntConst.LOADER_REMOVE_BL:
                return new LoaderMainPage(getActivity(), bundle.getInt("idBL"),IntConst.LOADER_REMOVE_BL);
        }
        return null;
    }

    // TODO onLoadFinished
    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        switch(cursorLoader.getId()){
            case IntConst.LOADER_CHANGE_ITEM_TYPE:
                getLoaderManager().destroyLoader(IntConst.LOADER_CHANGE_ITEM_TYPE);
                break;
            case IntConst.LOADER_CHANGE_TYPE_IF_OLD:
                getLoaderManager().destroyLoader(IntConst.LOADER_CHANGE_TYPE_IF_OLD);
                break;
            case IntConst.LOADER_GET_CATEGORIES_LIST:
                getLoaderManager().destroyLoader(IntConst.LOADER_GET_CATEGORIES_LIST);
                break;
            case IntConst.LOADER_GET_GOODS_LIST:
                getLoaderManager().destroyLoader(IntConst.LOADER_GET_GOODS_LIST);
                break;
            case IntConst.LOADER_REMOVE_BL:
                expLVAdapter.removeBL(groupPosition, childPosition);
                getLoaderManager().destroyLoader(IntConst.LOADER_REMOVE_BL);
                break;
            case IntConst.LOADER_GET_MAIN_PAGE_GOODS_LIST: {
                cursor.moveToFirst();
                if (cursor.getCount() > 0) {
                    ArrayList<BuyList> buyList = new ArrayList<BuyList>();

                    ArrayList<ArrayList<BuyList>> buyListExp = new ArrayList<ArrayList<BuyList>>();

                    ArrayList<Integer> dayHave = new ArrayList<Integer>();


                    Calendar yesterday = Calendar.getInstance();
                    yesterday.set(Calendar.HOUR_OF_DAY, 23);
                    yesterday.set(Calendar.MINUTE, 59);
                    yesterday.set(Calendar.SECOND, 59);
                    yesterday.set(Calendar.MILLISECOND, 59);


                    int POSITIONS_IN_ARRLIST = 10;
                    int TODAY_PLUS_WEEK = 8;

                    for (int i = 0; i < POSITIONS_IN_ARRLIST; i++) {
                        buyListExp.add(new ArrayList<BuyList>());
                        if (i < TODAY_PLUS_WEEK + 1) {
                            Utils.todayAndWeekArrList.add(yesterday.getTimeInMillis());
                            yesterday.add(Calendar.DATE, -1);
                        } else {
                            Utils.todayAndWeekArrList.add(1L);
                        }
                    }

                    buyListToUpdate = new ArrayList<>();

                    long curTime = Calendar.getInstance().getTimeInMillis();

                    do {

                        BuyList tmp = new BuyList(cursor.getInt(cursor.getColumnIndex("id")),
                                cursor.getString(cursor.getColumnIndex("name")),
                                cursor.getString(cursor.getColumnIndex("comment")),
                                cursor.getLong(cursor.getColumnIndex("date_start")),
                                cursor.getLong(cursor.getColumnIndex("date_end")),
                                cursor.getInt(cursor.getColumnIndex("colorBck")),
                                cursor.getInt(cursor.getColumnIndex("colorTxt")),
                                cursor.getInt(cursor.getColumnIndex("type")),
                                false,
                                0,
                                cursor.getInt(cursor.getColumnIndex("positions")),
                                cursor.getInt(cursor.getColumnIndex("bought")),
                                cursor.getInt(cursor.getColumnIndex("cost")));
                        if (tmp.dateEnd != -1 && tmp.type != 2) {
                            if (tmp.dateEnd < curTime) {
                                tmp.type = 2;
                                buyListToUpdate.add(tmp);
                            }
                        }


                        if (tmp.type == 2) {
                            buyListExp.get(9).add(0, tmp);
                        } else {
                            if (tmp.date < Utils.todayAndWeekArrList.get(TODAY_PLUS_WEEK)) {
                                buyListExp.get(8).add(0, tmp);
                            } else {
                                for (int i = 0; i < TODAY_PLUS_WEEK; i++) {
                                    if (tmp.date < Utils.todayAndWeekArrList.get(i) && tmp.date > Utils.todayAndWeekArrList.get(i + 1)) {
                                        buyListExp.get(i).add(0, tmp);
                                        break;
                                    }
                                }
                            }
                        }
                    } while (cursor.moveToNext());

                    cursor.close();

                    if (buyListToUpdate.size() > 0) {
                        startLoader(IntConst.LOADER_CHANGE_TYPE_IF_OLD, null);
                    }

                    for (int i = 0; i < buyListExp.size(); i++) {
                        if (buyListExp.get(i).size() != 0) {
                            dayHave.add(i);
                            Utils.expandedBuyLists.add(false);
                        }
                    }

                    progressBar.setVisibility(View.GONE);

                    //sortData(buyList);

                    expLVAdapter.setNewLists(buyListExp, dayHave);
                    expLVAdapter.notifyDataSetChanged();
                    expandAll();


                } else {
                    progressBar.setVisibility(View.GONE);
                    emptyTv.setVisibility(View.VISIBLE);
                }
                getLoaderManager().destroyLoader(0);
            }

                break;
        }
        Utils.loaderStarted = false;
    }

    public void notifyDataSetChanged(){
        blTmpTouch.cost = Utils.costInCurrentList;
        blTmpTouch.bought = Utils.boughtInCurrentList;
        blTmpTouch.positions = Utils.positionsInCurrentList;
        expLVAdapter.notifyDataSetChanged();
    }


    private void expandAll(){
        for ( int i = 0; i < expLVAdapter.getGroupCount(); i++ ) {
            expListView.expandGroup(i);
            Utils.expandedBuyLists.set(i, true);
        }
    }
    private void expandAllWasExpanded(){
        for ( int i = 0; i < expLVAdapter.getGroupCount(); i++ ) {
            if(Utils.expandedBuyLists.get(i)) {
                expListView.expandGroup(i);
            } else {
                expListView.collapseGroup(i);
            }
        }
    }


    public void hideEmptyTV(){
        emptyTv.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        Utils.loaderStarted = false;
    }

    // TODO MainPageDialog for select edit or delete
    public class MainPageDialog extends DialogFragment {
        private long idItem;
        MainPageDialog(long idItem){
            this.idItem = idItem;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.main_page_longclick_popup, container, false);


            LinearLayout root = (LinearLayout) v.findViewById(R.id.mp_popup_root);
            ListView lv = (ListView) v.findViewById(R.id.mp_popup_lv);
            lv.setDrawSelectorOnTop(true);
            lv.setSelector(R.drawable.selector_item);
            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            lv.setAdapter(new MainPageLongClickPopupAdapter(getActivity()));
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    switch (position){
                        case 0:
                            int groupPosition = ExpandableListView.getPackedPositionGroup(idItem);
                            int childPosition = ExpandableListView.getPackedPositionChild(idItem);
                            Utils.buyListEditStarted = true;
                            ((MainActivity)getActivity()).openEditBuyList(expLVAdapter.getChild(groupPosition, childPosition), groupPosition, childPosition);
                            break;
                        case 1:
                            new MyAlertDialog(idItem, position).show(getFragmentManager(),"DELETE");
                            break;
                    }
                    getDialog().dismiss();
                }
            });

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }

    // TODO MyAlertDialog
    public class MyAlertDialog extends DialogFragment {

        private long id;
        private int dialogId;
        public MyAlertDialog(){}
        public MyAlertDialog(long id, int dialogId){
            this.id = id;
            this.dialogId = dialogId;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.alert_dialog, container, false);

            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.alertDialogRoot);
            TextView title = (TextView) v.findViewById(R.id.alertDialogTitle);
            Button accept = (Button) v.findViewById(R.id.alertDialogAccept);
            Button cancel = (Button) v.findViewById(R.id.alertDialogCancel);

            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            title.setText(R.string.mp_popup_delete_title);
            title.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefTitle);

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //УДАЛЕНИЕ СПИСКА ПОКУПОК
                    groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    childPosition = ExpandableListView.getPackedPositionChild(id);

                    Bundle b = new Bundle();
                    b.putInt("idBL", expLVAdapter.getChild(groupPosition, childPosition).id);

                    startLoader(IntConst.LOADER_REMOVE_BL, b);
                    getDialog().dismiss();
                }
            });

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }


    public class MyCalendarDialog extends DialogFragment {

        private boolean firstScreen;
        private CalendarGridAdapter calendarGridAdapter;
        private Calendar calendarObj;

        int hour, initialHour;
        int minutes, initialMinutes;
        int day, initialDay;
        int month, initialMonth;

        public MyCalendarDialog(){}


        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.calendar_layout, container, false);

            final LinearLayout root = (LinearLayout) v.findViewById(R.id.CL_main_root);
            final TextView title = (TextView) v.findViewById(R.id.CL_title);

            final Button accept = (Button) v.findViewById(R.id.CL_accept);
            final Button cancel = (Button) v.findViewById(R.id.CL_cancel);
            final View divider = v.findViewById(R.id.CL_divider);
            final View shadow = v.findViewById(R.id.CL_shadow);

            final TextView monthName  = (TextView) v.findViewById(R.id.CL_month_name);
            final LinearLayout calendarRoot = (LinearLayout) v.findViewById(R.id.CL_calendar_root);
            final LinearLayout daysWeek = (LinearLayout) v.findViewById(R.id.CL_daysweek);
            final GridView calendar = (GridView) v.findViewById(R.id.CL_calendar);
            final TextView d1  = (TextView) v.findViewById(R.id.monday);
            final TextView d2  = (TextView) v.findViewById(R.id.tuesday);
            final TextView d3  = (TextView) v.findViewById(R.id.wednesday);
            final TextView d4  = (TextView) v.findViewById(R.id.thursday);
            final TextView d5  = (TextView) v.findViewById(R.id.friday);
            final TextView d6  = (TextView) v.findViewById(R.id.saturday);
            final TextView d7  = (TextView) v.findViewById(R.id.sunday);



            final RelativeLayout timePickerRoot = (RelativeLayout) v.findViewById(R.id.CL_timepicker_layout);
            final ImageButton upHour = (ImageButton) v.findViewById(R.id.CL_hour_up);
            final ImageButton downHour = (ImageButton) v.findViewById(R.id.CL_hour_down);
            final EditText enterHour  = (EditText) v.findViewById(R.id.CL_hour_enter);

            final ImageButton upMinutes = (ImageButton) v.findViewById(R.id.CL_minutes_up);
            final ImageButton downMinutes = (ImageButton) v.findViewById(R.id.CL_minutes_down);
            final EditText enterMinutes  = (EditText) v.findViewById(R.id.CL_minutes_enter);

            final TextView doubleDots  = (TextView) v.findViewById(R.id.CL_doubledots);

            calendarObj = Calendar.getInstance();

            initialDay = calendarObj.get(Calendar.DAY_OF_MONTH);
            initialMonth  = calendarObj.get(Calendar.MONTH);

            Rect displayRectangle = new Rect();
            Window window = getActivity().getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

            int dialogWidth = (int) (displayRectangle.width()*0.9);
            shadow.setLayoutParams(new LinearLayout.LayoutParams(dialogWidth, 1));

            calendarGridAdapter = new CalendarGridAdapter(getActivity(),dialogWidth);

            calendar.setAdapter(calendarGridAdapter);

            //getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            //int imgSize = (int) ((screenWidth-10*density-2*6*density)/7);

            final String[] months = getActivity().getResources().getStringArray(R.array.months);

            monthName.setText(months[calendarGridAdapter.getOrigMonth()]);

            calendar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(calendarGridAdapter.isClickable(position)) {
                        monthName.setText(months[calendarGridAdapter.getItem(position).getMonth()]);
                        calendarGridAdapter.setDate(position);
                    }
                }
            });

            divider.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);
            root.setBackgroundColor(AppPreferences.colorTheme_Background);
            title.setBackgroundColor(AppPreferences.colorTheme_Actionbar);
            daysWeek.setBackgroundColor(AppPreferences.colorTheme_Actionbar);

            firstScreen = true;

            timePickerRoot.setVisibility(View.GONE);

            title.setText(R.string.calendar_screen);
            accept.setText(R.string.color_popup_next);

            doubleDots.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextColor(AppPreferences.colorTheme_FontColor);
            enterHour.setTextColor(AppPreferences.colorTheme_FontColor);
            enterMinutes.setTextColor(AppPreferences.colorTheme_FontColor);

            monthName.setTextColor(AppPreferences.colorTheme_FontColor);
            monthName.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);

            d1.setTextColor(AppPreferences.colorTheme_FontColor);
            d2.setTextColor(AppPreferences.colorTheme_FontColor);
            d3.setTextColor(AppPreferences.colorTheme_FontColor);
            d4.setTextColor(AppPreferences.colorTheme_FontColor);
            d5.setTextColor(AppPreferences.colorTheme_FontColor);
            d6.setTextColor(AppPreferences.colorTheme_FontColor);
            d7.setTextColor(AppPreferences.colorTheme_FontColor);

            d1.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeCheckBox);
            d2.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeCheckBox);
            d3.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeCheckBox);
            d4.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeCheckBox);
            d5.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeCheckBox);
            d6.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeCheckBox);
            d7.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeCheckBox);

            accept.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);
            cancel.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);

            accept.setTextColor(AppPreferences.colorTheme_FontColor);
            cancel.setTextColor(AppPreferences.colorTheme_FontColor);



            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (firstScreen) {
                        firstScreen = false;
                        calendarRoot.setVisibility(View.GONE);
                        timePickerRoot.setVisibility(View.VISIBLE);
                        title.setText(R.string.time_screen);
                        accept.setText(R.string.accept);

                        month = calendarGridAdapter.getSelectedMonth();
                        day = calendarGridAdapter.getSelectedDay();



                        initialHour = hour = calendarObj.get(Calendar.HOUR_OF_DAY);
                        initialMinutes = minutes = calendarObj.get(Calendar.MINUTE);


                        enterHour.addTextChangedListener(new TextWatcher() {
                            boolean notChange;
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if (s.length() == 3) {
                                    notChange = true;
                                } else {
                                    notChange = false;
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if(!s.toString().isEmpty() && (Integer.parseInt(s.toString())>24 || notChange) ){
                                    s.delete(enterHour.getSelectionStart()-1,enterHour.getSelectionStart());
                                }
                            }
                        });

                        enterHour.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                String text = enterHour.getText().toString();
                                if(!hasFocus){
                                    if (text.isEmpty() || ((month == initialMonth && day == initialDay) && Integer.parseInt(text) < calendarObj.get(Calendar.HOUR_OF_DAY))) {
                                        if (hour < 10) {
                                            enterHour.setText("0".concat(String.valueOf(hour)));
                                        } else {
                                            enterHour.setText(String.valueOf(hour));
                                        }
                                    } else {
                                        hour = Integer.parseInt(text);
                                        if (hour < 10) {
                                            enterHour.setText("0".concat(String.valueOf(hour)));
                                        }
                                    }
                                }
                            }
                        });

                        enterMinutes.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                String text = enterMinutes.getText().toString();
                                if(!hasFocus){
                                    if (text.isEmpty() || ((month == initialMonth && day == initialDay) && Integer.parseInt(text) < calendarObj.get(Calendar.MINUTE))) {
                                        if (minutes < 10) {
                                            enterMinutes.setText("0".concat(String.valueOf(minutes)));
                                        } else {
                                            enterMinutes.setText(String.valueOf(minutes));
                                        }
                                    } else {
                                        minutes = Integer.parseInt(text);
                                        if (minutes < 10) {
                                            enterMinutes.setText("0".concat(String.valueOf(minutes)));
                                        }
                                    }
                                }
                            }
                        });

                        enterMinutes.addTextChangedListener(new TextWatcher() {
                            boolean notChange;
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if (s.length() == 3) {
                                    notChange = true;
                                } else {
                                    notChange = false;
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                if(!s.toString().isEmpty() && (Integer.parseInt(s.toString())>60 || notChange) ){
                                    s.delete(enterMinutes.getSelectionStart()-1,enterMinutes.getSelectionStart());
                                }
                            }
                        });



                        if(hour<10) {
                            enterHour.setText("0".concat(String.valueOf(hour)));
                        } else {
                            enterHour.setText(String.valueOf(hour));
                        }
                        if(minutes<10) {
                            enterMinutes.setText("0".concat(String.valueOf(minutes)));
                        } else {
                            enterMinutes.setText(String.valueOf(minutes));
                        }


                        upHour.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(++hour==24) {
                                    if(month == initialMonth && day == initialDay) {
                                        hour = calendarObj.get(Calendar.HOUR_OF_DAY);
                                    } else {
                                        hour = 0;
                                    }
                                }
                                if(hour<10) {
                                    enterHour.setText("0".concat(String.valueOf(hour)));
                                } else {
                                    enterHour.setText(String.valueOf(hour));
                                }
                                enterHour.setSelection(enterHour.length());
                            }
                        });
                        upMinutes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(++minutes==60) {
                                    if(month == initialMonth && day == initialDay) {
                                        if(hour > calendarObj.get(Calendar.HOUR_OF_DAY)) {
                                            minutes = 0;
                                        } else {
                                            minutes = calendarObj.get(Calendar.MINUTE);
                                        }
                                    } else {
                                        minutes = 0;
                                    }
                                }
                                if(minutes<10) {
                                    enterMinutes.setText("0".concat(String.valueOf(minutes)));
                                } else {
                                    enterMinutes.setText(String.valueOf(minutes));
                                }
                                enterMinutes.setSelection(enterMinutes.length());
                            }
                        });
                        downHour.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(month == initialMonth && day == initialDay) {
                                    if(--hour==calendarObj.get(Calendar.HOUR_OF_DAY)-1) {
                                        hour = 23;
                                    }
                                    if(hour==calendarObj.get(Calendar.HOUR_OF_DAY) && minutes < calendarObj.get(Calendar.MINUTE)){
                                        minutes = calendarObj.get(Calendar.MINUTE);
                                        if(minutes<10) {
                                            enterMinutes.setText("0".concat(String.valueOf(minutes)));
                                        } else {
                                            enterMinutes.setText(String.valueOf(minutes));
                                        }
                                    }
                                } else {
                                    if(--hour==-1) {
                                        hour = 23;
                                    }
                                }
                                if(hour<10) {
                                    enterHour.setText("0".concat(String.valueOf(hour)));
                                } else {
                                    enterHour.setText(String.valueOf(hour));
                                }
                                enterHour.setSelection(enterHour.length());
                            }
                        });
                        downMinutes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(month == initialMonth && day == initialDay) {
                                    if(hour > calendarObj.get(Calendar.HOUR_OF_DAY)) {
                                        if(--minutes==-1) {
                                            minutes = 59;
                                        }
                                    } else {
                                        if (--minutes == calendarObj.get(Calendar.MINUTE) - 1) {
                                            minutes = 59;
                                        }
                                    }
                                } else {
                                    if(--minutes==-1) {
                                        minutes = 59;
                                    }
                                }
                                if(minutes<10) {
                                    enterMinutes.setText("0".concat(String.valueOf(minutes)));
                                } else {
                                    enterMinutes.setText(String.valueOf(minutes));
                                }
                                enterMinutes.setSelection(enterMinutes.length());
                            }
                        });

                    } else {
                        calendarObj = Calendar.getInstance();
                        if(month == calendarObj.get(Calendar.MONTH) && day == calendarObj.get(Calendar.DAY_OF_MONTH)) {
                            if (minutes <= calendarObj.get(Calendar.MINUTE) && hour <= calendarObj.get(Calendar.HOUR_OF_DAY)) {
                                Toast.makeText(getActivity(), R.string.error_date, Toast.LENGTH_LONG).show();
                                getDialog().dismiss();
                                return;
                            }
                        }

                        //set notification
                        getDialog().dismiss();
                    }

                }
            });

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }

    public void invalidate(boolean old, int groupPos, int childPos, boolean hadChange) {
        if (hadChange) {
            if(old)
                expLVAdapter.moveToOld(groupPos, childPos);
            else
                expLVAdapter.moveFromOld(groupPos, childPos);
        }
        expLVAdapter.notifyDataSetChanged();
    }

    public void addNewItem(BuyList buyList){
        Utils.reFillTodayAndWeekArrList();
        expLVAdapter.addBuyList(0, buyList);
        expLVAdapter.notifyDataSetChanged();
        Utils.expandedBuyLists.add(0, true);
        expandAllWasExpanded();
    }


    public interface MainPageFragmentListener {
        public void clickOnBuyListViewItem();
        public void clickOnBtnCreateBuyList();
    }


}
