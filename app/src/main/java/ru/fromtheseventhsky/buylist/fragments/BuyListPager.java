package ru.fromtheseventhsky.buylist.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.adapters.BuyListViewPagerCustomAdapter;
import ru.fromtheseventhsky.buylist.utils.BuyListItem;

public class BuyListPager extends Fragment{

    BuyListFragment buyListFragment;
    FragmentCreateBuyListItem fragmentCreateBuyListItem;
    ViewPager vp;

    private OnFragmentInteractionListener mListener;

    public static BuyListPager newInstance() {
        BuyListPager fragment = new BuyListPager();
        return fragment;
    }

    public BuyListPager() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    public void notifyAdapter(){
        buyListFragment.notifyCategoryHadAdded();
    }



    public void cancelDeleteMode(){
        buyListFragment.cancelDeleteMode();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.buy_list_pager, container, false);
        vp = (ViewPager) v.findViewById(R.id.buy_list_view_pager);
        vp.setAdapter(new BuyListViewPagerCustomAdapter(getFragmentManager(),
                (buyListFragment = BuyListFragment.newInstance()),
                (fragmentCreateBuyListItem = FragmentCreateBuyListItem.newInstance())));
        vp.setOffscreenPageLimit(3);
        return v;
    }


    public void onBtnCreateNewPressed() {
        fragmentCreateBuyListItem.showSoftKeyboard();
        vp.setCurrentItem(1);
    }

    public void setBLI(BuyListItem bli){
        buyListFragment.setData(bli);
    }
    public void editBLI(BuyListItem bli){
        fragmentCreateBuyListItem.setEditMode(bli);
    }
    public void sortBLI(){
        buyListFragment.sortData();
    }
    public void updateBuyListBottomPanelInfo() {
        buyListFragment.updateBtmPanelTextInfo();
    }


    public void getList(){
        buyListFragment.openFragment();
    }
    public int getCurrentPage(){
        return vp.getCurrentItem();
    }
    public void setCurrentPage(int i){
        vp.setCurrentItem(i);
    }

    public void clearAll(){
        fragmentCreateBuyListItem.clearAll();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    public interface OnFragmentInteractionListener {
        public void onBackNeedPress();
    }
}
