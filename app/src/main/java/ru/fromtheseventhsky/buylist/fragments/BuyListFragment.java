package ru.fromtheseventhsky.buylist.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import ru.fromtheseventhsky.buylist.MainActivity;
import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.adapters.BuyListExpListViewAdapter;
import ru.fromtheseventhsky.buylist.loaders.LoaderBuyList;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyListItem;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BuyListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BuyListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BuyListFragment extends Fragment implements LoaderManager.LoaderCallbacks<HashMap<Integer, ArrayList<BuyListItem>>> {

    private static final String TABLE_NAME = "TABLE_NAME";

    private ImageButton btnCreateBuyListItem;
    private ExpandableListView expandableListView;
    private RelativeLayout root;

    private RelativeLayout rootBtmPanel;
    private TextView textBtmPanel;
    private ImageButton actionBtmPanel;
    private View borderBtmPanel;

    private float bottomForAnim;

    private BuyListItem buyListItem;

    private boolean canAnimateUpDown;

    private ArrayList<Integer> deleteList;
    private long checkedCount;

    private ObjectAnimator animateBtnUp;
    private ObjectAnimator animateBtnDown;

    private ObjectAnimator animateFade;

    private int commodityId;


    private final int DIRECTION_RIGHT = 1;
    private final int DIRECTION_LEFT = -1;

    private boolean downloadData;


    private boolean imageTap = false;

    private int displayHeight;
    private int depth;
    private int maxLength;
    private int workLength;

    private boolean topItem;
    private boolean hadStartedAnim;
    private View topItemOfLV;
    private Rect rect;
    private float length;
    private int initLength = -depth;
    private float initY;
    private float motionY;
    private ObjectAnimator animateUp;
    private ObjectAnimator animateUpToUpdate;
    private ObjectAnimator animateUpdate;
    private boolean up;
    DisplayMetrics dm;

    View content;


    private String deleteText;

    BuyListExpListViewAdapter adapter;

    private OnFragmentInteractionListener mListener;


    public static BuyListFragment newInstance() {
        BuyListFragment fragment = new BuyListFragment();
        return fragment;
    }

    public BuyListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setColorTheme(){
        root.setBackgroundColor(AppPreferences.colorTheme_Background);
        rootBtmPanel.setBackgroundColor(AppPreferences.colorTheme_Actionbar);
        textBtmPanel.setTextColor(AppPreferences.colorTheme_FontColor);
        textBtmPanel.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        borderBtmPanel.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);
        btnCreateBuyListItem.getBackground().setColorFilter(AppPreferences.colorTheme_DeselectedBtnColor, PorterDuff.Mode.MULTIPLY);
        //btnCreateBuyListItem.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.buy_list_fragment, container, false);
        btnCreateBuyListItem = (ImageButton) v.findViewById(R.id.btn_lv_create_item);
        expandableListView = (ExpandableListView) v.findViewById(R.id.buy_list_exp_list_view);
        root = (RelativeLayout) v.findViewById(R.id.buy_list_root);

        rootBtmPanel = (RelativeLayout) v.findViewById(R.id.buy_list_bottom_panel);
        textBtmPanel = (TextView) v.findViewById(R.id.buy_list_bottom_panel_info);
        actionBtmPanel = (ImageButton) v.findViewById(R.id.buy_list_operate_with_item);
        borderBtmPanel = v.findViewById(R.id.buy_list_border_btm_panel);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        displayHeight = point.y;
        dm = new DisplayMetrics();
        display.getMetrics(dm);
        bottomForAnim = 120*dm.density;
        depth = 50;
        maxLength = (int) (displayHeight/4.5);
        workLength = displayHeight/8;
        rect = new Rect();

        rootBtmPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        content = getActivity().getWindow().findViewById(Window.ID_ANDROID_CONTENT);

        setColorTheme();

        hadStartedAnim = false;
        topItemOfLV = null;

        canAnimateUpDown = false;

        animateBtnUp = ObjectAnimator.ofFloat(btnCreateBuyListItem, "translationY", btnCreateBuyListItem.getTranslationY(), bottomForAnim);
        animateBtnUp.setDuration(100);

        animateBtnDown = ObjectAnimator.ofFloat(btnCreateBuyListItem, "translationY", btnCreateBuyListItem.getTranslationY(), bottomForAnim);
        animateBtnDown.setDuration(100);

        expandableListView.setSelector(R.drawable.selector_item);
        expandableListView.setDrawSelectorOnTop(true);



        expandableListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                    }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    if(totalItemCount>0) {
                        if(totalItemCount>visibleItemCount){
                            canAnimateUpDown = true;
                        } else {
                            canAnimateUpDown = false;
                        }
                    }
                }
        });



        expandableListView.setOnTouchListener(new View.OnTouchListener() {
            float xPos = 0, yPos = 0;

            int dimension = 0;

            int direction = 0;

            boolean animationStarted = false;


            boolean canSwipe = true;

            boolean actionDownCalled = false;


            Button itemBtnAccept;
            Button itemBtnCancel;

            int selectedPositionWhileTouching;

            View v1;
            RelativeLayout mainRoot;
            LinearLayout secondRoot;
            RelativeLayout darkRoot;
            TextView secondRootTitle;

            int groupPos;
            int childPos;

            float initXForSwipe;
            float initYForSwipe;
            int initXRawForSwipe;
            int initYRawForSwipe;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ExpandableListView lv = (ExpandableListView) v;

                switch (event.getAction()){
                    // TODO ACTION_DOWN

                    case MotionEvent.ACTION_DOWN: {
                        initXForSwipe = event.getRawX();
                        initYForSwipe = event.getRawY();
                        actionDownCalled = true;
                        Rect rect = new Rect();
                        int childCount = lv.getChildCount();
                        int[] listViewCoordinates = new int[2];
                        lv.getLocationOnScreen(listViewCoordinates);
                        initXRawForSwipe = (int) event.getRawX() - listViewCoordinates[0];
                        initYRawForSwipe = (int) event.getRawY() - listViewCoordinates[1];
                        View child;

                        for (int i = 0; i < childCount; i++) {
                            child = lv.getChildAt(i);
                            child.getHitRect(rect);
                            if (rect.contains(initXRawForSwipe, initYRawForSwipe)) {
                                selectedPositionWhileTouching = lv.getFirstVisiblePosition()+i;

                                if(selectedPositionWhileTouching==0) {
                                    canSwipe = false;
                                    break;
                                }

                                long packedPosition = lv.getExpandableListPosition(selectedPositionWhileTouching);


                                if(ExpandableListView.getPackedPositionType(packedPosition)==ExpandableListView.PACKED_POSITION_TYPE_GROUP){
                                    canSwipe = false;
                                    v1 = null;
                                    break;
                                }
                                groupPos = ExpandableListView.getPackedPositionGroup(packedPosition);
                                childPos = ExpandableListView.getPackedPositionChild(packedPosition);
                                buyListItem =  adapter.getChild(groupPos, childPos);

                                canSwipe = true;

                                v1 = child;
                                mainRoot = (RelativeLayout) v1.findViewById(R.id.Item_BLI_main_root);
                                secondRoot = (LinearLayout) v1.findViewById(R.id.Item_BLI_third_root);
                                darkRoot = (RelativeLayout) v1.findViewById(R.id.Item_BLI_shadow);
                                secondRootTitle = (TextView) v1.findViewById(R.id.Item_BLI_thirdLayTitle);
                                if(!buyListItem.isBought())
                                    secondRootTitle.setText(R.string.buy_list_item_bought);
                                else
                                    secondRootTitle.setText(R.string.buy_list_item_not_bought);
                                break;
                            }
                        }


                        xPos = event.getX();
                        yPos = event.getY();
                        dimension = 0;
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        actionDownCalled = false;
                        if (dimension == 1) {
                            if(v1!=null) {
                                ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                transAnimation.setDuration(300);
                                transAnimation.setInterpolator(new OvershootInterpolator());
                                transAnimation.start();
                                v1.setPressed(false);
                            }
                        }
                        break;
                    }

                    //TODO ACTION_UP
                    case MotionEvent.ACTION_UP: {
                        if(Math.abs(initXForSwipe-event.getRawX())<10 && Math.abs(initYForSwipe-event.getRawY())<10){
                            Rect rect = new Rect();
                            View child = null;
                            if(v1!=null) {
                                child = v1.findViewById(R.id.Item_BLI_photo);
                            }
                            if(child!=null && child.getVisibility()!=View.GONE) {
                                child.getHitRect(rect);
                                int[] listViewCoordinates = new int[2];
                                child.getLocationOnScreen(listViewCoordinates);
                                initXForSwipe-=listViewCoordinates[0];
                                initYForSwipe-=listViewCoordinates[1];
                                if (rect.contains((int)initXForSwipe, (int)initYForSwipe)) {
                                    child.performClick();
                                    imageTap = true;
                                    return false;
                                }
                            }
                        }
                        actionDownCalled = false;
                        if (dimension == 1) {
                            if(v1!=null) {
                                if(direction==DIRECTION_RIGHT){
                                    if(mainRoot.getTranslationX()>dm.widthPixels/3.2){

                                        animateUp();
                                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                        transAnimation.setDuration(100);
                                        //transAnimation.setInterpolator(new OvershootInterpolator());
                                        transAnimation.start();

                                        ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), -dm.widthPixels);
                                        transAnimationSecond.setDuration(100);
                                        //transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                        transAnimationSecond.addListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                secondRoot.setVisibility(View.GONE);
                                                startBoughtLoader(!buyListItem.isBought());

                                                changePositionInList(buyListItem.isBought(), buyListItem);
                                                expandAllWasExpanded();
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });
                                        transAnimationSecond.start();
                                        animateUp();
                                        v1.setPressed(false);
                                    } else {
                                        animateUp();
                                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                        transAnimation.setDuration(300);
                                        transAnimation.setInterpolator(new OvershootInterpolator());
                                        transAnimation.start();

                                        ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), -dm.widthPixels);
                                        transAnimationSecond.setDuration(300);
                                        transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                        transAnimationSecond.addListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                secondRoot.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });
                                        transAnimationSecond.start();

                                        v1.setPressed(false);
                                    }
                                } else {
                                    if(Math.abs(mainRoot.getTranslationX())>dm.widthPixels/3.2){
                                        animateUp();
                                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                        transAnimation.setDuration(100);
                                        //transAnimation.setInterpolator(new OvershootInterpolator());
                                        transAnimation.start();

                                        ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), dm.widthPixels);
                                        transAnimationSecond.setDuration(100);
                                        //transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                        transAnimationSecond.addListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                secondRoot.setVisibility(View.GONE);
                                                startBoughtLoader(!buyListItem.isBought());

                                                changePositionInList(buyListItem.isBought(), buyListItem);

                                                expandAllWasExpanded();
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });
                                        transAnimationSecond.start();
                                        animateUp();
                                        v1.setPressed(false);
                                    } else {
                                        animateUp();
                                        ObjectAnimator transAnimation = ObjectAnimator.ofFloat(mainRoot, "translationX", mainRoot.getTranslationX(), 0);
                                        transAnimation.setDuration(300);
                                        transAnimation.setInterpolator(new OvershootInterpolator());
                                        transAnimation.start();

                                        ObjectAnimator transAnimationSecond = ObjectAnimator.ofFloat(secondRoot, "translationX", secondRoot.getTranslationX(), dm.widthPixels);
                                        transAnimationSecond.setDuration(300);
                                        transAnimationSecond.setInterpolator(new OvershootInterpolator());
                                        transAnimationSecond.addListener(new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                secondRoot.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });
                                        transAnimationSecond.start();

                                        v1.setPressed(false);
                                    }
                                }
                            }
                        }
                        if(v1!=null)
                            v1.setPressed(false);
                        break;
                    }

                    //TODO ACTION_MOVE

                    case MotionEvent.ACTION_MOVE: {
                        if (actionDownCalled) {
                            if (dimension == 0) {
                                if (Math.abs(Math.abs(xPos) - Math.abs(event.getX())) > 20) {
                                    dimension = 1;
                                }

                                initY = event.getY();
                                //Log.d("initX", "" + initX);
                                if (Math.abs(Math.abs(yPos) - Math.abs(event.getY())) > 20)
                                    dimension = 2;
                            }
                            if (dimension == 1 && !animationStarted && canSwipe) {
                                if (v1 != null) {
                                    if ((boolean) mainRoot.getTag()) {
                                        animateDown();
                                        mainRoot.setTranslationX(event.getX() - xPos);

                                        if (secondRoot.getVisibility() == View.GONE) {
                                            secondRoot.setVisibility(View.VISIBLE);
                                            v1.setPressed(false);
                                            //secondRoot.setLayoutParams(new LinearLayout.LayoutParams(mainRoot.getMeasuredWidth(), mainRoot.getMeasuredHeight()));
                                        }
                                        if (xPos - event.getX() < 0) {
                                            direction = DIRECTION_RIGHT;
                                            secondRootTitle.setGravity(Gravity.RIGHT);
                                        } else {
                                            direction = DIRECTION_LEFT;
                                            secondRootTitle.setGravity(Gravity.LEFT);
                                        }
                                        if (direction > 0) {
                                            secondRoot.setTranslationX(event.getX() - xPos - dm.widthPixels);
                                        } else {
                                            secondRoot.setTranslationX(event.getX() - xPos + dm.widthPixels);
                                        }
                                    }
                                }

                                event.setAction(MotionEvent.ACTION_CANCEL);
                            }
                            if (dimension == 2) {
                                if (canAnimateUpDown) {
                                    //если листаем вверх
                                    if (initY - event.getY() > 3) {
                                        initY = event.getY();
                                        animateDown();
                                    }
                                    //если листаем вниз
                                    if (initY - event.getY() < -3) {
                                        initY = event.getY();
                                        animateUp();
                                    }
                                }

                                return false;
                            }
                            return false;
                        }
                    }
                    //break;

                }
                return false;
            }
        });



        btnCreateBuyListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnCreateNewPressed();
            }
        });


        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                animateUp();
                if(expandableListView.isGroupExpanded(groupPosition)){
                    adapter.setGroupCollapsed(groupPosition);
                } else {
                    adapter.setGroupExpanded(groupPosition);
                }

                return false;
            }
        });
        final View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.buyListItemEditStarted = true;
                changeTabToEditBLI(adapter.getChild((Integer)v.getTag(R.id.BL_comment), (Integer)v.getTag(R.id.BL_comment_tv)));
            }
        };

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if(imageTap){
                    imageTap = false;
                    return false;
                }
                if(!Utils.buyListItemDeleteMode) {
                    if (AppPreferences.doubleTapToEdit) {
                    /*if(childClickListenerCh == childPosition && childClickListenerG == groupPosition) {
                        if (childClickListenerTap + 1000 > System.currentTimeMillis()) {
                            changeTabToEditBLI(adapter.getChild(groupPosition, childPosition));
                            Utils.buyListItemEditStarted = true;
                        }
                    } else {
                        childClickListenerCh = childPosition;
                        childClickListenerG = groupPosition;
                        childClickListenerTap = System.currentTimeMillis();
                        Toast.makeText(getActivity(), getString(R.string.double_press_edit), Toast.LENGTH_SHORT).show();
                    }*/
                        View tv = v.findViewById(R.id.Item_BLI_doubleClickEdit);
                        tv.setVisibility(View.VISIBLE);

                        tv.setTag(R.id.BL_comment, groupPosition);
                        tv.setTag(R.id.BL_comment_tv, childPosition);
                        tv.setOnClickListener(listener);
                        animateFadeForDoubleClick(tv);

                    } else {
                        changeTabToEditBLI(adapter.getChild(groupPosition, childPosition));
                        Utils.buyListItemEditStarted = true;
                    }
                } else {
                    CheckBox chb = (CheckBox) v.findViewById(R.id.Item_BLI_delete_chb);
                    if(chb.isChecked()) {
                        checkedCount--;
                    } else {
                        checkedCount++;
                    }
                    chb.setChecked(!chb.isChecked());
                    adapter.setDeletePosition(groupPosition, childPosition, chb.isChecked());

                    if(checkedCount>0)
                        textBtmPanel.setText(deleteText.concat(" (").concat(String.valueOf(checkedCount)).concat(")"));
                    else
                        textBtmPanel.setText(deleteText);
                }

                return false;
            }
        });

        actionBtmPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Utils.buyListItemDeleteMode) {
                    onBtnCreateNewPressed();
                } else {
                    new MyAlertDialog().show(getFragmentManager(),"removeDialog");
                }
            }
        });

        expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(!Utils.buyListItemDeleteMode) {
                    if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                        if (deleteList == null) {
                            deleteList = new ArrayList<Integer>();
                        } else {
                            deleteList.clear();
                        }
                        long packedPosition = expandableListView.getExpandableListPosition(position);
                        adapter.setDeleteModeOn(ExpandableListView.getPackedPositionGroup(packedPosition), ExpandableListView.getPackedPositionChild(packedPosition));
                        setDeleteMode();
                        return true;
                    }
                } else {
                    ((MainActivity)getActivity()).setTitleGoodsList();
                    cancelDeleteMode();
                    return true;
                }
                return false;
            }
        });

        HashMap<Integer, ArrayList<BuyListItem>> buyListExp = new HashMap<Integer, ArrayList<BuyListItem>>();
        ArrayList<Integer> dayHave = new ArrayList<>();

        adapter = new BuyListExpListViewAdapter(buyListExp, dayHave, getActivity(), getFragmentManager());
        expandableListView.setAdapter(adapter);

        deleteText = getActivity().getString(R.string.delete_mode_add_btn_title_btn_on);

        return v;
    }

    public void setDeleteMode(){
        ((MainActivity)getActivity()).setTitleDeleteMode();
        checkedCount = 1;
        actionBtmPanel.setImageResource(R.drawable.selector_delete);
        Utils.buyListItemDeleteMode = true;
        textBtmPanel.setText(deleteText.concat(" (1)"));
    }

    public void cancelDeleteMode(){
        checkedCount = 0;
        String positions = getActivity().getString(R.string.positions);
        String blCost = getActivity().getString(R.string.cost);
        adapter.setDeleteModeOff();
        if(AppPreferences.costAvailable) {
            textBtmPanel.setText(positions
                    .concat(" ")
                    .concat(String.valueOf(Utils.positionsInCurrentList))
                    .concat("/")
                    .concat(String.valueOf(Utils.boughtInCurrentList))
                    .concat("\n")
                    .concat(blCost)
                    .concat(" ")
                    .concat(String.valueOf(Utils.costInCurrentList))
                    .concat(" ")
                    .concat(AppPreferences.currency));
        } else {
            textBtmPanel.setText(positions
                    .concat(" ")
                    .concat(String.valueOf(Utils.positionsInCurrentList))
                    .concat("/")
                    .concat(String.valueOf(Utils.boughtInCurrentList)));
        }
        actionBtmPanel.setImageResource(R.drawable.selector_add);
        Utils.buyListItemDeleteMode = false;
    }

    public void updateBtmPanelTextInfo(){
        String positions = getActivity().getString(R.string.positions);
        String blCost = getActivity().getString(R.string.cost);
        if(AppPreferences.costAvailable) {
            textBtmPanel.setText(positions
                    .concat(" ")
                    .concat(String.valueOf(Utils.positionsInCurrentList))
                    .concat("/")
                    .concat(String.valueOf(Utils.boughtInCurrentList))
                    .concat("\n")
                    .concat(blCost)
                    .concat(" ")
                    .concat(String.valueOf(Utils.costInCurrentList))
                    .concat(" ")
                    .concat(AppPreferences.currency));
        } else {
            textBtmPanel.setText(positions
                    .concat(" ")
                    .concat(String.valueOf(Utils.positionsInCurrentList))
                    .concat("/")
                    .concat(String.valueOf(Utils.boughtInCurrentList)));
        }
    }



    //TODO openFragment and get data
    public void openFragment(){
        downloadData = true;
    }

    public void changePositionInList(boolean bought, BuyListItem buyListItem){
        if(bought) {
            adapter.moveFromBought(buyListItem);
        } else {
            adapter.moveToBought(buyListItem);
        }
        buyListItem.setBought(!buyListItem.isBought());
    }

    public void startOpenLoader(){
        getLoaderManager().initLoader(IntConst.LOADER_GET_BLI, null, this).forceLoad();
    }
    public void startRemoveLoader(){
        getLoaderManager().initLoader(IntConst.LOADER_REMOVE_COMMODITY, null, this).forceLoad();
    }

    public void startBoughtLoader(boolean bought){
        commodityId = buyListItem.getId();
        if(bought) {
            getLoaderManager().initLoader(IntConst.LOADER_SET_BOUGHT_TRUE, null, this).forceLoad();
        } else {
            getLoaderManager().initLoader(IntConst.LOADER_SET_BOUGHT_FALSE, null, this).forceLoad();
        }
    }

    public void setData(BuyListItem buyListItem){
        adapter.addCommodity(buyListItem);
        int position = adapter.getGroupIdForExpand(buyListItem);
        updateBtmPanelTextInfo();
        adapter.setGroupExpanded(position);
        expandAllWasExpanded();
    }
    public void sortData(){
        adapter.moveIfNeed();
        adapter.rebuildGroups();
        adapter.sortLists();
        adapter.notifyDataSetChanged();

    }

    public void notifyCategoryHadAdded(){
        adapter.addNewCategory();
    }

    private void animateFadeForDoubleClick(final View v){
        animateFade = ObjectAnimator.ofFloat(v, "alpha",  1f, 0);
        animateFade.setDuration(500);
        animateFade.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animateFade.start();
    }

    void animateUp(){
        if(animateBtnUp.isStarted())
            animateBtnUp.cancel();

        if(!animateBtnDown.isStarted()) {
            animateBtnDown = ObjectAnimator
                    .ofFloat(btnCreateBuyListItem, "translationY",
                            btnCreateBuyListItem.getTranslationY(), 0);
            animateBtnDown.start();
        }
    }
    void animateDown(){
        if (animateBtnDown.isStarted())
            animateBtnDown.cancel();

        if (!animateBtnUp.isStarted()) {
            animateBtnUp = ObjectAnimator
                    .ofFloat(btnCreateBuyListItem, "translationY",
                            btnCreateBuyListItem.getTranslationY(), bottomForAnim);
            animateBtnUp.start();
        }
    }


    public void onBtnCreateNewPressed() {
        if (mListener != null) {
            mListener.onBtnCreateNewPressed();
        }
    }
    public void changeTabToBL() {
        if (mListener != null) {
            mListener.changeTabToBL();
        }
    }
    public void changeTabToEditBLI(BuyListItem buyListItem) {
        if (mListener != null) {
            mListener.changeTabToEditBLI(buyListItem);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        if(downloadData){
            downloadData = false;
            startOpenLoader();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //TODO onCreateLoader
    @Override
    public Loader<HashMap<Integer, ArrayList<BuyListItem>>> onCreateLoader(int id, Bundle args) {
        Utils.loaderStarted = true;
        switch (id){
            case IntConst.LOADER_GET_BLI:
                return new LoaderBuyList(getActivity(), Utils.idOfCalledBuyList, IntConst.LOADER_GET_BLI);
            case IntConst.LOADER_SET_BOUGHT_TRUE:
                return new LoaderBuyList(getActivity(), Utils.idOfCalledBuyList, commodityId, IntConst.LOADER_SET_BOUGHT_TRUE);
            case IntConst.LOADER_SET_BOUGHT_FALSE:
                return new LoaderBuyList(getActivity(), Utils.idOfCalledBuyList, commodityId, IntConst.LOADER_SET_BOUGHT_FALSE);
            case IntConst.LOADER_REMOVE_COMMODITY:
                return new LoaderBuyList(getActivity(), Utils.idOfCalledBuyList, deleteList, IntConst.LOADER_REMOVE_COMMODITY);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<HashMap<Integer, ArrayList<BuyListItem>>> loader, HashMap<Integer, ArrayList<BuyListItem>> data) {

        switch (loader.getId()){
            case IntConst.LOADER_GET_BLI:{
                ArrayList<Integer> categoriesHave = new ArrayList<>();


                if(AppPreferences.categoryAvailable){
                    for (int i = 0; i < Utils.goodsCategories.size(); i++) {
                        if(data.get(Utils.goodsCategories.get(i).getId()).size()>0){
                            categoriesHave.add(Utils.goodsCategories.get(i).getId());
                        }
                    }
                    if(data.get(IntConst.ID_BOUGHT_CAT_ON).size()>0) {
                        categoriesHave.add(IntConst.ID_BOUGHT_CAT_ON);
                    }

                } else {
                    if(data.get(IntConst.ID_NOT_BOUGHT_CAT_OFF).size()>0) {
                        categoriesHave.add(IntConst.ID_NOT_BOUGHT_CAT_OFF);
                    }
                    if(data.get(IntConst.ID_BOUGHT_CAT_OFF).size()>0) {
                        categoriesHave.add(IntConst.ID_BOUGHT_CAT_OFF);
                    }

                }

                adapter.setNewGroup(data, categoriesHave);
                expandAll();
                expandableListView.setVisibility(View.VISIBLE);
                //try {
                //    TimeUnit.SECONDS.sleep(2);
                //    changeTabToBL();
                //} catch (InterruptedException e) {
                //    //Handle exception
                //}
                updateBtmPanelTextInfo();
                changeTabToBL();
                getLoaderManager().destroyLoader(IntConst.LOADER_GET_BLI);
            }
                break;
            case IntConst.LOADER_SET_BOUGHT_TRUE:
                updateBtmPanelTextInfo();
                getLoaderManager().destroyLoader(IntConst.LOADER_SET_BOUGHT_TRUE);
                break;
            case IntConst.LOADER_SET_BOUGHT_FALSE:
                updateBtmPanelTextInfo();
                getLoaderManager().destroyLoader(IntConst.LOADER_SET_BOUGHT_FALSE);
                break;
            case IntConst.LOADER_REMOVE_COMMODITY:
                adapter.deleteObjInDeleteList();
                updateBtmPanelTextInfo();
                ((MainActivity)getActivity()).setTitleGoodsList();
                cancelDeleteMode();
                getLoaderManager().destroyLoader(IntConst.LOADER_REMOVE_COMMODITY);
                break;
        }
        Utils.loaderStarted = false;

    }

    @Override
    public void onLoaderReset(Loader<HashMap<Integer, ArrayList<BuyListItem>>> loader) {
        Utils.loaderStarted = false;
    }

    private void expandAll(){
        for ( int i = 0; i < adapter.getGroupCount(); i++ ) {
            expandableListView.expandGroup(i);
            adapter.setGroupExpanded(i);
        }
    }

    private void expandAllWasExpanded(){
        for ( int i = 0; i < adapter.getGroupCount(); i++ ) {
            if(adapter.isGroupExpanded(i)) {
                expandableListView.expandGroup(i);
            } else {
                expandableListView.collapseGroup(i);
            }
        }
    }

    // TODO MyAlertDialog
    public class MyAlertDialog extends DialogFragment {

        private long id;
        private int dialogId;
        public MyAlertDialog(){}
        public MyAlertDialog(long id, int dialogId){
            this.id = id;
            this.dialogId = dialogId;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.alert_dialog, container, false);

            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.alertDialogRoot);
            TextView title = (TextView) v.findViewById(R.id.alertDialogTitle);
            Button accept = (Button) v.findViewById(R.id.alertDialogAccept);
            Button cancel = (Button) v.findViewById(R.id.alertDialogCancel);

            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            title.setText(R.string.mp_popup_delete_title);
            title.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefTitle);

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.fillDeleteList(deleteList);
                    startRemoveLoader();
                    getDialog().dismiss();
                }
            });

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }


    public interface OnFragmentInteractionListener {
        public void onBtnCreateNewPressed();
        public void changeTabToBL();
        public void changeTabToEditBLI(BuyListItem bli);
    }

}
