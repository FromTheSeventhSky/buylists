package ru.fromtheseventhsky.buylist.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.fromtheseventhsky.buylist.MainActivity;
import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.adapters.PreferencesCategoriesAdapter;
import ru.fromtheseventhsky.buylist.loaders.LoaderCreateBuyListItem;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.GoodsCategory;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class PreferenceCategoriesFragment extends Fragment implements LoaderManager.LoaderCallbacks<Long> {

    private PreferencesCategoriesAdapter adapter;

    public PreferenceCategoriesFragment(){}

    private RelativeLayout root;
    private ListView lv;
    private Button accept;

    private int checkedCount;

    private String deleteTitle;
    private String acceptTitle;


    LoaderManager.LoaderCallbacks callback;

    private ArrayList<Integer> deleteList;

    public static PreferenceCategoriesFragment newInstance() {
        PreferenceCategoriesFragment fragment = new PreferenceCategoriesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_categories_preference, container, false);

        root = (RelativeLayout) v.findViewById(R.id.preferencesCategoriesRoot);
        lv = (ListView) v.findViewById(R.id.preferencesCategoriesLV);

        deleteTitle = getText(R.string.delete_mode_add_btn_title_btn_on).toString();
        acceptTitle = getText(R.string.category_add).toString();

        accept = (Button) v.findViewById(R.id.preferencesCategoriesAccept);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.deleteMode){
                    if(checkedCount==0){
                        Toast.makeText(getActivity(), getText(R.string.delete_mode_title_toast), Toast.LENGTH_LONG).show();
                        return;
                    }
                    adapter.fillDeleteList(deleteList);
                    getLoaderManager().initLoader(IntConst.LOADER_REMOVE_CATEGORY, null, callback).forceLoad();
                } else {
                    new PreferencesAddCategoryDialog(IntConst.CREATE).show(getFragmentManager(), "CREATE");
                }
            }
        });
        callback = this;

        applyTheme();

        deleteList = new ArrayList<>();

        adapter = new PreferencesCategoriesAdapter(inflater, getLoaderManager(), getActivity());



        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter.getItemId(position) != -1) {
                    if (!Utils.deleteMode) {
                        new PreferencesAddCategoryDialog(IntConst.EDIT, position).show(getFragmentManager(), "EDIT");
                    } else {

                        CheckBox chb = (CheckBox) view.findViewById(R.id.PrefCat_chb);
                        if (chb.isChecked()) {
                            checkedCount--;
                        } else {
                            checkedCount++;
                        }
                        chb.setChecked(!chb.isChecked());
                        if (checkedCount > 0)
                            accept.setText(deleteTitle.concat(" (").concat(String.valueOf(checkedCount)).concat(")"));
                        else
                            accept.setText(deleteTitle);
                    }
                }
            }
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter.getItemId(position) != -1) {
                    switchDeleteMode(position);
                    if (Utils.deleteMode) {
                        checkedCount++;
                        accept.setText(deleteTitle.concat(" (").concat(String.valueOf(checkedCount)).concat(")"));
                    }
                    ((MainActivity) getActivity()).setTitleDeleteMode();

                    return true;
                }
                return false;
            }
        });

        return v;
    }

    public void switchDeleteMode(int pos){
        checkedCount = 0;
        Utils.deleteMode = Utils.deleteMode ? false : true;
        accept.setText(Utils.deleteMode ? deleteTitle : acceptTitle);
        adapter.setRemoveMode(Utils.deleteMode, pos);
    }

    public void applyTheme(){
        accept.setBackgroundDrawable(Utils.getStateList());
        root.setBackgroundColor(AppPreferences.colorTheme_Background);
        accept.setTextColor(AppPreferences.colorTheme_FontColor);
        accept.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
    }

    @Override
    public Loader<Long> onCreateLoader(int id, Bundle bundle) {
        switch (id){
            case IntConst.LOADER_ADD_CATEGORY:
                return new LoaderCreateBuyListItem(getActivity(), IntConst.LOADER_ADD_CATEGORY);
            case IntConst.LOADER_REMOVE_CATEGORY:
                return new LoaderCreateBuyListItem(getActivity(), deleteList, IntConst.LOADER_REMOVE_CATEGORY);
            case IntConst.LOADER_UPDATE_CATEGORY_NAME:
                return new LoaderCreateBuyListItem(getActivity(), bundle, IntConst.LOADER_UPDATE_CATEGORY_NAME);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Long> longLoader, Long aLong) {
        switch (longLoader.getId()){
            case IntConst.LOADER_ADD_CATEGORY:
                adapter.notifyDataSetChanged();
                getLoaderManager().destroyLoader(IntConst.LOADER_ADD_CATEGORY);
                break;
            case IntConst.LOADER_REMOVE_CATEGORY:
                adapter.notifyDataSetChanged();
                switchDeleteMode(-1);
                getLoaderManager().destroyLoader(IntConst.LOADER_REMOVE_CATEGORY);
                break;
            case IntConst.LOADER_UPDATE_CATEGORY_NAME:
                getLoaderManager().destroyLoader(IntConst.LOADER_UPDATE_CATEGORY_NAME);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Long> longLoader) {

    }



    //TODO startLoader method
    public void startLoader(int id, Bundle b){
        getLoaderManager().initLoader(id, b, callback).forceLoad();
    }

    public class PreferencesAddCategoryDialog extends DialogFragment {

        private int id;
        private int clickedPosition;

        PreferencesAddCategoryDialog(int id, int clickedPosition){
            this.id = id;
            this.clickedPosition = clickedPosition;
        }
        PreferencesAddCategoryDialog(int id){
            this.id = id;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.category_add_popup, container, false);

            TextView title = (TextView) v.findViewById(R.id.CategoryAddPopup_title);
            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.CategoryAddPopup_root);
            final EditText editTitle = (EditText) v.findViewById(R.id.CategoryAddPopup_et);

            final Button accept = (Button) v.findViewById(R.id.CategoryAddPopup_accept);
            final Button cancel = (Button) v.findViewById(R.id.CategoryAddPopup_cancel);


            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
            accept.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);
            cancel.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);
            editTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeEditText);

            accept.setTextColor(AppPreferences.colorTheme_FontColor);
            cancel.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextColor(AppPreferences.colorTheme_FontColor);
            editTitle.setTextColor(AppPreferences.colorTheme_FontColor);

            editTitle.setHintTextColor(AppPreferences.colorTheme_HintColor);
            editTitle.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
            editTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            });
            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            editTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    editTitle.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            switch (id){
                case IntConst.EDIT: {
                    title.setText(R.string.pref_popup_title_edit_category);
                    accept.setText(R.string.change);

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getDialog().dismiss();
                        }
                    });
                    accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String name;
                            if((name = editTitle.getText().toString().trim()).equalsIgnoreCase("")){
                                return;
                            }
                            Utils.goodsCategories.get(clickedPosition).setName(name);

                            Bundle b = new Bundle();
                            b.putInt("clickedPosition", clickedPosition);
                            startLoader(IntConst.LOADER_UPDATE_CATEGORY_NAME, b);

                            getDialog().dismiss();
                        }
                    });
                }
                break;
                case IntConst.CREATE: {
                    title.setText(R.string.pref_popup_title_create_category);
                    accept.setText(R.string.accept);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getDialog().dismiss();
                        }
                    });
                    accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String name;
                            if((name = editTitle.getText().toString().trim()).equalsIgnoreCase("")){
                                editTitle.setError(getActivity().getString(R.string.create_buy_list_title_error));
                                return;
                            }
                            GoodsCategory gc = new GoodsCategory();
                            gc.setName(name);
                            if(Utils.goodsCategories.size()>1) {
                                gc.setSort(Utils.goodsCategories.get(Utils.goodsCategories.size() - 2).getSort() + 1);
                            } else {
                                gc.setSort(0);
                            }
                            Utils.goodsCategories.add(Utils.goodsCategories.size()-1, gc);
                            startLoader(IntConst.LOADER_ADD_CATEGORY, null);
                            getDialog().dismiss();
                        }
                    });
                }
                break;
            }

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }

}
