package ru.fromtheseventhsky.buylist.fragments;


import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Random;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.adapters.ColorGridAdapter;
import ru.fromtheseventhsky.buylist.custom.views.ColorCircle;
import ru.fromtheseventhsky.buylist.custom.views.ColorPickerDialog;
import ru.fromtheseventhsky.buylist.custom.views.TwoColorsCircle;
import ru.fromtheseventhsky.buylist.loaders.LoaderCreateBuyListItem;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyList;
import ru.fromtheseventhsky.buylist.utils.ColorCircleContainer;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.TwiceColorCircleContainer;
import ru.fromtheseventhsky.buylist.utils.Utils;

public class FragmentCreateBuyList  extends Fragment implements
        ColorPickerDialog.OnColorChangedListener,
        LoaderManager.LoaderCallbacks<Long>{


    private CreateBuyListInterface mListener;

    private EditText nameET;
    private EditText commentET;
    private EditText itemDurationET;
    private CheckBox itemDurEnabledChB;

    private int groupPos;
    private int childPos;
    private boolean editMode;

    private TextView nameTV;
    private TextView commentTV;
    private TextView itemDurationTV;

    private View blDivider;
    private View blDivider2;

    private RelativeLayout blRoot;


    private Button openColorThemeDialog;
    private Button randomColorTheme;

    private ColorGridAdapter adapterColorGrid;

    LoaderManager.LoaderCallbacks<Long> callback;

    private Button accept;
    private Button preview;

    private ColorPickerDialog picker;

    private ObjectAnimator animateET;
    private ObjectAnimator animateChB;

    private long durationOnStartEdit;
    private long durationOnFinishEdit;


    private int bckColor;
    private int txtColor;


    BuyList finalItem;

    float startX;


    public static FragmentCreateBuyList newInstance() {
        FragmentCreateBuyList fragment = new FragmentCreateBuyList();
        return fragment;
    }

    public FragmentCreateBuyList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    //TODO applyTheme
    public void applyTheme(){
        accept.setBackgroundDrawable(Utils.getStateList());
        preview.setBackgroundDrawable(Utils.getStateList());
        openColorThemeDialog.setBackgroundDrawable(Utils.getStateList());
        randomColorTheme.setBackgroundDrawable(Utils.getStateList());

        nameET.setTextColor(AppPreferences.colorTheme_FontColor);
        nameET.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
        nameET.setHintTextColor(AppPreferences.colorTheme_HintColor);
        commentET.setTextColor(AppPreferences.colorTheme_FontColor);
        commentET.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
        commentET.setHintTextColor(AppPreferences.colorTheme_HintColor);
        itemDurationET.setTextColor(AppPreferences.colorTheme_FontColor);
        itemDurationET.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
        itemDurationET.setHintTextColor(AppPreferences.colorTheme_HintColor);
        itemDurEnabledChB.setTextColor(AppPreferences.colorTheme_FontColor);

        blDivider.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);
        blDivider2.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);

        accept.setTextColor(AppPreferences.colorTheme_FontColor);
        preview.setTextColor(AppPreferences.colorTheme_FontColor);
        openColorThemeDialog.setTextColor(AppPreferences.colorTheme_FontColor);
        randomColorTheme.setTextColor(AppPreferences.colorTheme_FontColor);

        nameTV.setTextColor(AppPreferences.colorTheme_FontColor);
        commentTV.setTextColor(AppPreferences.colorTheme_FontColor);
        itemDurationTV.setTextColor(AppPreferences.colorTheme_FontColor);

        blRoot.setBackgroundColor(AppPreferences.colorTheme_Background);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_create_buy_list, container, false);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final DisplayMetrics metric = new DisplayMetrics();
        display.getMetrics(metric);

        callback = this;

        if(finalItem==null) {
            finalItem = new BuyList();
        }

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        picker = new ColorPickerDialog(getActivity(), this, Color.RED, metrics.heightPixels, metrics.widthPixels);

        bckColor = Utils.orig.get(AppPreferences.defaultBckColor).getColor();
        txtColor = Utils.orig.get(AppPreferences.defaultTextColor).getColor();


        nameET = (EditText) v.findViewById(R.id.BL_name);
        commentET = (EditText) v.findViewById(R.id.BL_comment);
        itemDurationET = (EditText) v.findViewById(R.id.BL_actuality);
        itemDurEnabledChB = (CheckBox) v.findViewById(R.id.BL_chb);



        nameTV = (TextView) v.findViewById(R.id.BL_name_tv);
        commentTV = (TextView) v.findViewById(R.id.BL_comment_tv);
        itemDurationTV = (TextView) v.findViewById(R.id.BL_actuality_tv);

        blDivider = v.findViewById(R.id.BL_divider);
        blDivider2 = v.findViewById(R.id.CommodityLay_nothing);

        blRoot = (RelativeLayout) v.findViewById(R.id.BL_root);

        nameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                nameET.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        accept = (Button) v.findViewById(R.id.BL_accept);
        preview = (Button) v.findViewById(R.id.CommodityLay_cancel);




        openColorThemeDialog = (Button) v.findViewById(R.id.Create_BLI_open_color_dialog);
        randomColorTheme = (Button) v.findViewById(R.id.BL_random_color);


        applyTheme();

        final Random random = new Random(System.currentTimeMillis());


        randomColorTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(v);
                if(Utils.customTemp.size()==0){
                    if(Utils.origTemp.size()==0){
                        Toast.makeText(getActivity(), R.string.no_color_themes_yet, Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                int color = random.nextInt(Utils.customTemp.size() + Utils.origTemp.size());
                adapterColorGrid.setSelectedPositionTemplate(color);
                bckColor = adapterColorGrid.getSelectedColorTemplate(true);
                txtColor = adapterColorGrid.getSelectedColorTemplate(false);
                adapterColorGrid.setSelectedPositionBoth(getContainColor(bckColor, true), getContainColor(txtColor, false), adapterColorGrid.getSelectedPositionTemp());
            }
        });

        openColorThemeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(v);
                new ColorSelectDialog().show(getFragmentManager(), "d");
            }
        });


        animateET = ObjectAnimator.ofFloat(itemDurationET, "scaleX", 0, 0);
        animateChB = ObjectAnimator.ofFloat(itemDurEnabledChB, "x", 0, 0);


        int dividerId = picker.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
        View divider = picker.findViewById(dividerId);
        divider.setBackgroundColor(AppPreferences.colorTheme_Actionbar); // change divider color


        startX = -10;

        itemDurEnabledChB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.buyListCreateStarted = true;
                if (itemDurationET.isEnabled()) {

                    itemDurationET.setEnabled(false);

                    if (startX == -10)
                        startX = itemDurEnabledChB.getX();
                    if (animateChB.isStarted())
                        animateChB.cancel();
                    animateChB.setFloatValues(itemDurEnabledChB.getX(), itemDurationET.getX());
                    animateChB.setDuration((long) 300);
                    animateChB.start();

                    if (animateET.isStarted())
                        animateET.cancel();
                    animateET.setFloatValues(itemDurationET.getScaleX(), 0);
                    animateET.setDuration((long) 300);
                    animateET.start();
                } else {
                    itemDurationET.setEnabled(true);

                    if (animateChB.isStarted())
                        animateChB.cancel();
                    animateChB.setFloatValues(itemDurEnabledChB.getX(), startX);
                    animateChB.setDuration((long) 300);
                    animateChB.start();


                    if (animateET.isStarted())
                        animateET.cancel();
                    animateET.setFloatValues(itemDurationET.getScaleX(), 1);
                    animateET.setDuration((long) (300));
                    animateET.start();
                }
            }
        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                if(Utils.buyListEditStarted){
                    new MyAlertDialog(IntConst.BUY_LIST_CHANGE).show(getFragmentManager(), "BUY_LIST_CHANGE");
                } else {
                    if (nameET.getText().toString().trim().equalsIgnoreCase("")) {
                        nameET.setError(getResources().getText(R.string.create_buy_list_title_error));
                        nameET.requestFocus();
                        return;
                    }
                    new MyAlertDialog(IntConst.BUY_LIST_ADD).show(getFragmentManager(), "BUY_LIST_ADD");
                }
            }
        });



        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PreviewPopup().show(getFragmentManager(),"");
            }
        });





        adapterColorGrid = new ColorGridAdapter(getActivity());


        if(editMode){
            editMode = false;

            nameET.setText(finalItem.name);
            nameET.setHint(finalItem.name);
            commentET.setText(finalItem.comment);
            commentET.setHint(finalItem.comment);
            int day;

            nameET.setSelection(nameET.getText().length());

            durationOnStartEdit = finalItem.dateEnd;

            if(finalItem.dateEnd==-1){
                itemDurEnabledChB.setChecked(true);
                itemDurationET.setText("");
                itemDurationET.setEnabled(false);

                if (startX == -10)
                    startX = itemDurEnabledChB.getX();
                if (animateChB.isStarted())
                    animateChB.cancel();
                animateChB.setFloatValues(itemDurEnabledChB.getX(), itemDurationET.getX());
                animateChB.setDuration((long) 10);
                animateChB.start();

                if (animateET.isStarted())
                    animateET.cancel();
                animateET.setFloatValues(itemDurationET.getScaleX(), 0);
                animateET.setDuration((long) 10);
                animateET.start();

            } else {
                day = (int) ((finalItem.dateEnd - finalItem.date)/86400000);
                itemDurationET.setText(String.valueOf(day));
                itemDurationET.setHint(String.valueOf(day));
                itemDurEnabledChB.setChecked(false);
            }

            if(finalItem.type==2){
                itemDurationET.setVisibility(View.GONE);
                itemDurEnabledChB.setVisibility(View.GONE);
                itemDurationTV.setVisibility(View.GONE);
            }
            adapterColorGrid.setSelectedPositionBoth(getContainColor(finalItem.colorBck, true), getContainColor(finalItem.colorTxt, false), getContainColorTemplate(finalItem.colorBck, finalItem.colorTxt));
            accept.setText(R.string.change);

            bckColor = finalItem.colorBck;
            txtColor = finalItem.colorTxt;

            Utils.buyListCreateStarted = false;

        }


        return v;
    }

    public void changeTab(BuyList rowId, boolean add, boolean old, int groupPos, int childPos) {
        if (mListener != null) {
            mListener.changeTab(rowId, add, old, groupPos, childPos);
        }
    }

    public void hideSoftKeyboard(View view){
        InputMethodManager imm =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public void showSoftKeyboard(){
        InputMethodManager imm =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        nameET.requestFocus();
        imm.showSoftInput(nameET, 0);

    }

    //TODO edit item
    public void editItem(BuyList item, int groupPos, int childPos){

        this.groupPos = groupPos;
        this.childPos = childPos;
        editMode = true;

        finalItem = item;
    }


    //TODO clearAll
    public void clearAll() {
        nameET.setError(null);
        nameET.setText("");
        commentET.setText("");
        itemDurationET.setText("");

        nameET.setHint(R.string.create_buy_list_name_hint);
        commentET.setHint(R.string.create_buy_list_comment_hint);
        itemDurationET.setHint("7");

        itemDurationET.setVisibility(View.VISIBLE);
        itemDurEnabledChB.setVisibility(View.VISIBLE);
        itemDurationTV.setVisibility(View.VISIBLE);


        bckColor = Utils.orig.get(AppPreferences.defaultBckColor).getColor();
        txtColor = Utils.orig.get(AppPreferences.defaultTextColor).getColor();

        if(itemDurEnabledChB.isChecked()) {
            if(animateET.isStarted())
                animateET.cancel();
            animateET.setFloatValues(itemDurationET.getScaleX(), 1);
            animateET.setDuration(10);
            animateET.start();
            itemDurEnabledChB.setX(startX);
        }

        accept.setText(R.string.title_add);

        itemDurEnabledChB.setChecked(false);

        adapterColorGrid.reset();
        Utils.buyListCreateStarted = false;
    };



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CreateBuyListInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    //TODO colorChanged
    @Override
    public void colorChanged(int color, boolean isBckAdapter) {
        ColorCircleContainer c = new ColorCircleContainer(false, color, false);
        if(!containColor(color, isBckAdapter)) {
            Bundle b = new Bundle();
            b.putInt("1", color);
            //b.putInt("3", color);
            if(isBckAdapter) {
                Utils.customBck.add(c);
                adapterColorGrid.selectLastItem();
                adapterColorGrid.notifyDataSetChanged();
                b.putInt("2", 1);
                getLoaderManager().initLoader(IntConst.LOADER_CREATE_COLOR, b, callback).forceLoad();
            } else {
                Utils.customTxt.add(c);
                adapterColorGrid.selectLastItem();
                adapterColorGrid.notifyDataSetChanged();
                b.putInt("2", 2);
                getLoaderManager().initLoader(IntConst.LOADER_CREATE_COLOR, b, callback).forceLoad();
            }

        } else {
            Toast.makeText(getActivity(),getText(R.string.color_popup_already),Toast.LENGTH_LONG).show();
        }
    }

    //TODO startLoader method
    public void startLoader(int id, Bundle b){
        getLoaderManager().initLoader(id, b, callback).forceLoad();
    }

    //TODO onCreateLoader
    @Override
    public Loader<Long> onCreateLoader(int id, Bundle args) {
        Utils.loaderStarted = true;
        LoaderCreateBuyListItem loader = null;
        switch(id){
            case IntConst.LOADER_CREATE_BL:
                loader = new LoaderCreateBuyListItem(getActivity(),
                        0,
                        args.getString("name"),
                        args.getString("comment"),
                        args.getLong("date"),
                        args.getInt("colorBck"),
                        args.getInt("colorTxt"),
                        args.getInt("duration"),
                        IntConst.LOADER_CREATE_BL);
                break;
            case IntConst.LOADER_UPDATE_BL:
                loader = new LoaderCreateBuyListItem(getActivity(),
                        args.getLong("id"),
                        args.getString("name"),
                        args.getString("comment"),
                        args.getLong("date"),
                        args.getInt("colorBck"),
                        args.getInt("colorTxt"),
                        args.getInt("duration"),
                        IntConst.LOADER_UPDATE_BL);
                break;
            case IntConst.LOADER_CREATE_COLOR:
                loader = new LoaderCreateBuyListItem(getActivity(),args.getInt("1"), args.getInt("3"), args.getInt("2"), IntConst.LOADER_CREATE_COLOR);
                break;
            case IntConst.LOADER_DELETE_COLOR:
                loader = new LoaderCreateBuyListItem(getActivity(), args.getInt("1"), args.getInt("3"), args.getInt("2"), IntConst.LOADER_DELETE_COLOR);
                break;
        }
        return loader;
    }


    //TODO onLoadFinished
    @Override
    public void onLoadFinished(Loader<Long> loader, Long rowId) {
        switch(loader.getId()){
            case IntConst.LOADER_CREATE_BL: {
                clearAll();
                finalItem.id = rowId.intValue();
                BuyList bl = new BuyList(finalItem);
                changeTab(bl, true, false, 0, 0);
                getLoaderManager().destroyLoader(IntConst.LOADER_CREATE_BL);
                break;
            }
            case IntConst.LOADER_UPDATE_BL: {
                Utils.buyListEditStarted = false;
                clearAll();
                BuyList bl = new BuyList(finalItem);

                boolean hasChange;

                if(durationOnFinishEdit == durationOnStartEdit) {
                    hasChange = false;
                } else {
                    hasChange = true;
                }

                changeTab(bl, false, hasChange, groupPos, childPos);
                getLoaderManager().destroyLoader(IntConst.LOADER_UPDATE_BL);
                break;
            }
            case IntConst.LOADER_GET_COLOR:
                adapterColorGrid.notifyDataSetChanged();
                getLoaderManager().destroyLoader(IntConst.LOADER_GET_COLOR);
                break;
            case IntConst.LOADER_CREATE_COLOR:
                getLoaderManager().destroyLoader(IntConst.LOADER_CREATE_COLOR);
                break;
            case IntConst.LOADER_DELETE_COLOR:
                adapterColorGrid.notifyDataSetChanged();
                getLoaderManager().destroyLoader(IntConst.LOADER_DELETE_COLOR);
                break;
        }

        Utils.loaderStarted = false;
    }

    @Override
    public void onLoaderReset(Loader<Long> loader) {
        Utils.loaderStarted = false;
    }


    public interface CreateBuyListInterface {
        public void changeTab(BuyList rowId, boolean add, boolean old, int groupPos, int childPos);
    }


    //TODO PreviewPopup
    public class PreviewPopup extends DialogFragment {
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.item_of_list_of_buy_list, null);

            getDialog().requestWindowFeature(STYLE_NO_TITLE);

            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.list_of_buy_list_item_root);
            TextView name = (TextView) v.findViewById(R.id.list_of_buy_list_item_name);
            TextView comment = (TextView) v.findViewById(R.id.list_of_buy_list_item_comment);
            TextView positions = (TextView) v.findViewById(R.id.list_of_buy_list_item_positions);

            String nameTmp = nameET.getText().toString().trim();
            if(nameTmp.equalsIgnoreCase(""))
                nameTmp = getResources().getString(R.string.create_buy_list_name_title);

            String commentTmp =  commentET.getText().toString().trim();
            if(commentTmp.equalsIgnoreCase("")){
                comment.setVisibility(View.GONE);
            } else {
                comment.setText(commentTmp);
            }
            name.setText(nameTmp);

            String strPositions = getActivity().getString(R.string.positions);
            String blCost = getActivity().getString(R.string.positions);

            if(AppPreferences.costAvailable) {
                positions.setText(strPositions
                        .concat("0/0")
                        .concat(" ")
                        .concat(blCost)
                        .concat(" ")
                        .concat(String.valueOf(0)));
            } else {
                positions.setText(strPositions
                        .concat("0/0"));
            }

            root.setBackgroundColor(bckColor);

            name.setTextColor(txtColor);
            comment.setTextColor(txtColor);
            positions.setTextColor(txtColor);


            return v;
        }
    }

    private boolean containColor(int color, boolean isBck){
        for(int i = 0; Utils.orig.size() > i; i++)
            if(Utils.orig.get(i).getColor() == color)
                return true;
        if(isBck) {
            for (int i = 0; Utils.customBck.size() > i; i++)
                if (Utils.customBck.get(i).getColor() == color)
                    return true;
        } else {
            for (int i = 0; Utils.customTxt.size() > i; i++)
                if (Utils.customTxt.get(i).getColor() == color)
                    return true;
        }

        return false;
    }

    private int getContainColor(int color, boolean bck) {
        for (int i = 0; Utils.orig.size() > i; i++)
            if (Utils.orig.get(i).getColor() == color)
                return i;
        if (bck) {
            for (int i = 0; Utils.customBck.size() > i; i++)
                if (Utils.customBck.get(i).getColor() == color)
                    return i+Utils.orig.size();
        } else {
            for (int i = 0; Utils.customTxt.size() > i; i++)
                if (Utils.customTxt.get(i).getColor() == color)
                    return i+Utils.orig.size();

        }
        return -1;
    }

    private boolean isContainColorTemplate(int firstColor, int secondColor) {
        for (int i = 0; Utils.origTemp.size() > i; i++) {
            if (Utils.origTemp.get(i).getFirstColor() == firstColor && Utils.origTemp.get(i).getSecondColor() == secondColor) {
                return true;
            }
        }
        for (int i = 0; Utils.customTemp.size() > i; i++) {
            if (Utils.customTemp.get(i).getFirstColor() == firstColor && Utils.customTemp.get(i).getSecondColor() == secondColor) {
                return true;
            }
        }
        return false;
    }
    private int getContainColorTemplate(int firstColor, int secondColor) {
        for (int i = 0; Utils.origTemp.size() > i; i++)
            if (Utils.origTemp.get(i).getFirstColor() == firstColor && Utils.origTemp.get(i).getSecondColor() == secondColor) {
                return i;
            }

            for (int i = 0; Utils.customTemp.size() > i; i++) {
                if (Utils.customTemp.get(i).getFirstColor() == firstColor && Utils.customTemp.get(i).getSecondColor() == secondColor) {
                    return i + Utils.origTemp.size();
                }
            }
            return -1;

    }


    //TODO MyAlertDialog
    public class MyAlertDialog extends DialogFragment {

        private int id;
        private int position;
        private int color;
        private int secondColor;
        private boolean isBckAdapter;
        private boolean isTemplate;

        public MyAlertDialog(){}
        public MyAlertDialog(int id){
            this.id = id;
        }
        public MyAlertDialog(int id, int color, int position, boolean isBckAdapter, boolean isTemplate){
            this.id = id;
            this.color = color;
            this.position = position;
            this.isBckAdapter = isBckAdapter;
            this.isTemplate = isTemplate;
        }
        public MyAlertDialog(int id, int color, int secondColor, int position, boolean isBckAdapter, boolean isTemplate){
            this.id = id;
            this.color = color;
            this.secondColor = secondColor;
            this.position = position;
            this.isBckAdapter = isBckAdapter;
            this.isTemplate = isTemplate;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.alert_dialog, container, false);


            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.alertDialogRoot);
            TextView title = (TextView) v.findViewById(R.id.alertDialogTitle);
            Button accept = (Button) v.findViewById(R.id.alertDialogAccept);
            final Button cancel = (Button) v.findViewById(R.id.alertDialogCancel);

            root.setBackgroundColor(AppPreferences.colorTheme_Background);


            switch(id){
                case IntConst.BUY_LIST_ADD:
                    title.setText(R.string.create_buy_list_add);
                    break;
                case IntConst.BUY_LIST_DELETE_COLOR:
                    title.setText(R.string.color_delete);
                    break;
                case IntConst.BUY_LIST_CHANGE:
                    title.setText(R.string.alert_dialog_change_save);
                    break;
            }

            title.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefTitle);

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    switch(id){
                        //todo BUY_LIST_ADD: {
                        case IntConst.BUY_LIST_ADD: {
                            String name = nameET.getText().toString().trim();
                            String comment = commentET.getText().toString().trim();
                            String durationString = itemDurationET.getText().toString().trim();
                            int duration;


                            if (!itemDurEnabledChB.isChecked()) {
                                if (durationString.equalsIgnoreCase("")) {
                                    duration = 7;
                                } else {
                                    duration = Integer.parseInt(durationString);
                                }
                            } else {
                                duration = -1;
                            }
                            long date = Calendar.getInstance().getTimeInMillis();

                            finalItem.name = name;
                            finalItem.comment = comment;
                            finalItem.date = date;
                            if (duration != -1) {
                                finalItem.dateEnd = date + duration * 86400000;
                            } else {
                                finalItem.dateEnd = -1;
                            }
                            finalItem.colorBck = bckColor;
                            finalItem.colorTxt = txtColor;
                            finalItem.sync = false;
                            finalItem.type = 1;

                            b.putString("name", name);
                            b.putString("comment", comment);
                            b.putLong("date", date);
                            b.putInt("colorBck", finalItem.colorBck);
                            b.putInt("colorTxt", finalItem.colorTxt);
                            b.putInt("duration", duration);

                            if(!isContainColorTemplate(bckColor, txtColor)){
                                Utils.customTemp.add(new TwiceColorCircleContainer(bckColor, txtColor, false));

                                Bundle bndl = new Bundle();
                                bndl.putInt("1", bckColor);
                                bndl.putInt("3", txtColor);
                                bndl.putInt("2", 3);
                                startLoader(IntConst.LOADER_CREATE_COLOR, bndl);
                                adapterColorGrid.notifyDataSetChanged();
                            }

                            startLoader(IntConst.LOADER_CREATE_BL, b);
                            //getLoaderManager().initLoader(IntConst.LOADER_CREATE_BL, b, callback).forceLoad();
                            break;
                        }
                        //todo BUY_LIST_DELETE_COLOR: {
                        case IntConst.BUY_LIST_DELETE_COLOR: {
                            b.putInt("1", color);


                            if (!isTemplate) {
                                b.putInt("3", -1);
                                if (isBckAdapter) {
                                    Utils.customBck.remove(position - Utils.orig.size());
                                    adapterColorGrid.removeColor(position, true, false);
                                    b.putInt("2", 1);
                                } else {
                                    Utils.customTxt.remove(position - Utils.orig.size());
                                    adapterColorGrid.removeColor(position, false, false);
                                    b.putInt("2", 2);
                                }
                            } else {
                                Utils.customTemp.remove(position - Utils.origTemp.size());
                                adapterColorGrid.removeColor(position, false, true);
                                b.putInt("2", 3);
                                b.putInt("3", secondColor);
                            }
                            //getLoaderManager().initLoader(IntConst.LOADER_DELETE_COLOR, b, callback).forceLoad();
                            startLoader(IntConst.LOADER_DELETE_COLOR, b);
                            break;
                        }

                        //todo BUY_LIST_CHANGE: {
                        case IntConst.BUY_LIST_CHANGE: {
                            if(!isContainColorTemplate(bckColor, txtColor)){
                                Utils.customTemp.add(new TwiceColorCircleContainer(bckColor, txtColor, false));

                                Bundle bndl = new Bundle();
                                bndl.putInt("1", bckColor);
                                bndl.putInt("3", txtColor);
                                bndl.putInt("2", 3);
                                startLoader(IntConst.LOADER_CREATE_COLOR, bndl);
                                adapterColorGrid.notifyDataSetChanged();
                            }

                            String name = nameET.getText().toString().trim();
                            String comment = commentET.getText().toString().trim();
                            String durationString = itemDurationET.getText().toString().trim();
                            int duration;

                            if (!name.equalsIgnoreCase("")) {
                                finalItem.name = name;
                            }

                            finalItem.comment = comment;


                            if (!itemDurEnabledChB.isChecked()) {
                                if (durationString.equalsIgnoreCase("")) {
                                    duration = 7;
                                } else {
                                    duration = Integer.parseInt(durationString);
                                }
                            } else {
                                duration = -1;
                            }

                            if (duration != -1) {
                                finalItem.dateEnd = finalItem.date + duration * 86400000;
                            } else {
                                finalItem.dateEnd = -1;
                            }
                            finalItem.colorBck = bckColor;
                            finalItem.colorTxt = txtColor;
                            finalItem.sync = false;

                            if(finalItem.dateEnd!=-1){
                                if(finalItem.dateEnd < Calendar.getInstance().getTimeInMillis())
                                    finalItem.type = 2;
                            }

                            durationOnFinishEdit = finalItem.dateEnd;

                            b.putLong("id", finalItem.id);
                            b.putString("name", name);
                            b.putString("comment", comment);
                            b.putLong("date", finalItem.date);
                            b.putInt("colorBck", finalItem.colorBck);
                            b.putInt("colorTxt", finalItem.colorTxt);
                            b.putInt("duration", duration);

                            //getLoaderManager().initLoader(IntConst.LOADER_UPDATE_BL, b, callback).forceLoad();
                            startLoader(IntConst.LOADER_UPDATE_BL, b);


                            break;
                        }
                    }
                    getDialog().dismiss();
                }
            });

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }


    public class ColorSelectDialog extends DialogFragment {

        private Button selectBckColor;
        private Button selectTxtColor;
        private Button selectColorPattern;

        private Button cancelBtn;
        private Button acceptBtn;

        private TextView divider_1;
        private TextView divider_2;
        private TextView divider_3;

        private RelativeLayout mainRoot;

        private RelativeLayout itemRoot;
        private TextView itemName;
        private TextView itemComment;
        private TextView itemPositions;


        private GridView gridView;

        private boolean isBckAdapter;
        private boolean isTemplate;

        private int backup_colorBck;
        private int backup_colorTxt;
        private int backup_posTxt;
        private int backup_posBck;
        private int backup_posTemp;

        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {

            // the content
            final RelativeLayout root = new RelativeLayout(getActivity());
            root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            // creating the fullscreen dialog
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(root);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return dialog;
        }

        protected void applyTxtColor(int color){
            itemName.setTextColor(color);
            itemComment.setTextColor(color);
            itemPositions.setTextColor(color);
        }
        protected void applyBckColor(int color){
            itemRoot.setBackgroundColor(color);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            //getDialog().requestWindowFeature(STYLE_NO_TITLE);
            View v = inflater.inflate(R.layout.color_theme_select_popup, container, false);

            mainRoot = (RelativeLayout) v.findViewById(R.id.CTP_color_theme_root);

            itemRoot = (RelativeLayout) v.findViewById(R.id.CTP_main_root);
            itemName = (TextView) v.findViewById(R.id.CTP_name);
            itemComment = (TextView) v.findViewById(R.id.CTP_comment);
            itemPositions = (TextView) v.findViewById(R.id.CTP_additional);


            String nameTmp = nameET.getText().toString().trim();
            if(nameTmp.equalsIgnoreCase(""))
                nameTmp = getResources().getString(R.string.create_buy_list_name_title);

            String commentTmp =  commentET.getText().toString().trim();
            if(commentTmp.equalsIgnoreCase("")){
                itemComment.setVisibility(View.GONE);
            } else {
                itemComment.setText(commentTmp);
            }

            itemName.setText(nameTmp);
            itemComment.setText(commentTmp);

            String strPositions = getActivity().getString(R.string.positions);
            String blCost = getActivity().getString(R.string.positions);

            if(AppPreferences.costAvailable) {
                itemPositions.setText(strPositions
                        .concat("0/0")
                        .concat(" ")
                        .concat(blCost)
                        .concat(" ")
                        .concat(String.valueOf(0)));
            } else {
                itemPositions.setText(strPositions
                        .concat("0/0"));
            }



            selectBckColor = (Button) v.findViewById(R.id.CTP_color_bck);
            selectTxtColor = (Button) v.findViewById(R.id.CTP_colorTxt);
            selectColorPattern = (Button) v.findViewById(R.id.CTP_color_patterns);

            cancelBtn = (Button) v.findViewById(R.id.CTP_cancel);
            acceptBtn = (Button) v.findViewById(R.id.CTP_accept);

            backup_colorBck = bckColor;
            backup_colorTxt = txtColor;
            backup_posTxt = adapterColorGrid.getSelectedPositionTxt();
            backup_posBck = adapterColorGrid.getSelectedPositionBck();
            backup_posTemp = adapterColorGrid.getSelectedPositionTemp();

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bckColor = backup_colorBck;
                    txtColor = backup_colorTxt;
                    adapterColorGrid.setSelectedPositionBoth(backup_posBck, backup_posTxt, backup_posTemp);
                    dismiss();
                }
            });

            acceptBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            selectBckColor.setActivated(true);
            selectTxtColor.setActivated(false);
            selectColorPattern.setActivated(false);


            cancelBtn.setBackgroundDrawable(Utils.getStateList());
            acceptBtn.setBackgroundDrawable(Utils.getStateList());

            selectBckColor.setBackgroundDrawable(Utils.getStateList());
            selectTxtColor.setBackgroundDrawable(Utils.getStateList());
            selectColorPattern.setBackgroundDrawable(Utils.getStateList());

            mainRoot.setBackgroundColor(AppPreferences.colorTheme_Background);

            divider_1 = (TextView) v.findViewById(R.id.CTP_divider_first);
            divider_2 = (TextView) v.findViewById(R.id.CTP_divider_second);
            divider_3 = (TextView) v.findViewById(R.id.CTP_divider_bp);

            divider_1.setBackgroundColor(AppPreferences.colorTheme_Actionbar);
            divider_2.setBackgroundColor(AppPreferences.colorTheme_Actionbar);
            divider_3.setBackgroundColor(AppPreferences.colorTheme_Actionbar);

            gridView = (GridView) v.findViewById(R.id.CTP_colorGrid);

            selectBckColor.setTextColor(AppPreferences.colorTheme_FontColor);
            selectTxtColor.setTextColor(AppPreferences.colorTheme_FontColor);
            selectColorPattern.setTextColor(AppPreferences.colorTheme_FontColor);
            cancelBtn.setTextColor(AppPreferences.colorTheme_FontColor);
            acceptBtn.setTextColor(AppPreferences.colorTheme_FontColor);

            isBckAdapter = true;
            isTemplate = false;

            applyBckColor(bckColor);
            applyTxtColor(txtColor);

            selectBckColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isBckAdapter) {
                        selectBckColor.setActivated(true);
                        selectTxtColor.setActivated(false);
                        selectColorPattern.setActivated(false);
                        isBckAdapter = true;
                        isTemplate = false;
                        adapterColorGrid.setBck(isBckAdapter);
                        adapterColorGrid.setTemplate(isTemplate);
                        adapterColorGrid.notifyDataSetChanged();
                    }
                }
            });
            selectTxtColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isBckAdapter || isTemplate) {
                        selectBckColor.setActivated(false);
                        selectTxtColor.setActivated(true);
                        selectColorPattern.setActivated(false);
                        isBckAdapter = false;
                        isTemplate = false;
                        adapterColorGrid.setBck(isBckAdapter);
                        adapterColorGrid.setTemplate(isTemplate);
                        adapterColorGrid.notifyDataSetChanged();
                    }
                }
            });

            selectColorPattern.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isTemplate) {
                        selectBckColor.setActivated(false);
                        selectTxtColor.setActivated(false);
                        selectColorPattern.setActivated(true);
                        isBckAdapter = false;
                        isTemplate = true;
                        adapterColorGrid.setBck(isBckAdapter);
                        adapterColorGrid.setTemplate(isTemplate);
                        adapterColorGrid.notifyDataSetChanged();
                    }
                }
            });


            Display display = getActivity().getWindowManager().getDefaultDisplay();
            final DisplayMetrics metric = new DisplayMetrics();
            display.getMetrics(metric);

            gridView.setAdapter(adapterColorGrid);
            gridView.setNumColumns(GridView.AUTO_FIT);
            gridView.setColumnWidth((int) (48*metric.density));
            gridView.setVerticalSpacing(5);
            gridView.setHorizontalSpacing(5);
            gridView.setSelector(R.drawable.selector_circle);

            if(isContainColorTemplate(bckColor, txtColor)){
                adapterColorGrid.setSelectedPositionTemplate(getContainColorTemplate(bckColor, txtColor));
            } else {
                adapterColorGrid.setSelectedPositionTemplate(-1);
            }

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if(!isTemplate) {
                        if (!((ColorCircle) view).isVisibleAdd()) {
                            ((ColorCircle) view).setSelectedCC(true);
                            adapterColorGrid.setSelectedPosition(position);
                            if(isBckAdapter){
                                bckColor = adapterColorGrid.getSelectedColor(isBckAdapter);
                                applyBckColor(bckColor);
                            } else {
                                txtColor = adapterColorGrid.getSelectedColor(isBckAdapter);
                                applyTxtColor(txtColor);
                            }
                            if(isContainColorTemplate(bckColor, txtColor)){
                                adapterColorGrid.setSelectedPositionTemplate(getContainColorTemplate(bckColor, txtColor));
                            } else {
                                adapterColorGrid.setSelectedPositionTemplate(-1);
                            }
                            gridView.invalidateViews();

                        } else {
                            picker.show();
                            picker.setBck(isBckAdapter, bckColor, txtColor);
                        }
                    } else {
                        ((TwoColorsCircle) view).setSelectedCC(true);
                        adapterColorGrid.setSelectedPosition(position);
                        bckColor = adapterColorGrid.getSelectedColorTemplate(true);
                        txtColor = adapterColorGrid.getSelectedColorTemplate(false);
                        applyBckColor(bckColor);
                        applyTxtColor(txtColor);

                        if(containColor(bckColor, true)){
                            adapterColorGrid.setSelectedPositionBck(getContainColor(bckColor, true), true);
                        } else {
                            adapterColorGrid.setSelectedPositionBck(-1, true);
                        }

                        if(isContainColorTemplate(bckColor, txtColor)){
                            adapterColorGrid.setSelectedPositionBck(getContainColor(txtColor, false), false);
                        } else {
                            adapterColorGrid.setSelectedPositionBck(-1, false);
                        }

                        adapterColorGrid.setSelectedPosition(position);
                        gridView.invalidateViews();
                    }
                }
            });
            adapterColorGrid.reset();

            gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    if (!isTemplate) {
                        if (((ColorCircleContainer) (parent.getItemAtPosition(position))).isOriginal() != true) {
                            final int color = ((ColorCircleContainer) (parent.getItemAtPosition(position))).getColor();
                            new MyAlertDialog(IntConst.BUY_LIST_DELETE_COLOR, color, position, isBckAdapter, isTemplate).show(getFragmentManager(), "BUY_LIST_DELETE_COLOR");
                        }
                    } else {
                        if (((TwiceColorCircleContainer) (parent.getItemAtPosition(position))).isOriginal() != true) {
                            int color1 = ((TwiceColorCircleContainer) (parent.getItemAtPosition(position))).getFirstColor();
                            int color2 = ((TwiceColorCircleContainer) (parent.getItemAtPosition(position))).getSecondColor();
                            new MyAlertDialog(IntConst.BUY_LIST_DELETE_COLOR, color1, color2, position, isBckAdapter, isTemplate).show(getFragmentManager(), "BUY_LIST_DELETE_COLOR");
                        }
                    }

                    return true;
                }
            });

            return v;
        }
    }

}
