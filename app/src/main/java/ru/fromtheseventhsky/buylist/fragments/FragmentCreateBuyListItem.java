package ru.fromtheseventhsky.buylist.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Random;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.adapters.AutoCompleteAdapter;
import ru.fromtheseventhsky.buylist.custom.adapters.ColorGridAdapter;
import ru.fromtheseventhsky.buylist.custom.adapters.SpinnerAdapter;
import ru.fromtheseventhsky.buylist.custom.views.ColorCircle;
import ru.fromtheseventhsky.buylist.custom.views.ColorPickerDialog;
import ru.fromtheseventhsky.buylist.custom.views.CustomSpinner;
import ru.fromtheseventhsky.buylist.custom.views.TwoColorsCircle;
import ru.fromtheseventhsky.buylist.loaders.LoaderCreateBuyListItem;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.BuyListItem;
import ru.fromtheseventhsky.buylist.utils.ColorCircleContainer;
import ru.fromtheseventhsky.buylist.utils.GoodsCategory;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.ProductInfo;
import ru.fromtheseventhsky.buylist.utils.StrConst;
import ru.fromtheseventhsky.buylist.utils.TwiceColorCircleContainer;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class FragmentCreateBuyListItem extends Fragment implements
        ColorPickerDialog.OnColorChangedListener, LoaderManager.LoaderCallbacks<Long>{


    private TextView nameTitle;
    private TextView commentTitle;
    private TextView costTitle;
    private TextView categoryTitle;

    private AutoCompleteTextView name;
    private EditText comment;
    private EditText cost;
    private EditText weight;
    private CustomSpinner category;
    private CustomSpinner weightType;
    private TextView currency;
    private TextView weightTitle;

    private TextView bliDivider;

    private Button btnColorDialogOpen;
    private Button btnRandomColor;

    private Button btnAccept;
    private Button btnPreview;
    private Context context;

    private RelativeLayout categoryRoot;
    private RelativeLayout costRoot;

    private RelativeLayout root;

    private String[] weightTypeTitleArray;
    private String[] weightTypeArray;

    private String tmpPathForEdit;

    private int selectedPositionWhileWork;

    private BuyListItem finalItem;
    private int buyListId;

    private boolean cameraOpened;


    Bitmap finalBmp;

    private boolean editMode;

    private ImageView preview;

    private int bckColor;
    private int txtColor;

    private int tmpCostForEdit;

    private boolean firstStart = true;

    LoaderManager.LoaderCallbacks callback;


    private ColorPickerDialog picker;

    private ColorGridAdapter adapterColorGrid;

    private AutoCompleteAdapter autoCompleteAdapter;
    SpinnerAdapter categoryAdapter;


    private CreateBuyListItemInterface mListener;

    public static FragmentCreateBuyListItem newInstance() {
        FragmentCreateBuyListItem fragment = new FragmentCreateBuyListItem();
        return fragment;
    }

    public void hideSoftKeyboard(View view){
        InputMethodManager imm =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public FragmentCreateBuyListItem() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_create_buy_list_item, container, false);

        bliDivider = (TextView) v.findViewById(R.id.Create_BL_centerView);

        nameTitle = (TextView) v.findViewById(R.id.CommodityLay_titleName);
        commentTitle = (TextView) v.findViewById(R.id.Create_BLI_titleComment);
        costTitle = (TextView) v.findViewById(R.id.CommodityLay_costTitle);
        categoryTitle = (TextView) v.findViewById(R.id.CommodityLay_categoryTitle);
        name = (AutoCompleteTextView) v.findViewById(R.id.CommodityLay_enterName);
        comment = (EditText) v.findViewById(R.id.Create_BLI_enterComment);
        cost = (EditText) v.findViewById(R.id.CommodityLay_enterCost);
        weight = (EditText) v.findViewById(R.id.Create_BLI_enterWeight);
        category = (CustomSpinner) v.findViewById(R.id.CommodityLay_spinnerCategory);
        weightType = (CustomSpinner) v.findViewById(R.id.CommodityLay_spinnerWeight);
        currency = (TextView) v.findViewById(R.id.Create_BLI_tv_currency);
        weightTitle = (TextView) v.findViewById(R.id.CommodityLay_weightTitle);
        btnColorDialogOpen = (Button) v.findViewById(R.id.Create_BLI_open_color_dialog);
        btnRandomColor = (Button) v.findViewById(R.id.Create_BLI_random_color);
        btnAccept = (Button) v.findViewById(R.id.Create_BLI_accept);
        btnPreview = (Button) v.findViewById(R.id.CommodityLay_cancel);

        categoryRoot = (RelativeLayout) v.findViewById(R.id.relativeLayoutCategory);
        costRoot = (RelativeLayout) v.findViewById(R.id.relativeLayoutCost);

        root = (RelativeLayout) v.findViewById(R.id.Create_BLI_root);

        preview = (ImageView) v.findViewById(R.id.CommodityLay_preview);

        context = getActivity();

        setColorTheme();
        acceptPrefs();

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        if(firstStart){
            firstStart = false;
            if(finalItem==null) {
                finalItem = new BuyListItem();
            }

            bckColor = Utils.orig.get(AppPreferences.defaultBckColor).getColor();
            txtColor = Utils.orig.get(AppPreferences.defaultTextColor).getColor();

            weightTypeTitleArray = context.getResources().getStringArray(R.array.weight_type_title);
            weightTypeArray = context.getResources().getStringArray(R.array.weight_type);

            autoCompleteAdapter = new AutoCompleteAdapter(context);

            picker = new ColorPickerDialog(getActivity(), this, Color.RED, metrics.heightPixels, metrics.widthPixels);

        }

        if(AppPreferences.categoryAvailable) {
            category.setBl(this);
        }
        callback = this;

        finalBmp = null;

        cameraOpened = false;

        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(finalBmp == null) {
                    hideSoftKeyboard(v);
                    new CameraDialog().show(getFragmentManager(), "CameraDialog");
                } else {
                    new PreviewImageDialog().show(getFragmentManager(), "PreviewImageDialog");
                }
            }
        });







        final ArrayAdapter<String> weightTypeAdapter = new ArrayAdapter<String>(context, R.layout.spinner_tv, weightTypeArray);
        if(AppPreferences.categoryAvailable) {
            categoryAdapter = new SpinnerAdapter(inflater, context);
        }



        name.setAdapter(autoCompleteAdapter);
        name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProductInfo productInfo = Utils.availableProducts.get(autoCompleteAdapter.getItem(position));
                weightType.setSelection(productInfo.getUnit());
                if(AppPreferences.categoryAvailable) {
                    category.setSelection(productInfo.getCategory());
                }
                if(AppPreferences.costAvailable) {
                    cost.setText(String.valueOf(productInfo.getCost()));
                }
            }
        });

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                name.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        weightType.setAdapter(weightTypeAdapter);

        weightTitle.setText(weightTypeTitleArray[0]);

        weightType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                weightTitle.setText(weightTypeTitleArray[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //УКАЗАНИЕ ИСПОЛЬЗУЕМОЙ ВАЛЮТЫ
        currency.setText(AppPreferences.currency);


        //ПРИСВОЕНИЕ АДЕПТЕРА СПИННЕРУ С КАТЕГОРИЯМИ ТОВАРОВ И ОБРАБОТКА НАЖАТИЯ НА ПУНКТЫ
        if(AppPreferences.categoryAvailable) {
            category.setAdapter(categoryAdapter);
            category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (categoryAdapter.getCount() == position + 1) {
                        category.setSelection(selectedPositionWhileWork);
                        new AddCategoryDialog().show(getFragmentManager(), "BUY_LIST_ITEM_ADD_NEW_CATEGORY");
                        return;
                    }
                    selectedPositionWhileWork = position;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }



        selectedPositionWhileWork = 0;


        btnColorDialogOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(v);
                new ColorSelectDialog().show(getFragmentManager(), "d");
            }
        });

        final Random random = new Random(System.currentTimeMillis());

        btnRandomColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(v);
                if(Utils.customTemp.size()==0){
                    if(Utils.origTemp.size()==0){
                        Toast.makeText(getActivity(), R.string.no_color_themes_yet, Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                int color = random.nextInt(Utils.customTemp.size() + Utils.origTemp.size());
                adapterColorGrid.setSelectedPositionTemplate(color);
                bckColor = adapterColorGrid.getSelectedColorTemplate(true);
                txtColor = adapterColorGrid.getSelectedColorTemplate(false);
                adapterColorGrid.setSelectedPositionBoth(getContainColor(bckColor, true), getContainColor(txtColor, false), adapterColorGrid.getSelectedPositionTemp());

            }
        });





        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Utils.itemCreateStarted = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Utils.itemCreateStarted = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Utils.itemCreateStarted = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        if(AppPreferences.costAvailable) {
            cost.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    Utils.itemCreateStarted = true;
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        adapterColorGrid = new ColorGridAdapter(getActivity());

        btnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PreviewPopup().show(getFragmentManager(),"");
            }
        });

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!Utils.buyListItemEditStarted) {
                    if (Utils.availableProductsInCurrentList.containsKey(name.getText().toString().trim())) {
                        name.setError(getResources().getText(R.string.create_buy_list_item_title_error));
                        name.requestFocus();
                        return;
                    }
                    if (name.getText().toString().trim().equalsIgnoreCase("")) {
                        name.setError(getResources().getText(R.string.create_buy_list_title_error));
                        name.requestFocus();
                        return;
                    }
                    new MyAlertDialog(IntConst.BUY_LIST_ITEM_ADD).show(getFragmentManager(), "");
                } else {
                    new MyAlertDialog(IntConst.BUY_LIST_ITEM_CHANGE).show(getFragmentManager(), "");
                }
            }
        });

        if(editMode){
            editMode = false;

            name.setEnabled(false);
            name.setText(finalItem.getName());
            name.setHint(finalItem.getName());
            name.setBackgroundColor(AppPreferences.colorTheme_Background);

            preview.setImageBitmap(finalItem.getBmpPrev());

            new Thread(new Runnable() {
                @Override
                public void run() {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    finalBmp = BitmapFactory.decodeFile(finalItem.getPath(), options);
                }
            }).start();

            comment.setText(finalItem.getComment());
            comment.setHint(finalItem.getComment());
            if(AppPreferences.costAvailable) {
                cost.setText(String.valueOf(finalItem.getCost()));
                cost.setHint(String.valueOf(finalItem.getCost()));
            }
            tmpCostForEdit = finalItem.getCost();
            weight.setText(String.valueOf(finalItem.getWeight()));
            weight.setHint(String.valueOf(finalItem.getWeight()));
            if(AppPreferences.categoryAvailable) {
                category.setSelection(finalItem.getCategory());
            }
            weightType.setSelection(finalItem.getWeightType());
            weightTitle.setText(weightTypeTitleArray[finalItem.getWeightType()]);

            bckColor = finalItem.getColorBck();
            txtColor = finalItem.getColorTxt();

            tmpPathForEdit = finalItem.getPath();

            adapterColorGrid.setSelectedPositionBoth(getContainColor(bckColor, true), getContainColor(txtColor, false), getContainColorTemplate(bckColor, txtColor));
            btnAccept.setText(R.string.change);

        }


        return v;
    }
    public void acceptPrefs(){
        setColorTheme();
        if(AppPreferences.categoryAvailable) {
            categoryRoot.setVisibility(View.VISIBLE);
        } else {
            categoryRoot.setVisibility(View.GONE);
        }
        if(AppPreferences.costAvailable) {
            costRoot.setVisibility(View.VISIBLE);
        } else {
            costRoot.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    //TODO setColorTheme
    public void setColorTheme(){
        nameTitle.setTextColor(AppPreferences.colorTheme_FontColor);
        commentTitle.setTextColor(AppPreferences.colorTheme_FontColor);
        costTitle.setTextColor(AppPreferences.colorTheme_FontColor);
        if(AppPreferences.categoryAvailable) {
            categoryTitle.setTextColor(AppPreferences.colorTheme_FontColor);
            categoryTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        }
        nameTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        commentTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        costTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        currency.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        weightTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        name.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeEditText);
        comment.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeEditText);

        weight.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeEditText);


        name.setTextColor(AppPreferences.colorTheme_FontColor);
        name.setHintTextColor(AppPreferences.colorTheme_HintColor);
        name.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);

        comment.setTextColor(AppPreferences.colorTheme_FontColor);
        comment.setHintTextColor(AppPreferences.colorTheme_HintColor);
        comment.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
        if(AppPreferences.costAvailable) {
            cost.setTextColor(AppPreferences.colorTheme_FontColor);
            cost.setHintTextColor(AppPreferences.colorTheme_HintColor);
            cost.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
            cost.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeEditText);
        }

        weight.setTextColor(AppPreferences.colorTheme_FontColor);
        weight.setHintTextColor(AppPreferences.colorTheme_HintColor);
        weight.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);


        currency.setTextColor(AppPreferences.colorTheme_FontColor);
        weightTitle.setTextColor(AppPreferences.colorTheme_FontColor);

        btnRandomColor.setTextColor(AppPreferences.colorTheme_FontColor);
        btnRandomColor.setBackgroundDrawable(Utils.getStateList());
        btnColorDialogOpen.setTextColor(AppPreferences.colorTheme_FontColor);
        btnColorDialogOpen.setBackgroundDrawable(Utils.getStateList());

        btnAccept.setTextColor(AppPreferences.colorTheme_FontColor);
        btnAccept.setBackgroundDrawable(Utils.getStateList());
        btnPreview.setTextColor(AppPreferences.colorTheme_FontColor);
        btnPreview.setBackgroundDrawable(Utils.getStateList());

        bliDivider.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);

        if(AppPreferences.categoryAvailable) {
            category.getBackground().setColorFilter(AppPreferences.colorTheme_FontColor, PorterDuff.Mode.MULTIPLY);
        }
        weightType.getBackground().setColorFilter(AppPreferences.colorTheme_FontColor, PorterDuff.Mode.MULTIPLY);

        root.setBackgroundColor(AppPreferences.colorTheme_Background);
    }

    public void changeTabToBL(BuyListItem buyListItem) {
        if (mListener != null) {
            mListener.changeTabToBL(buyListItem);
        }
    }
    public void editFinished() {
        if (mListener != null) {
            mListener.editFinished();
        }
    }
    public void categoryMustBeNotified() {
        if (mListener != null) {
            mListener.categoryMustBeNotified();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (CreateBuyListItemInterface) activity;
            mListener.openFragmentCreateBLI();

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //TODO colorChanged
    @Override
    public void colorChanged(int color, boolean isBckAdapter) {
        ColorCircleContainer c = new ColorCircleContainer(false, color, false);
        if(!containColor(color, isBckAdapter)) {
            Bundle b = new Bundle();
            b.putInt("1", color);

            if(isBckAdapter) {
                Utils.customBck.add(c);
                adapterColorGrid.selectLastItem();
                adapterColorGrid.notifyDataSetChanged();
                b.putInt("2", 1);
                getLoaderManager().initLoader(IntConst.LOADER_CREATE_COLOR, b, callback).forceLoad();
            } else {
                Utils.customTxt.add(c);
                adapterColorGrid.selectLastItem();
                adapterColorGrid.notifyDataSetChanged();
                b.putInt("2", 2);
                getLoaderManager().initLoader(IntConst.LOADER_CREATE_COLOR, b, callback).forceLoad();
            }

        } else {
            Toast.makeText(getActivity(), getText(R.string.color_popup_already), Toast.LENGTH_LONG).show();
        }
    }






    //TODO PreviewPopup
    public class PreviewPopup extends DialogFragment {
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.item_of_buy_list_item, null);

            getDialog().requestWindowFeature(STYLE_NO_TITLE);

            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.Item_BLI_main_root);
            TextView TVname = (TextView) v.findViewById(R.id.Item_BLI_name);
            TextView TVcomment = (TextView) v.findViewById(R.id.Item_BLI_comment);
            TextView TVcost = (TextView) v.findViewById(R.id.Item_BLI_additional);
            ImageView IVprev = (ImageView) v.findViewById(R.id.Item_BLI_photo);

            if(finalBmp!=null){
                IVprev.setVisibility(View.VISIBLE);
                IVprev.setImageDrawable(preview.getDrawable());
            } else {
                IVprev.setVisibility(View.GONE);
            }

            String nameTmp = name.getText().toString().trim();
            if(nameTmp.equalsIgnoreCase("")) {
                nameTmp = getResources().getString(R.string.create_buy_list_name_title);
            }

            String commentTmp =  comment.getText().toString().trim();
            if(commentTmp.equalsIgnoreCase("")) {
                TVcomment.setVisibility(View.GONE);
            } else {
                TVcomment.setText(commentTmp);
                TVcomment.setTextColor(txtColor);
            }

            String weightTmp =  weight.getText().toString().trim();
            String costTmp =  cost.getText().toString().trim();

            if(weightTmp.equalsIgnoreCase("")) {
                weightTmp = "1";
            }
            TVcost.setTextColor(txtColor);

            String tmp = "";
            tmp = tmp.concat(weightTypeTitleArray[weightType.getSelectedItemPosition()])
                    .concat(": ")
                    .concat(weightTmp)
                    .concat(" ")
                    .concat(weightTypeArray[weightType.getSelectedItemPosition()]);

            if (AppPreferences.costAvailable && !costTmp.equalsIgnoreCase("")) {
                tmp = tmp.concat("   ")
                        .concat(context.getString(R.string.cost)).concat(" ")
                        .concat(costTmp)
                        .concat(" ")
                        .concat(AppPreferences.currency);
            }
            TVcost.setText(tmp);
            TVname.setText(nameTmp);

            root.setBackgroundColor(bckColor);

            TVname.setTextColor(txtColor);

            return v;
        }
    }

    private boolean containColor(int color, boolean isBck){
        for(int i = 0; Utils.orig.size() > i; i++)
            if(Utils.orig.get(i).getColor() == color)
                return true;
        if(isBck) {
            for (int i = 0; Utils.customBck.size() > i; i++)
                if (Utils.customBck.get(i).getColor() == color)
                    return true;
        } else {
            for (int i = 0; Utils.customTxt.size() > i; i++)
                if (Utils.customTxt.get(i).getColor() == color)
                    return true;
        }

        return false;
    }

    private int getContainColor(int color, boolean bck) {
        for (int i = 0; Utils.orig.size() > i; i++)
            if (Utils.orig.get(i).getColor() == color)
                return i;
        if (bck) {
            for (int i = 0; Utils.customBck.size() > i; i++)
                if (Utils.customBck.get(i).getColor() == color)
                    return i+Utils.orig.size();
        } else {
            for (int i = 0; Utils.customTxt.size() > i; i++)
                if (Utils.customTxt.get(i).getColor() == color)
                    return i+Utils.orig.size();

        }
        return -1;
    }

    private boolean isContainColorTemplate(int firstColor, int secondColor) {
        for (int i = 0; Utils.origTemp.size() > i; i++) {
            if (Utils.origTemp.get(i).getFirstColor() == firstColor && Utils.origTemp.get(i).getSecondColor() == secondColor) {
                return true;
            }
        }
        for (int i = 0; Utils.customTemp.size() > i; i++) {
            if (Utils.customTemp.get(i).getFirstColor() == firstColor && Utils.customTemp.get(i).getSecondColor() == secondColor) {
                return true;
            }
        }
        return false;
    }
    private int getContainColorTemplate(int firstColor, int secondColor) {
        for (int i = 0; Utils.origTemp.size() > i; i++)
            if (Utils.origTemp.get(i).getFirstColor() == firstColor && Utils.origTemp.get(i).getSecondColor() == secondColor) {
                return i;
            }

        for (int i = 0; Utils.customTemp.size() > i; i++) {
            if (Utils.customTemp.get(i).getFirstColor() == firstColor && Utils.customTemp.get(i).getSecondColor() == secondColor) {
                return i + Utils.origTemp.size();
            }
        }
        return -1;

    }

    public void showSoftKeyboard(){
        InputMethodManager imm =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        name.requestFocus();
        imm.showSoftInput(name, 0);

    }


    //TODO clearAll
    public void clearAll(){
        name.setEnabled(true);
        name.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
        name.setText("");
        name.setHint(R.string.create_buy_list_name_hint);
        comment.setText("");
        comment.setHint(R.string.create_buy_list_comment_hint);
        cost.setText("");
        cost.setHint("0");
        weight.setText("");
        weight.setHint("1");
        category.setSelection(0);
        weightType.setSelection(0);



        bckColor = Utils.orig.get(AppPreferences.defaultBckColor).getColor();
        txtColor = Utils.orig.get(AppPreferences.defaultTextColor).getColor();
        if(finalBmp!=null) {
            finalBmp.recycle();
            finalBmp = null;
        }
        adapterColorGrid.reset();
        finalItem = new BuyListItem();
    }

    public String savePhotoToSd(){
        String path = "";

        if(finalBmp != null) {
            if (Environment.isExternalStorageEmulated()) {
                path = context.getExternalCacheDir().getAbsolutePath()
                        + File.separator + "goods" + File.separator
                        + "img";
            } else {
                path = context.getExternalCacheDir().getAbsolutePath()
                        + File.separator + "goods" + File.separator
                        + "img";
            }

            File myDir = new File(path);
            myDir.mkdirs();
            String fname = "Image-" + System.currentTimeMillis() + ".jpg";
            fname = Utils.md5(fname);
            File file = new File(myDir, fname);

            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBmp.compress(Bitmap.CompressFormat.JPEG, 80, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
            path = file.getPath();
        }
        return path;
    }



    //TODO onCreateLoader
    @Override
    public Loader<Long> onCreateLoader(int id, Bundle args) {
        Utils.loaderStarted = true;
        switch(id){
            case IntConst.LOADER_CREATE_BLI:
                return new LoaderCreateBuyListItem(getActivity(),
                        finalItem,
                        Utils.idOfCalledBuyList,
                        IntConst.LOADER_CREATE_BLI);
            case IntConst.LOADER_ADD_COMMODITY:
                return new LoaderCreateBuyListItem(getActivity(), IntConst.LOADER_ADD_COMMODITY);
            case IntConst.LOADER_UPDATE_BLI:
                return new LoaderCreateBuyListItem(getActivity(),
                        finalItem,
                        args.getBoolean(StrConst.PATH_HAD_BEEN_CHANGED),
                        Utils.idOfCalledBuyList,
                        IntConst.LOADER_UPDATE_BLI);
            case IntConst.LOADER_GET_COLOR:
                return new LoaderCreateBuyListItem(getActivity(), IntConst.LOADER_GET_COLOR);
            case IntConst.LOADER_CREATE_COLOR:
                return new LoaderCreateBuyListItem(getActivity(),args.getInt("1"), args.getInt("3"), args.getInt("2"), IntConst.LOADER_CREATE_COLOR);
            case IntConst.LOADER_DELETE_COLOR:
                return new LoaderCreateBuyListItem(getActivity(), args.getInt("1"), args.getInt("3"), args.getInt("2"), IntConst.LOADER_DELETE_COLOR);
            case IntConst.LOADER_ADD_CATEGORY:
                return new LoaderCreateBuyListItem(getActivity(), IntConst.LOADER_ADD_CATEGORY);
        }
        return null;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //TODO onLoadFinished
    @Override
    public void onLoadFinished(Loader<Long> loader, Long rowId) {
        switch(loader.getId()){
            case IntConst.LOADER_CREATE_BLI: {
                BuyListItem bliFinal = new BuyListItem(finalItem);
                clearAll();
                changeTabToBL(bliFinal);
                getLoaderManager().destroyLoader(IntConst.LOADER_CREATE_BLI);
                break;
            }
            case IntConst.LOADER_UPDATE_BLI: {
                Utils.buyListItemEditStarted = false;
                clearAll();
                editFinished();
                getLoaderManager().destroyLoader(IntConst.LOADER_UPDATE_BLI);
                break;
            }
            case IntConst.LOADER_ADD_COMMODITY: {
                String finalComment = comment.getText().toString();
                int finalWeight = 1;
                if(!weight.getText().toString().trim().equalsIgnoreCase(""))
                    finalWeight = Integer.parseInt(weight.getText().toString().trim());
                int finalColorBck = bckColor;
                int finalColorTxt = txtColor;

                ProductInfo pi = Utils.availableProducts.get(Utils.productNames.get(Utils.productNames.size()-1));

                finalItem.setDateAdd(System.currentTimeMillis());
                finalItem.setId(pi.getId());
                finalItem.setName(pi.getName());
                finalItem.setComment(finalComment);
                finalItem.setCategory(pi.getCategory());
                finalItem.setCategoryId(pi.getCategoryId());
                finalItem.setCost(pi.getCost());
                finalItem.setColorBck(finalColorBck);
                finalItem.setColorTxt(finalColorTxt);
                finalItem.setWeight(finalWeight);
                finalItem.setWeightType(pi.getUnit());
                finalItem.setSync(false);
                finalItem.setAccountId(1);
                finalItem.setBought(false);

                if(finalBmp!=null) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bitmap = BitmapFactory.decodeFile(finalItem.getPath(), options);
                    bitmap = Bitmap.createScaledBitmap(bitmap, (int) getResources().getDimension(R.dimen.Photo_prev_size), (int) getResources().getDimension(R.dimen.Photo_prev_size), true);
                    finalItem.setBmpPrev(bitmap);
                } else {
                    finalItem.setBmpPrev(null);
                }

                getLoaderManager().destroyLoader(IntConst.LOADER_ADD_COMMODITY);
                startLoader(IntConst.LOADER_CREATE_BLI, null);

                //changeTab(bl, false, hasChange, groupPos, childPos);

                break;
            }
            case IntConst.LOADER_GET_COLOR:
                adapterColorGrid.notifyDataSetChanged();
                getLoaderManager().destroyLoader(IntConst.LOADER_GET_COLOR);
                break;
            case IntConst.LOADER_CREATE_COLOR:
                getLoaderManager().destroyLoader(IntConst.LOADER_CREATE_COLOR);
            case IntConst.LOADER_DELETE_COLOR:
                adapterColorGrid.notifyDataSetChanged();
                getLoaderManager().destroyLoader(IntConst.LOADER_DELETE_COLOR);
                break;
            case IntConst.LOADER_ADD_CATEGORY:
                categoryMustBeNotified();
                categoryAdapter.notifyDataSetChanged();
                getLoaderManager().destroyLoader(IntConst.LOADER_ADD_CATEGORY);
        }
        Utils.loaderStarted = false;

    }

    @Override
    public void onLoaderReset(Loader<Long> loader) {
        Utils.loaderStarted = false;
    }



    //TODO MyAlertDialog
    public class MyAlertDialog extends DialogFragment {

        private int id;
        private int position;
        private int color;
        private int secondColor;
        private boolean isBckAdapter;
        private boolean isTemplate;


        public MyAlertDialog(){}
        public MyAlertDialog(int id){
            this.id = id;
        }
        public MyAlertDialog(int id, int color, int position){
            this.id = id;
            this.color = color;
            this.position = position;
        }

        public MyAlertDialog(int id, int color, int position, boolean isBckAdapter, boolean isTemplate){
            this.id = id;
            this.color = color;
            this.position = position;
            this.isBckAdapter = isBckAdapter;
            this.isTemplate = isTemplate;
        }

        public MyAlertDialog(int id, int color, int secondColor, int position, boolean isBckAdapter, boolean isTemplate){
            this.id = id;
            this.color = color;
            this.secondColor = secondColor;
            this.position = position;
            this.isBckAdapter = isBckAdapter;
            this.isTemplate = isTemplate;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.alert_dialog, container, false);


            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.alertDialogRoot);
            TextView title = (TextView) v.findViewById(R.id.alertDialogTitle);
            Button accept = (Button) v.findViewById(R.id.alertDialogAccept);
            final Button cancel = (Button) v.findViewById(R.id.alertDialogCancel);

            root.setBackgroundColor(AppPreferences.colorTheme_Background);


            switch(id){
                case IntConst.BUY_LIST_ITEM_ADD:
                    title.setText(R.string.create_buy_list_add);
                    break;
                case IntConst.BUY_LIST_ITEM_DELETE_COLOR:
                    title.setText(R.string.color_delete);
                    break;
                case IntConst.BUY_LIST_ITEM_CHANGE:
                    title.setText(R.string.alert_dialog_change_save);
                    break;
                case IntConst.BUY_LIST_ITEM_DELETE_PHOTO:
                    title.setText(R.string.alert_dialog_delete_photo);
                    break;

            }

            title.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizePrefTitle);

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    switch(id){
                        //TODO BUY_LIST_ITEM_ADD
                        case IntConst.BUY_LIST_ITEM_ADD: {
                            String finalName = name.getText().toString().trim();
                            String finalComment = comment.getText().toString();
                            int finalWeight;
                            if(!weight.getText().toString().trim().equalsIgnoreCase("")) {
                                finalWeight = Integer.parseInt(weight.getText().toString().trim());
                            } else {
                                finalWeight = 1;
                            }
                            int finalUnit = weightType.getSelectedItemPosition();
                            int finalCategoryNum;
                            int finalCategoryId;
                            int finalCost;
                            int finalColorBck = bckColor;
                            int finalColorTxt = txtColor;



                            if(AppPreferences.categoryAvailable){
                                finalCategoryNum = category.getSelectedItemPosition();
                                finalCategoryId = ((GoodsCategory)category.getSelectedItem()).getId();
                            } else {
                                finalCategoryNum = 0;
                                finalCategoryId = -1;
                            }

                            if(AppPreferences.costAvailable){
                                if(!cost.getText().toString().trim().equalsIgnoreCase(""))
                                    finalCost = Integer.parseInt(cost.getText().toString().trim());
                                else
                                    finalCost = 0;
                            } else {
                                finalCost = 0;
                            }
                            Utils.costInCurrentList+=finalCost;
                            Utils.positionsInCurrentList++;

                            finalItem.setPath(savePhotoToSd());

                            if(!isContainColorTemplate(bckColor, txtColor)){
                                Utils.customTemp.add(new TwiceColorCircleContainer(bckColor, txtColor, false));

                                Bundle bndl = new Bundle();
                                bndl.putInt("1", bckColor);
                                bndl.putInt("3", txtColor);
                                bndl.putInt("2", 3);
                                startLoader(IntConst.LOADER_CREATE_COLOR, bndl);
                                adapterColorGrid.notifyDataSetChanged();
                            }

                            if(!Utils.availableProducts.containsKey(finalName)){
                                ProductInfo pi = new ProductInfo(
                                        finalName,
                                        finalCost,
                                        finalUnit,
                                        finalCategoryNum,
                                        finalCategoryId, Calendar.getInstance().getTimeInMillis());
                                Utils.availableProductsInCurrentList.put(finalName.toLowerCase(), null);
                                Utils.availableProducts.put(pi.getName(), pi);
                                Utils.productNames.add(pi.getName());
                                startLoader(IntConst.LOADER_ADD_COMMODITY, null);
                            } else {
                                if(Utils.availableProductsInCurrentList.containsKey(finalName.toLowerCase())){
                                    Toast.makeText(getActivity(), R.string.this_commodity_cannot_be_added, Toast.LENGTH_LONG).show();
                                    name.requestFocus();
                                    dismiss();
                                    return;
                                }
                                Utils.availableProductsInCurrentList.put(finalName.toLowerCase(), null);
                                ProductInfo pi = Utils.availableProducts.get(finalName);

                                if(finalBmp!=null) {
                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                                    Bitmap bitmap = BitmapFactory.decodeFile(finalItem.getPath(), options);
                                    bitmap = Bitmap.createScaledBitmap(bitmap, (int) getResources().getDimension(R.dimen.Photo_prev_size), (int) getResources().getDimension(R.dimen.Photo_prev_size), true);
                                    finalItem.setBmpPrev(bitmap);
                                } else {
                                    finalItem.setBmpPrev(null);
                                }
                                finalItem.setId(pi.getId());
                                finalItem.setName(pi.getName());
                                finalItem.setComment(finalComment);
                                finalItem.setCategory(pi.getCategory());
                                finalItem.setCategoryId(pi.getCategoryId());
                                finalItem.setCost(pi.getCost());
                                finalItem.setColorBck(finalColorBck);
                                finalItem.setColorTxt(finalColorTxt);
                                finalItem.setWeight(finalWeight);
                                finalItem.setWeightType(pi.getUnit());
                                finalItem.setSync(false);
                                finalItem.setAccountId(1);
                                finalItem.setBought(false);
                                finalItem.setDateAdd(System.currentTimeMillis());
                                startLoader(IntConst.LOADER_CREATE_BLI, null);
                            }
                            break;
                        }



                        case IntConst.BUY_LIST_ITEM_DELETE_COLOR: {
                            b.putInt("1", color);
                            if (!isTemplate) {
                                b.putInt("3", -1);
                                if (isBckAdapter) {
                                    Utils.customBck.remove(position - Utils.orig.size());
                                    adapterColorGrid.removeColor(position, true, false);
                                    b.putInt("2", 1);
                                } else {
                                    Utils.customTxt.remove(position - Utils.orig.size());
                                    adapterColorGrid.removeColor(position, false, false);
                                    b.putInt("2", 2);
                                }
                            } else {
                                Utils.customTemp.remove(position - Utils.origTemp.size());
                                adapterColorGrid.removeColor(position, false, true);
                                b.putInt("2", 3);
                                b.putInt("3", secondColor);
                            }
                            startLoader(IntConst.LOADER_DELETE_COLOR, b);
                            break;
                        }




                        case IntConst.BUY_LIST_ITEM_DELETE_PHOTO: {
                            if(finalItem.getPath()!=null) {
                                File file = new File(finalItem.getPath());
                                boolean deleted = file.delete();
                                finalBmp.recycle();
                                finalItem.setPath("");
                                finalItem.getBmpPrev().recycle();
                                finalItem.setBmpPrev(null);
                            } else {
                                finalBmp.recycle();
                            }
                            finalBmp = null;
                            preview.setImageResource(R.drawable.selector_add_photo);
                            break;
                        }




                        case IntConst.BUY_LIST_ITEM_CHANGE: {

                            String finalComment = comment.getText().toString();

                            if(finalComment.equalsIgnoreCase("")){
                                finalComment = finalItem.getComment();
                            }
                            int finalWeight;

                            if(!weight.getText().toString().trim().equalsIgnoreCase("")) {
                                finalWeight = Integer.parseInt(weight.getText().toString().trim());
                            } else {
                                finalWeight = finalItem.getWeight();
                            }
                            int finalUnit = weightType.getSelectedItemPosition();
                            int finalCategoryNum = finalItem.getCategory();
                            int finalCategoryId = finalItem.getCategoryId();
                            int finalCost = finalItem.getCost();

                            if(AppPreferences.categoryAvailable){
                                finalCategoryNum = category.getSelectedItemPosition();
                                finalCategoryId = ((GoodsCategory)category.getSelectedItem()).getId();
                            }
                            if(AppPreferences.costAvailable){
                                if(!cost.getText().toString().trim().equalsIgnoreCase(""))
                                    finalCost = Integer.parseInt(cost.getText().toString().trim());
                                else
                                    finalCost = finalItem.getCost();
                                Utils.costInCurrentList = Utils.costInCurrentList - tmpCostForEdit + finalCost;
                            }

                            finalItem.setComment(finalComment);
                            finalItem.setCategory(finalCategoryNum);
                            finalItem.setCategoryId(finalCategoryId);
                            finalItem.setCost(finalCost);
                            finalItem.setColorBck(bckColor);
                            finalItem.setColorTxt(txtColor);
                            finalItem.setWeight(finalWeight);
                            finalItem.setWeightType(finalUnit);
                            finalItem.setSync(false);
                            finalItem.setAccountId(1);
                            finalItem.setDateAdd(System.currentTimeMillis());
                            Bundle bundle = new Bundle();


                            if(!isContainColorTemplate(bckColor, txtColor)){
                                Utils.customTemp.add(new TwiceColorCircleContainer(bckColor, txtColor, false));

                                Bundle bndl = new Bundle();
                                bndl.putInt("1", bckColor);
                                bndl.putInt("3", txtColor);
                                bndl.putInt("2", 3);
                                startLoader(IntConst.LOADER_CREATE_COLOR, bndl);
                                adapterColorGrid.notifyDataSetChanged();
                            }



                            finalItem.setPath(savePhotoToSd());
                            if(finalItem.getPath().equalsIgnoreCase("")){
                                finalItem.setBmpPrev(null);
                            } else {
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                                Bitmap bitmap = BitmapFactory.decodeFile(finalItem.getPath(), options);
                                bitmap = Bitmap.createScaledBitmap(bitmap, (int) getResources().getDimension(R.dimen.Photo_prev_size), (int) getResources().getDimension(R.dimen.Photo_prev_size), true);
                                finalItem.setBmpPrev(bitmap);
                            }

                            boolean equalsPaths = !finalItem.getPath().equalsIgnoreCase(tmpPathForEdit);
                            bundle.putBoolean(StrConst.PATH_HAD_BEEN_CHANGED, equalsPaths);

                            startLoader(IntConst.LOADER_UPDATE_BLI, bundle);
                            break;
                        }
                    }
                    getDialog().dismiss();
                }
            });

            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }

    //TODO setEditMode method
    public void setEditMode(final BuyListItem bli){
        editMode = true;
        finalItem = bli;
    }

    public void openSelectionDialog(){
        new AddCategoryDialog().show(getFragmentManager(),"BUY_LIST_ITEM_ADD_NEW_CATEGORY");
    }

    //TODO startLoader method
    public void startLoader(int id, Bundle b){
        getLoaderManager().initLoader(id, b, callback).forceLoad();
    }


    //TODO AddCategoryDialog
    public class AddCategoryDialog extends DialogFragment {

        AddCategoryDialog(){}

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.category_add_popup, container, false);

            TextView title = (TextView) v.findViewById(R.id.CategoryAddPopup_title);
            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.CategoryAddPopup_root);
            final EditText editTitle = (EditText) v.findViewById(R.id.CategoryAddPopup_et);

            final Button accept = (Button) v.findViewById(R.id.CategoryAddPopup_accept);
            final Button cancel = (Button) v.findViewById(R.id.CategoryAddPopup_cancel);


            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
            accept.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);
            cancel.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);
            editTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeEditText);

            accept.setTextColor(AppPreferences.colorTheme_FontColor);
            cancel.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextColor(AppPreferences.colorTheme_FontColor);

            editTitle.setTextColor(AppPreferences.colorTheme_FontColor);
            editTitle.setHintTextColor(AppPreferences.colorTheme_HintColor);
            editTitle.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
            editTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            });
            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            editTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    editTitle.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());


            title.setText(R.string.pref_popup_title_create_category);
            accept.setText(R.string.accept);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name;
                    if ((name = editTitle.getText().toString().trim()).equalsIgnoreCase("")) {
                        editTitle.setError(getActivity().getString(R.string.create_buy_list_title_error));
                        return;
                    }
                    GoodsCategory gc = new GoodsCategory();
                    gc.setName(name);
                    if (Utils.goodsCategories.size() > 1) {
                        gc.setSort(Utils.goodsCategories.get(Utils.goodsCategories.size() - 2).getSort() + 1);
                    } else {
                        gc.setSort(0);
                    }
                    Utils.goodsCategories.add(Utils.goodsCategories.size() - 1, gc);
                    selectedPositionWhileWork = Utils.goodsCategories.size()-2;
                    category.setSelection(selectedPositionWhileWork);

                    //getLoaderManager().initLoader(IntConst.LOADER_ADD_CATEGORY, null, callback).forceLoad();
                    startLoader(IntConst.LOADER_ADD_CATEGORY, null);
                    getDialog().dismiss();
                }
            });


            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }


    public interface CreateBuyListItemInterface {
        public void changeTabToBL(BuyListItem buyListItem);
        public void editFinished();
        public void categoryMustBeNotified();
        public void openFragmentCreateBLI();
    }

    public class ColorSelectDialog extends DialogFragment {

        private Button selectBckColor;
        private Button selectTxtColor;
        private Button selectColorPattern;

        private Button cancelBtn;
        private Button acceptBtn;

        private TextView divider_1;
        private TextView divider_2;
        private TextView divider_3;

        private RelativeLayout mainRoot;

        private RelativeLayout itemRoot;
        private TextView itemName;
        private TextView itemComment;
        private TextView itemPositions;
        private ImageView itemIV;

        private GridView gridView;

        private boolean isBckAdapter;
        private boolean isTemplate;

        private int backup_colorBck;
        private int backup_colorTxt;
        private int backup_posTxt;
        private int backup_posBck;
        private int backup_posTemp;

        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {

            // the content
            final RelativeLayout root = new RelativeLayout(getActivity());
            root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            // creating the fullscreen dialog
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(root);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return dialog;
        }

        protected void applyTxtColor(int color){
            itemName.setTextColor(color);
            itemComment.setTextColor(color);
            itemPositions.setTextColor(color);
        }
        protected void applyBckColor(int color){
            itemRoot.setBackgroundColor(color);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            //getDialog().requestWindowFeature(STYLE_NO_TITLE);
            View v = inflater.inflate(R.layout.color_theme_select_popup, container, false);

            mainRoot = (RelativeLayout) v.findViewById(R.id.CTP_color_theme_root);

            itemRoot = (RelativeLayout) v.findViewById(R.id.CTP_main_root);
            itemName = (TextView) v.findViewById(R.id.CTP_name);
            itemComment = (TextView) v.findViewById(R.id.CTP_comment);
            itemPositions = (TextView) v.findViewById(R.id.CTP_additional);
            itemIV = (ImageView) v.findViewById(R.id.CTP_image);

            if(finalBmp!=null){
                itemIV.setVisibility(View.VISIBLE);
                itemIV.setImageDrawable(preview.getDrawable());
            } else {
                itemIV.setVisibility(View.GONE);
            }

            String nameTmp = name.getText().toString().trim();
            if(nameTmp.equalsIgnoreCase("")) {
                nameTmp = getResources().getString(R.string.create_buy_list_name_title);
            }

            String commentTmp =  comment.getText().toString().trim();
            if(commentTmp.equalsIgnoreCase("")) {
                itemComment.setVisibility(View.GONE);
            } else {
                itemComment.setText(commentTmp);
            }

            String weightTmp =  weight.getText().toString().trim();
            String costTmp =  cost.getText().toString().trim();

            if(weightTmp.equalsIgnoreCase("")) {
                weightTmp = "1";
            }

            String tmp = "";
            tmp = tmp.concat(weightTypeTitleArray[weightType.getSelectedItemPosition()])
                    .concat(": ")
                    .concat(weightTmp)
                    .concat(" ")
                    .concat(weightTypeArray[weightType.getSelectedItemPosition()]);

            if (AppPreferences.costAvailable && !costTmp.equalsIgnoreCase("")) {
                tmp = tmp.concat("   ")
                        .concat(context.getString(R.string.cost)).concat(" ")
                        .concat(costTmp)
                        .concat(" ")
                        .concat(AppPreferences.currency);
            }
            itemPositions.setText(tmp);
            itemName.setText(nameTmp);



            selectBckColor = (Button) v.findViewById(R.id.CTP_color_bck);
            selectTxtColor = (Button) v.findViewById(R.id.CTP_colorTxt);
            selectColorPattern = (Button) v.findViewById(R.id.CTP_color_patterns);

            cancelBtn = (Button) v.findViewById(R.id.CTP_cancel);
            acceptBtn = (Button) v.findViewById(R.id.CTP_accept);

            backup_colorBck = bckColor;
            backup_colorTxt = txtColor;
            backup_posTxt = adapterColorGrid.getSelectedPositionTxt();
            backup_posBck = adapterColorGrid.getSelectedPositionBck();
            backup_posTemp = adapterColorGrid.getSelectedPositionTemp();

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bckColor = backup_colorBck;
                    txtColor = backup_colorTxt;
                    adapterColorGrid.setSelectedPositionBoth(backup_posBck, backup_posTxt, backup_posTemp);
                    dismiss();
                }
            });

            acceptBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            selectBckColor.setActivated(true);
            selectTxtColor.setActivated(false);
            selectColorPattern.setActivated(false);


            cancelBtn.setBackgroundDrawable(Utils.getStateList());
            acceptBtn.setBackgroundDrawable(Utils.getStateList());

            selectBckColor.setBackgroundDrawable(Utils.getStateList());
            selectTxtColor.setBackgroundDrawable(Utils.getStateList());
            selectColorPattern.setBackgroundDrawable(Utils.getStateList());

            mainRoot.setBackgroundColor(AppPreferences.colorTheme_Background);

            divider_1 = (TextView) v.findViewById(R.id.CTP_divider_first);
            divider_2 = (TextView) v.findViewById(R.id.CTP_divider_second);
            divider_3 = (TextView) v.findViewById(R.id.CTP_divider_bp);

            divider_1.setBackgroundColor(AppPreferences.colorTheme_Actionbar);
            divider_2.setBackgroundColor(AppPreferences.colorTheme_Actionbar);
            divider_3.setBackgroundColor(AppPreferences.colorTheme_Actionbar);

            gridView = (GridView) v.findViewById(R.id.CTP_colorGrid);

            selectBckColor.setTextColor(AppPreferences.colorTheme_FontColor);
            selectTxtColor.setTextColor(AppPreferences.colorTheme_FontColor);
            selectColorPattern.setTextColor(AppPreferences.colorTheme_FontColor);

            cancelBtn.setTextColor(AppPreferences.colorTheme_FontColor);
            acceptBtn.setTextColor(AppPreferences.colorTheme_FontColor);

            isBckAdapter = true;
            isTemplate = false;

            applyBckColor(bckColor);
            applyTxtColor(txtColor);

            selectBckColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isBckAdapter) {
                        selectBckColor.setActivated(true);
                        selectTxtColor.setActivated(false);
                        selectColorPattern.setActivated(false);
                        isBckAdapter = true;
                        isTemplate = false;
                        adapterColorGrid.setBck(isBckAdapter);
                        adapterColorGrid.setTemplate(isTemplate);
                        adapterColorGrid.notifyDataSetChanged();
                    }
                }
            });
            selectTxtColor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isBckAdapter || isTemplate) {
                        selectBckColor.setActivated(false);
                        selectTxtColor.setActivated(true);
                        selectColorPattern.setActivated(false);
                        isBckAdapter = false;
                        isTemplate = false;
                        adapterColorGrid.setBck(isBckAdapter);
                        adapterColorGrid.setTemplate(isTemplate);
                        adapterColorGrid.notifyDataSetChanged();
                    }
                }
            });

            selectColorPattern.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!isTemplate) {
                        selectBckColor.setActivated(false);
                        selectTxtColor.setActivated(false);
                        selectColorPattern.setActivated(true);
                        isBckAdapter = false;
                        isTemplate = true;
                        adapterColorGrid.setBck(isBckAdapter);
                        adapterColorGrid.setTemplate(isTemplate);
                        adapterColorGrid.notifyDataSetChanged();
                    }
                }
            });

            adapterColorGrid.reset();
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            final DisplayMetrics metric = new DisplayMetrics();
            display.getMetrics(metric);

            gridView.setAdapter(adapterColorGrid);
            gridView.setNumColumns(GridView.AUTO_FIT);
            gridView.setColumnWidth((int) (48*metric.density));
            gridView.setVerticalSpacing(5);
            gridView.setHorizontalSpacing(5);
            gridView.setSelector(R.drawable.selector_circle);

            if(isContainColorTemplate(bckColor, txtColor)){
                adapterColorGrid.setSelectedPositionTemplate(getContainColorTemplate(bckColor, txtColor));
            } else {
                adapterColorGrid.setSelectedPositionTemplate(-1);
            }

            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    if(!isTemplate) {
                        if (!((ColorCircle) view).isVisibleAdd()) {
                            ((ColorCircle) view).setSelectedCC(true);
                            adapterColorGrid.setSelectedPosition(position);
                            if(isBckAdapter){
                                bckColor = adapterColorGrid.getSelectedColor(isBckAdapter);
                                applyBckColor(bckColor);
                            } else {
                                txtColor = adapterColorGrid.getSelectedColor(isBckAdapter);
                                applyTxtColor(txtColor);
                            }
                            if(isContainColorTemplate(bckColor, txtColor)){
                                adapterColorGrid.setSelectedPositionTemplate(getContainColorTemplate(bckColor, txtColor));
                            } else {
                                adapterColorGrid.setSelectedPositionTemplate(-1);
                            }
                            gridView.invalidateViews();

                        } else {
                            picker.show();
                            picker.setBck(isBckAdapter, bckColor, txtColor);
                        }
                    } else {
                        ((TwoColorsCircle) view).setSelectedCC(true);
                        adapterColorGrid.setSelectedPosition(position);
                        bckColor = adapterColorGrid.getSelectedColorTemplate(true);
                        txtColor = adapterColorGrid.getSelectedColorTemplate(false);
                        applyBckColor(bckColor);
                        applyTxtColor(txtColor);

                        if(containColor(bckColor, true)){
                            adapterColorGrid.setSelectedPositionBck(getContainColor(bckColor, true), true);
                        } else {
                            adapterColorGrid.setSelectedPositionBck(-1, true);
                        }

                        if(isContainColorTemplate(bckColor, txtColor)){
                            adapterColorGrid.setSelectedPositionBck(getContainColor(txtColor, false), false);
                        } else {
                            adapterColorGrid.setSelectedPositionBck(-1, false);
                        }

                        adapterColorGrid.setSelectedPosition(position);
                        gridView.invalidateViews();
                    }
                }
            });

            gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    if (!isTemplate) {
                        if (((ColorCircleContainer) (parent.getItemAtPosition(position))).isOriginal() != true) {
                            final int color = ((ColorCircleContainer) (parent.getItemAtPosition(position))).getColor();
                            new MyAlertDialog(IntConst.BUY_LIST_DELETE_COLOR, color, position, isBckAdapter, isTemplate).show(getFragmentManager(), "BUY_LIST_DELETE_COLOR");
                        }
                    } else {
                        if (((TwiceColorCircleContainer) (parent.getItemAtPosition(position))).isOriginal() != true) {
                            int color1 = ((TwiceColorCircleContainer) (parent.getItemAtPosition(position))).getFirstColor();
                            int color2 = ((TwiceColorCircleContainer) (parent.getItemAtPosition(position))).getSecondColor();
                            new MyAlertDialog(IntConst.BUY_LIST_DELETE_COLOR, color1, color2, position, isBckAdapter, isTemplate).show(getFragmentManager(), "BUY_LIST_DELETE_COLOR");
                        }
                    }

                    return true;
                }
            });

            return v;
        }
    }


    public class CameraDialog extends DialogFragment {
        ImageButton cameraShoot;
        ImageButton btnLight;

        int lightMode=0;

        RelativeLayout photoPrevContainer;
        RelativeLayout photoButtonsContainer;

        Button acceptPhoto;
        Button cancelPhoto;

        ImageView previewPhoto;

        View divider;

        View bottomBorder;
        View topBorder;


        Bitmap bmp;
        Bitmap prevBmp;


        SurfaceView surfaceView;
        Camera camera;


        public void resetCamera(){
            photoPrevContainer.setVisibility(View.GONE);
        }

        @Override
        public void onResume() {
            super.onResume();
            Camera.Size bestSize = null;

            if(!cameraOpened) {
                cameraOpened = true;
                camera = Camera.open(0);
            }

            setCameraDisplayOrientation(0);
            Camera.Parameters params = camera.getParameters();
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            params.getSupportedPictureSizes();
            int height = 480, width = 640;
            int pHeight = 480, pWidth = 640;


            params.setPictureSize(width, height);
            params.setPreviewSize(pWidth, pHeight);


            params.set("orientation", "portrait");
            params.set("rotation", 90);
            camera.setParameters(params);
            setPreviewSize();
        }




        private void setFullscreen(boolean on) {
            Window win = getActivity().getWindow();
            WindowManager.LayoutParams winParams = win.getAttributes();
            final int bits = WindowManager.LayoutParams.FLAG_FULLSCREEN;
            if (on) {
                winParams.flags |=  bits;
            } else {
                winParams.flags &= ~bits;
            }
            win.setAttributes(winParams);
        }

        @Override
        public void onPause() {
            super.onPause();
            if (camera != null) {
                camera.release();
                cameraOpened = false;
            }
            camera = null;
            setFullscreen(false);
            dismiss();
        }


        public CameraDialog() {
            // Required empty public constructor
        }

        public void closePreview(){
            photoPrevContainer.setVisibility(View.GONE);
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v =  inflater.inflate(R.layout.camera_layout, container, false);

            Rect rectangle = new Rect();
            Window window = getActivity().getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
            int statusBarTop = rectangle.top;
            int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
            int statusbarHeight = Math.abs(statusBarTop - contentViewTop);

            int displayHeight = getActivity().getResources().getDisplayMetrics().heightPixels - statusbarHeight;
            int displayWidth = getActivity().getResources().getDisplayMetrics().widthPixels;

            int borderWidth = (displayHeight - displayWidth)/2;

            cameraShoot = (ImageButton) v.findViewById(R.id.CF_cameraShoot);

            btnLight = (ImageButton) v.findViewById(R.id.CF_enableLight);

            bottomBorder =  v.findViewById(R.id.CF_bottom_border);
            topBorder =  v.findViewById(R.id.CF_top_border);
            divider =  v.findViewById(R.id.CF_divider);

            FrameLayout.LayoutParams topBorderLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, borderWidth);
            topBorderLP.gravity = Gravity.BOTTOM;
            topBorder.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, borderWidth));
            bottomBorder.setLayoutParams(topBorderLP);



            divider.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);

            photoPrevContainer = (RelativeLayout) v.findViewById(R.id.CF_root_preview);
            photoButtonsContainer = (RelativeLayout) v.findViewById(R.id.CF_camera_buttons_container);

            acceptPhoto = (Button) v.findViewById(R.id.CF_accept);
            cancelPhoto = (Button) v.findViewById(R.id.CF_cancel);

            previewPhoto = (ImageView) v.findViewById(R.id.CF_preview_iv);


            photoPrevContainer.setVisibility(View.GONE);

            btnLight.setImageDrawable(getResources().getDrawable(R.drawable.selector_light_off));
            cameraShoot.setImageDrawable(getResources().getDrawable(R.drawable.selector_camera));

            btnLight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Camera.Parameters params = camera.getParameters();
                    lightMode++;
                    lightMode%=3;
                    switch (lightMode){
                        case 0:
                            btnLight.setImageDrawable(getResources().getDrawable(R.drawable.selector_light_off));
                            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                            break;
                        case 1:
                            btnLight.setImageDrawable(getResources().getDrawable(R.drawable.selector_light_auto));
                            params.setFlashMode(Camera.Parameters.FOCUS_MODE_AUTO);
                            break;
                        case 2:
                            btnLight.setImageDrawable(getResources().getDrawable(R.drawable.selector_light_on));
                            params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                            break;
                    }


                    camera.setParameters(params);

                }
            });

            acceptPhoto.setBackgroundDrawable(Utils.getStateList());
            cancelPhoto.setBackgroundDrawable(Utils.getStateList());

            acceptPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finalBmp = bmp;
                    Bitmap bitmap = Bitmap.createScaledBitmap(finalBmp, (int) getResources().getDimension(R.dimen.Photo_prev_size), (int) getResources().getDimension(R.dimen.Photo_prev_size), true);
                    preview.setImageBitmap(bitmap);
                    dismiss();
                }
            });

            cancelPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    photoButtonsContainer.setVisibility(View.VISIBLE);
                    photoPrevContainer.setVisibility(View.GONE);
                    bmp.recycle();
                    prevBmp.recycle();
                    finalBmp = null;
                }
            });



            cameraShoot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    camera.autoFocus(new Camera.AutoFocusCallback() {
                        public void onAutoFocus(boolean success, Camera camera) {
                            if(success){
                                camera.takePicture(null, null, new Camera.PictureCallback() {
                                    @Override
                                    public void onPictureTaken(byte[] data, Camera camera) {
                                        try {
                                            photoButtonsContainer.setVisibility(View.GONE);
                                            bmp = RotateBitmap(BitmapFactory.decodeByteArray(data, 0, data.length), 90);
                                            int width = getActivity().getResources().getDisplayMetrics().widthPixels;
                                            int height = (width*bmp.getHeight())/bmp.getWidth();
                                            prevBmp = Bitmap.createScaledBitmap(bmp, width, height, true);

                                            bmp.recycle();

                                            int difHW = (height - width)/2;

                                            ByteArrayOutputStream os = new ByteArrayOutputStream();
                                            prevBmp = Bitmap.createBitmap(prevBmp, 0, difHW, width, width);
                                            prevBmp.compress(Bitmap.CompressFormat.JPEG, 80, os);

                                            bmp = BitmapFactory.decodeByteArray(os.toByteArray(), 0, os.size());

                                            prevBmp.recycle();

                                            previewPhoto.setImageBitmap(bmp);
                                            photoPrevContainer.setVisibility(View.VISIBLE);
                                            camera.startPreview();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }
                    });

                }
            });


            surfaceView = (SurfaceView) v.findViewById(R.id.CF_camera_surface);
            SurfaceHolder holder = surfaceView.getHolder();
            holder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {
                        camera.setPreviewDisplay(holder);
                        camera.startPreview();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format,
                                           int width, int height) {
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {

                }
            });
            return v;
        }

        public Bitmap RotateBitmap(Bitmap source, float angle){
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            Bitmap bm = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
            source.recycle();
            return bm;
        }


        void setPreviewSize() {

            // получаем размеры экрана
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            boolean widthIsMax = display.getWidth() > display.getHeight();

            // определяем размеры превью камеры
            Camera.Size size = camera.getParameters().getPreviewSize();

            RectF rectDisplay = new RectF();
            RectF rectPreview = new RectF();

            // RectF экрана, соотвествует размерам экрана
            rectDisplay.set(0, 0, display.getWidth(), display.getHeight());

            // RectF первью
            if (widthIsMax) {
                // превью в горизонтальной ориентации
                rectPreview.set(0, 0, size.width, size.height);
            } else {
                // превью в вертикальной ориентации
                rectPreview.set(0, 0, size.height, size.width);
            }

            Matrix matrix = new Matrix();
            // подготовка матрицы преобразования
            matrix.setRectToRect(rectPreview, rectDisplay, Matrix.ScaleToFit.START);

            // преобразование
            matrix.mapRect(rectPreview);

            // установка размеров surface из получившегося преобразования
            surfaceView.getLayoutParams().height = (int) (rectPreview.bottom);
            surfaceView.getLayoutParams().width = (int) (rectPreview.right);
        }

        void setCameraDisplayOrientation(int cameraId) {
            // определяем насколько повернут экран от нормального положения
            int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
            int degrees = 0;
            switch (rotation) {
                case Surface.ROTATION_0:
                    degrees = 0;
                    break;
                case Surface.ROTATION_90:
                    degrees = 90;
                    break;
                case Surface.ROTATION_180:
                    degrees = 180;
                    break;
                case Surface.ROTATION_270:
                    degrees = 270;
                    break;
            }

            int result = 0;

            // получаем инфо по камере cameraId
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(cameraId, info);

            // задняя камера
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                result = ((360 - degrees) + info.orientation);
            } else
                // передняя камера
                if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    result = ((360 - degrees) - info.orientation);
                    result += 360;
                }
            result = result % 360;
            camera.setDisplayOrientation(result);
        }




        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {

            // the content
            final RelativeLayout root = new RelativeLayout(getActivity());
            root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            // creating the fullscreen dialog
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(root);
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            return dialog;
        }
    }

    //TODO PreviewImageDialog
    public class PreviewImageDialog extends DialogFragment {

        PreviewImageDialog(){}

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.dialog_preview_image, container, false);


            final ImageView iv = (ImageView) v.findViewById(R.id.DPI_iv);

            final Button accept = (Button) v.findViewById(R.id.DPI_accept);
            final Button cancel = (Button) v.findViewById(R.id.DPI_cancel);

            final View divider =  v.findViewById(R.id.DPI_divider);


            iv.setImageBitmap(finalBmp);

            divider.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);

            accept.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);
            cancel.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);

            accept.setTextColor(AppPreferences.colorTheme_FontColor);
            cancel.setTextColor(AppPreferences.colorTheme_FontColor);

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                    new MyAlertDialog(IntConst.BUY_LIST_ITEM_DELETE_PHOTO).show(getFragmentManager(),"BUY_LIST_ITEM_DELETE_PHOTO");
                }
            });

            //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState) {

            // the content
            final RelativeLayout root = new RelativeLayout(getActivity());
            root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            // creating the fullscreen dialog
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(root);

            return dialog;
        }
    }


}
