package ru.fromtheseventhsky.buylist.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import ru.fromtheseventhsky.buylist.MainActivity;
import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.custom.adapters.PrefGoodsLVAdapter;
import ru.fromtheseventhsky.buylist.custom.adapters.SpinnerAdapter;
import ru.fromtheseventhsky.buylist.custom.views.CustomSpinner;
import ru.fromtheseventhsky.buylist.loaders.LoaderCreateBuyListItem;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;
import ru.fromtheseventhsky.buylist.utils.GoodsCategory;
import ru.fromtheseventhsky.buylist.utils.IntConst;
import ru.fromtheseventhsky.buylist.utils.ProductInfo;
import ru.fromtheseventhsky.buylist.utils.Utils;


public class PreferenceGoodsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Long>{


    private RelativeLayout root;
    private ListView lv;
    private PrefGoodsLVAdapter adapter;
    private Button addOrAccept;
    private LoaderManager.LoaderCallbacks callbacks;
    private ArrayList<Integer> deleteList;

    private int checkedCount;

    private PreferenceGoodsFragment pfg;

    private String deleteTitle;
    private String acceptTitle;


    private CustomSpinner tmpCS;

    public static PreferenceGoodsFragment newInstance() {
        PreferenceGoodsFragment fragment = new PreferenceGoodsFragment();
        return fragment;
    }

    public PreferenceGoodsFragment() {
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pref_goods_lay, container, false);

        addOrAccept = (Button) view.findViewById(R.id.PrefGoodsLay_add);
        root = (RelativeLayout) view.findViewById(R.id.PrefGoodsLay_root);
        lv = (ListView) view.findViewById(R.id.PrefGoodsLay_lv);

        Utils.deleteMode = false;
        callbacks = this;

        checkedCount = 0;

        deleteList = new ArrayList<Integer>();

        applyTheme();

        pfg = this;

        deleteTitle = getText(R.string.delete_mode_add_btn_title_btn_on).toString();
        acceptTitle = getText(R.string.pref_commodity_fragment_add_btn_title).toString();

        adapter = new PrefGoodsLVAdapter(getActivity());
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(Utils.deleteMode){
                    CheckBox chb = (CheckBox) view.findViewById(R.id.PrefGoodsLvItem_chb);
                    if(chb.isChecked()) {
                        checkedCount--;
                    } else {
                        checkedCount++;
                    }
                    chb.setChecked(!chb.isChecked());
                    adapter.setDeletePosition(position, chb.isChecked());
                    if(checkedCount>0)
                        addOrAccept.setText(deleteTitle.concat(" (").concat(String.valueOf(checkedCount)).concat(")"));
                    else
                        addOrAccept.setText(deleteTitle);
                } else {
                    new PreferencesAddCommodityDialog(IntConst.EDIT, position).show(getFragmentManager(), "EDIT");
                }
            }
        });

        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                switchDeleteMode(position);
                if(Utils.deleteMode) {
                    checkedCount++;
                    addOrAccept.setText(deleteTitle.concat(" (").concat(String.valueOf(checkedCount)).concat(")"));
                }
                ((MainActivity)getActivity()).setTitleDeleteMode();
                return true;
            }
        });

        addOrAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.deleteMode){
                    if(checkedCount==0){
                        Toast.makeText(getActivity(),getText(R.string.delete_mode_title_toast),Toast.LENGTH_LONG).show();
                        return;
                    }
                    adapter.fillDeleteList(deleteList);
                    getLoaderManager().initLoader(IntConst.LOADER_REMOVE_COMMODITY, null, callbacks).forceLoad();
                } else {
                    new PreferencesAddCommodityDialog(IntConst.CREATE).show(getFragmentManager(), "CREATE");
                }
            }
        });



        return view;
    }

    public void switchDeleteMode(int pos){
        checkedCount = 0;
        Utils.deleteMode = Utils.deleteMode ? false : true;
        addOrAccept.setText(Utils.deleteMode ? deleteTitle : acceptTitle);
        adapter.setRemoveMode(Utils.deleteMode, pos);
    }

    public void applyTheme(){
        //deleteModeBtn.setBackgroundDrawable(Utils.getStateList());
        addOrAccept.setBackgroundDrawable(Utils.getStateList());
        root.setBackgroundColor(AppPreferences.colorTheme_Background);
        //deleteModeBtn.setTextColor(AppPreferences.colorTheme_FontColor);
        //deleteModeBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
        addOrAccept.setTextColor(AppPreferences.colorTheme_FontColor);
        addOrAccept.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
    }

    @Override
    public Loader<Long> onCreateLoader(int id, Bundle bundle) {
        switch (id){
            case IntConst.LOADER_ADD_CATEGORY:
                return new LoaderCreateBuyListItem(getActivity(), IntConst.LOADER_ADD_CATEGORY);
            case IntConst.LOADER_ADD_COMMODITY:
                return new LoaderCreateBuyListItem(getActivity(), IntConst.LOADER_ADD_COMMODITY);
            case IntConst.LOADER_REMOVE_COMMODITY:
                return new LoaderCreateBuyListItem(getActivity(), deleteList, IntConst.LOADER_REMOVE_COMMODITY);
            case IntConst.LOADER_UPDATE_COMMODITY:
                return new LoaderCreateBuyListItem(getActivity(), bundle.getInt("clickedPosition"), IntConst.LOADER_UPDATE_COMMODITY);
            case IntConst.LOADER_UPDATE_COMMODITY_LAST_USED_DATE:
                return new LoaderCreateBuyListItem(getActivity(), bundle.getInt("clickedPosition"), IntConst.LOADER_UPDATE_COMMODITY_LAST_USED_DATE);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Long> longLoader, Long aLong) {
        switch (longLoader.getId()){
            case IntConst.LOADER_ADD_COMMODITY:
                adapter.notifyDataSetChanged();
                getLoaderManager().destroyLoader(IntConst.LOADER_ADD_COMMODITY);
                break;
            case IntConst.LOADER_ADD_CATEGORY:
                getLoaderManager().destroyLoader(IntConst.LOADER_ADD_CATEGORY);
                break;
            case IntConst.LOADER_REMOVE_COMMODITY:
                adapter.notifyDataSetChanged();
                switchDeleteMode(-1);
                getLoaderManager().destroyLoader(IntConst.LOADER_REMOVE_COMMODITY);
                break;
            case IntConst.LOADER_UPDATE_COMMODITY:
                getLoaderManager().destroyLoader(IntConst.LOADER_UPDATE_COMMODITY);
                break;
            case IntConst.LOADER_UPDATE_COMMODITY_LAST_USED_DATE:
                adapter.notifyDataSetChanged();
                getLoaderManager().destroyLoader(IntConst.LOADER_UPDATE_COMMODITY_LAST_USED_DATE);

                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Long> longLoader) {

    }

    //TODO AddCategoryDialog
    public class AddCategoryDialog extends DialogFragment {

        AddCategoryDialog(){}

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.category_add_popup, container, false);

            TextView title = (TextView) v.findViewById(R.id.CategoryAddPopup_title);
            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.CategoryAddPopup_root);
            final EditText editTitle = (EditText) v.findViewById(R.id.CategoryAddPopup_et);

            final Button accept = (Button) v.findViewById(R.id.CategoryAddPopup_accept);
            final Button cancel = (Button) v.findViewById(R.id.CategoryAddPopup_cancel);


            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
            accept.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);
            cancel.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeButton);
            editTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeEditText);

            accept.setTextColor(AppPreferences.colorTheme_FontColor);
            cancel.setTextColor(AppPreferences.colorTheme_FontColor);
            title.setTextColor(AppPreferences.colorTheme_FontColor);

            editTitle.setTextColor(AppPreferences.colorTheme_FontColor);
            editTitle.setHintTextColor(AppPreferences.colorTheme_HintColor);
            editTitle.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
            editTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            });
            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            editTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    editTitle.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());


            title.setText(R.string.pref_popup_title_create_category);
            accept.setText(R.string.accept);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getDialog().dismiss();
                }
            });
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name;
                    if ((name = editTitle.getText().toString().trim()).equalsIgnoreCase("")) {
                        editTitle.setError(getActivity().getString(R.string.create_buy_list_title_error));
                        return;
                    }
                    GoodsCategory gc = new GoodsCategory();
                    gc.setName(name);
                    if (Utils.goodsCategories.size() > 1) {
                        gc.setSort(Utils.goodsCategories.get(Utils.goodsCategories.size() - 2).getSort() + 1);
                    } else {
                        gc.setSort(0);
                    }
                    Utils.goodsCategories.add(Utils.goodsCategories.size() - 1, gc);
                    tmpCS.setSelection(Utils.goodsCategories.size() - 2);

                    //getLoaderManager().initLoader(IntConst.LOADER_ADD_CATEGORY, null, callback).forceLoad();
                    startLoader(IntConst.LOADER_ADD_CATEGORY, null);
                    getDialog().dismiss();
                }
            });


            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }


    //todo dialog for add commodity
    public class PreferencesAddCommodityDialog extends DialogFragment {

        private int id;
        private int clickedPosition;

        PreferencesAddCommodityDialog(int id, int clickedPosition){
            this.id = id;
            this.clickedPosition = clickedPosition;
        }
        PreferencesAddCommodityDialog(int id){
            this.id = id;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v;
            v = inflater.inflate(R.layout.commodity_add_popup, container, false);

            TextView title = (TextView) v.findViewById(R.id.CommodityLay_titleName);
            RelativeLayout root = (RelativeLayout) v.findViewById(R.id.CommodityLay_root);
            final EditText editTitle = (EditText) v.findViewById(R.id.CommodityLay_enterName);
            final Button accept = (Button) v.findViewById(R.id.CommodityLay_accept);
            final Button cancel = (Button) v.findViewById(R.id.CommodityLay_cancel);

            final Spinner weight = (Spinner) v.findViewById(R.id.CommodityLay_spinnerWeight);
            final CustomSpinner category = (CustomSpinner) v.findViewById(R.id.CommodityLay_spinnerCategory);

            tmpCS = category;

            final EditText editCost = (EditText) v.findViewById(R.id.CommodityLay_enterCost);

            String[] weightTypeArray = getActivity().getResources().getStringArray(R.array.weight_type);

            final ArrayAdapter<String> weightTypeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_tv, weightTypeArray);
            final SpinnerAdapter categoryAdapter = new SpinnerAdapter(inflater, getActivity());
            title.setTextSize(TypedValue.COMPLEX_UNIT_PX, AppPreferences.fontSizeTextView);
            title.setTextColor(AppPreferences.colorTheme_FontColor);

            if(AppPreferences.costAvailable){
                editCost.setTextColor(AppPreferences.colorTheme_FontColor);
                editCost.setHintTextColor(AppPreferences.colorTheme_HintColor);
                editCost.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
            } else {
                v.findViewById(R.id.relativeLayoutCost).setVisibility(View.GONE);
            }
            if(!AppPreferences.categoryAvailable){
                v.findViewById(R.id.relativeLayoutCategory).setVisibility(View.GONE);
            }
            editTitle.setTextColor(AppPreferences.colorTheme_FontColor);
            editTitle.setHintTextColor(AppPreferences.colorTheme_HintColor);
            editTitle.setBackgroundColor(AppPreferences.colorTheme_EditTextBck);
            root.setBackgroundColor(AppPreferences.colorTheme_Background);

            editTitle.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    editTitle.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            category.setPGF(pfg);

            accept.setBackgroundDrawable(Utils.getStateList());
            cancel.setBackgroundDrawable(Utils.getStateList());

            switch (id){
                case IntConst.EDIT: {
                    title.setText(R.string.pref_popup_title_edit_commodity);
                    ProductInfo pi = Utils.availableProducts.get(Utils.productNames.get(clickedPosition));
                    editTitle.setHint(pi.getName());
                    editTitle.setText(pi.getName());
                    if (AppPreferences.costAvailable) {
                        editCost.setText(String.valueOf(pi.getCost()));
                        editCost.setHint(String.valueOf(pi.getCost()));
                    }
                    weight.setAdapter(weightTypeAdapter);

                    if(AppPreferences.categoryAvailable) {
                        category.setAdapter(categoryAdapter);
                        category.setSelection(pi.getCategory());
                    }
                    weight.setSelection(pi.getUnit());

                    accept.setText(R.string.change);

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getDialog().dismiss();
                        }
                    });
                    accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!editTitle.getText().toString().trim().equalsIgnoreCase("")) {
                                Utils.availableProducts.get(Utils.productNames.get(clickedPosition)).setName(editTitle.getText().toString().trim());
                            }
                            if (AppPreferences.costAvailable) {
                                if(!editCost.getText().toString().trim().equalsIgnoreCase("")) {
                                    Utils.availableProducts.get(Utils.productNames.get(clickedPosition)).setCost(Integer.parseInt(editCost.getText().toString().trim()));
                                }
                            } else {
                                Utils.availableProducts.get(Utils.productNames.get(clickedPosition)).setCost(0);
                            }

                            Utils.availableProducts.get(Utils.productNames.get(clickedPosition)).setUnit(weight.getSelectedItemPosition());
                            if(AppPreferences.categoryAvailable) {
                                Utils.availableProducts.get(Utils.productNames.get(clickedPosition)).setCategory(category.getSelectedItemPosition());
                                Utils.availableProducts.get(Utils.productNames.get(clickedPosition)).setCategoryId((int)categoryAdapter.getItemId(category.getSelectedItemPosition()));
                            } else {
                                Utils.availableProducts.get(Utils.productNames.get(clickedPosition)).setCategory(0);
                                Utils.availableProducts.get(Utils.productNames.get(clickedPosition)).setCategoryId(-1);
                            }


                            Bundle b = new Bundle();
                            b.putInt("clickedPosition", clickedPosition);
                            startLoader(IntConst.LOADER_UPDATE_COMMODITY, b);

                            adapter.notifyDataSetChanged();
                            getDialog().dismiss();
                        }
                    });
                }
                break;
                case IntConst.CREATE: {
                    title.setText(R.string.pref_popup_title_create_commodity);
                    weight.setAdapter(weightTypeAdapter);
                    category.setAdapter(categoryAdapter);
                    accept.setText(R.string.accept);
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getDialog().dismiss();
                        }
                    });
                    accept.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(editTitle.getText().toString().trim().equalsIgnoreCase("")) {
                                editTitle.setError(getActivity().getString(R.string.create_buy_list_title_error));
                                return;
                            }

                            String costText = "";
                            if(AppPreferences.costAvailable) {
                                costText = editCost.getText().toString().trim();
                            }
                            int cost;
                            if(costText.equalsIgnoreCase(""))
                                cost = 0;
                            else
                                cost = Integer.parseInt(editCost.getText().toString().trim());
                            String name = editTitle.getText().toString().trim();
                            int categoryNum = 0;
                            int categoryId = -1;

                            if(AppPreferences.categoryAvailable) {
                                categoryNum = category.getSelectedItemPosition();
                                categoryId = (int) categoryAdapter.getItemId(category.getSelectedItemPosition());
                            }
                            int weightNum = weight.getSelectedItemPosition();

                            ProductInfo pi = new ProductInfo(
                                    name,
                                    cost,
                                    weightNum,
                                    categoryNum,
                                    categoryId, Calendar.getInstance().getTimeInMillis());
                            Utils.availableProducts.put(pi.getName(), pi);
                            Utils.productNames.add(pi.getName());
                            startLoader(IntConst.LOADER_ADD_COMMODITY, null);

                            getDialog().dismiss();
                        }
                    });
                }
                break;
            }

            editTitle.requestFocus();
            showSoftKeyboard(editTitle);
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            return v;
        }
    }

    public void openSelectionDialog(){
        new AddCategoryDialog().show(getFragmentManager(),"BUY_LIST_ITEM_ADD_NEW_CATEGORY");
    }

    public void showSoftKeyboard(View view){
        InputMethodManager imm =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }

    public void startLoader(int id, Bundle b){
        getLoaderManager().initLoader(id, b, callbacks).forceLoad();
    }

}
