package ru.fromtheseventhsky.buylist.fragments;


import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import ru.fromtheseventhsky.buylist.R;
import ru.fromtheseventhsky.buylist.utils.AppPreferences;

public class CameraFragment extends Fragment {

    ImageButton cameraShoot;
    ImageButton btnLight;

    int lightMode=0;

    RelativeLayout photoPrevContainer;
    RelativeLayout photoButtonsContainer;

    Button acceptPhoto;
    Button cancelPhoto;

    ImageView previewPhoto;

    View divider;

    View bottomBorder;
    View topBorder;


    Bitmap bmp;
    Bitmap prevBmp;


    SurfaceView surfaceView;
    Camera camera;
    MediaRecorder mediaRecorder;


    public void resetCamera(){
        photoPrevContainer.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setFullscreen(true);
        Camera.Size bestSize = null;

        camera = Camera.open(0);

        setCameraDisplayOrientation(0);
        Camera.Parameters params = camera.getParameters();
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        params.getSupportedPictureSizes();
        //List<Camera.Size> pictureSizeList = params.getSupportedPictureSizes();
        int height = 480, width = 640;
        int pHeight = 480, pWidth = 640;

        //for(Camera.Size pictureSize : pictureSizeList){
        //    if(pictureSize.width  == 1280 && pictureSize.height == 720){
        //        height = 720;
        //        width = 1280;
        //    }
        //}
        //List<Camera.Size> previewSizeList = params.getSupportedPreviewSizes();

        //for(Camera.Size previewSize : previewSizeList){
        //    if(previewSize.width  == 1280 && previewSize.height == 720){
        //        pHeight = 720;
        //        pWidth = 1280;
        //    }
        //}


        params.setPictureSize(width, height);
        params.setPreviewSize(pWidth, pHeight);


        params.set("orientation", "portrait");
        params.set("rotation", 90);
        camera.setParameters(params);
        setPreviewSize();
    }




    private void setFullscreen(boolean on) {
        Window win = getActivity().getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int bits = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        if (on) {
            winParams.flags |=  bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaRecorder();
        if (camera != null)
            camera.release();
        camera = null;
        setFullscreen(false);
    }

    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            mediaRecorder.reset();
            mediaRecorder.release();
            mediaRecorder = null;
            camera.lock();
        }
    }

    private PhotoReturnListener mListener;


    public static CameraFragment newInstance() {
        CameraFragment fragment = new CameraFragment();
        return fragment;
    }

    public CameraFragment() {
        // Required empty public constructor
    }

    public void closePreview(){
        photoPrevContainer.setVisibility(View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.camera_layout, container, false);


        int displayHeight = getActivity().getResources().getDisplayMetrics().widthPixels;
        int displayWidth = getActivity().getResources().getDisplayMetrics().widthPixels;

        int borderWidth = (displayHeight - displayWidth)/2;

        cameraShoot = (ImageButton) v.findViewById(R.id.CF_cameraShoot);

        btnLight = (ImageButton) v.findViewById(R.id.CF_enableLight);

        bottomBorder =  v.findViewById(R.id.CF_divider);
        topBorder =  v.findViewById(R.id.CF_top_border);
        divider =  v.findViewById(R.id.CF_bottom_border);

        topBorder.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, borderWidth));
        bottomBorder.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, borderWidth));

        divider.setBackgroundColor(AppPreferences.colorTheme_SelectedBtnColor);

        photoPrevContainer = (RelativeLayout) v.findViewById(R.id.CF_root_preview);
        photoButtonsContainer = (RelativeLayout) v.findViewById(R.id.CF_camera_buttons_container);

        acceptPhoto = (Button) v.findViewById(R.id.CF_accept);
        cancelPhoto = (Button) v.findViewById(R.id.CF_cancel);

        previewPhoto = (ImageView) v.findViewById(R.id.CF_preview_iv);


        photoPrevContainer.setVisibility(View.GONE);

        btnLight.setImageDrawable(getResources().getDrawable(R.drawable.add_new_list));


        btnLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters params = camera.getParameters();
                lightMode++;
                lightMode%=3;
                switch (lightMode){
                    case 0:
                        btnLight.setImageDrawable(getResources().getDrawable(R.drawable.add_new_list));
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        break;
                    case 1:
                        btnLight.setImageDrawable(getResources().getDrawable(R.drawable.add_new_list));
                        params.setFlashMode(Camera.Parameters.FOCUS_MODE_AUTO);
                        break;
                    case 2:
                        btnLight.setImageDrawable(getResources().getDrawable(R.drawable.add_new_list));
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                        break;
                }


                camera.setParameters(params);

            }
        });

        acceptPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prevBmp.recycle();


            }
        });

        cancelPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoPrevContainer.setVisibility(View.GONE);
                bmp.recycle();
                prevBmp.recycle();
            }
        });



        cameraShoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.autoFocus(new Camera.AutoFocusCallback() {
                    public void onAutoFocus(boolean success, Camera camera) {
                        if(success){
                            camera.takePicture(null, null, new Camera.PictureCallback() {
                                @Override
                                public void onPictureTaken(byte[] data, Camera camera) {
                                    try {
                                        bmp = RotateBitmap(BitmapFactory.decodeByteArray(data, 0, data.length), 90);
                                        int width = getActivity().getResources().getDisplayMetrics().widthPixels;
                                        int height = (width*bmp.getHeight())/bmp.getWidth();
                                        prevBmp = Bitmap.createScaledBitmap(bmp, width, width, true);

                                        ByteArrayOutputStream os = new ByteArrayOutputStream();
                                        prevBmp.compress(Bitmap.CompressFormat.JPEG, 80, os);

                                        previewPhoto.setImageBitmap(BitmapFactory.decodeByteArray(os.toByteArray(), 0, os.size()));
                                        photoPrevContainer.setVisibility(View.VISIBLE);
                                        camera.startPreview();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                    }
                });

            }
        });


        surfaceView = (SurfaceView) v.findViewById(R.id.CF_camera_surface);
        SurfaceHolder holder = surfaceView.getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    camera.setPreviewDisplay(holder);
                    camera.startPreview();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });
        return v;
    }

    public Bitmap RotateBitmap(Bitmap source, float angle){
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Bitmap bm = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        source.recycle();
        return bm;
    }


    void setPreviewSize() {

        // получаем размеры экрана
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        boolean widthIsMax = display.getWidth() > display.getHeight();

        // определяем размеры превью камеры
        Camera.Size size = camera.getParameters().getPreviewSize();

        RectF rectDisplay = new RectF();
        RectF rectPreview = new RectF();

        // RectF экрана, соотвествует размерам экрана
        rectDisplay.set(0, 0, display.getWidth(), display.getHeight());

        // RectF первью
        if (widthIsMax) {
            // превью в горизонтальной ориентации
            rectPreview.set(0, 0, size.width, size.height);
        } else {
            // превью в вертикальной ориентации
            rectPreview.set(0, 0, size.height, size.width);
        }

        Matrix matrix = new Matrix();
        // подготовка матрицы преобразования
        matrix.setRectToRect(rectPreview, rectDisplay, Matrix.ScaleToFit.START);

        // преобразование
        matrix.mapRect(rectPreview);

        // установка размеров surface из получившегося преобразования
        surfaceView.getLayoutParams().height = (int) (rectPreview.bottom);
        surfaceView.getLayoutParams().width = (int) (rectPreview.right);
    }

    void setCameraDisplayOrientation(int cameraId) {
        // определяем насколько повернут экран от нормального положения
        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;

        // получаем инфо по камере cameraId
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        // задняя камера
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
            result = ((360 - degrees) + info.orientation);
        } else
            // передняя камера
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                result = ((360 - degrees) - info.orientation);
                result += 360;
            }
        result = result % 360;
        camera.setDisplayOrientation(result);
    }

    public void PhotoReturn(ArrayList<Bitmap> allPhoto) {
        if (mListener != null) {
            mListener.PhotoReturn(allPhoto);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (PhotoReturnListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PhotoReturnListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface PhotoReturnListener {
        public void PhotoReturn(ArrayList<Bitmap> allPhoto);
    }


}
