package ru.fromtheseventhsky.buylist.utils;

import android.graphics.Bitmap;

/**
 * информация о конкретной покупке
 */
public class BuyListItem {
    private int id;
    private String name;            //название покупки
    private String comment;         //коммент к покупке
    private int cost;               //цена
    private int weightType;         //тип веса, к примеру, кг, фунты
    private int weight;             //вес
    private int category;           //категория
    private int categoryId;         //айди категории
    private boolean bought;         //куплено ли
    private int colorBck;           //цвет бекграунда
    private int colorTxt;           //цвет текста
    private long dateAdd;           //цвет текста
    private String path;            //путь к фотке на карте SD
    private boolean sync;           //синхронизовано ли
    private int accountId;          //айди аккаунт к которому привязано это
    private Bitmap bmpPrev;         //превьюха маленькая

    public BuyListItem(int id, String name, String comment, int cost, int weightType, int weight, int category, int categoryId, boolean bought, int colorBck, int colorTxt, int dateAdd, boolean sync, int accountId, String path){
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.cost = cost;
        this.weightType = weightType;
        this.weight = weight;
        this.category = category;
        this.categoryId = categoryId;
        this.bought = bought;
        this.colorBck = colorBck;
        this.colorTxt = colorTxt;
        this.dateAdd = dateAdd;
        this.sync = sync;
        this.accountId = accountId;
        this.path = path;
        bmpPrev = null;
    }

    public BuyListItem(){
    }

    public Bitmap getBmpPrev() {
        return bmpPrev;
    }

    public void setBmpPrev(Bitmap bmpPrev) {
        this.bmpPrev = bmpPrev;
    }
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getDateAdd() {
        return dateAdd;
    }

    public void setDateAdd(long dateAdd) {
        this.dateAdd = dateAdd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getWeightType() {
        return weightType;
    }

    public void setWeightType(int weightType) {
        this.weightType = weightType;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public boolean isBought() {
        return bought;
    }

    public void setBought(boolean bought) {
        this.bought = bought;
    }

    public int getColorBck() {
        return colorBck;
    }

    public void setColorBck(int colorBck) {
        this.colorBck = colorBck;
    }

    public int getColorTxt() {
        return colorTxt;
    }

    public void setColorTxt(int colorTxt) {
        this.colorTxt = colorTxt;
    }

    public boolean isSync() {
        return sync;
    }

    public void setSync(boolean sync) {
        this.sync = sync;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
    public BuyListItem(
            BuyListItem buyListItem){
        this.dateAdd = buyListItem.dateAdd;
        this.id = buyListItem.id;
        this.name = buyListItem.name;
        this.comment = buyListItem.comment;
        this.cost = buyListItem.cost;
        this.weightType = buyListItem.weightType;
        this.weight = buyListItem.weight;
        this.sync = buyListItem.sync;
        this.category = buyListItem.category;
        this.categoryId = buyListItem.categoryId;
        this.bought = buyListItem.bought;
        this.colorBck = buyListItem.colorBck;
        this.colorTxt = buyListItem.colorTxt;
        this.accountId = buyListItem.accountId;
        this.path = buyListItem.getPath();
        this.bmpPrev = buyListItem.getBmpPrev();
    }
}
