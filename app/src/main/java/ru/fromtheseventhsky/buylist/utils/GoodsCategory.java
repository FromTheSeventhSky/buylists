package ru.fromtheseventhsky.buylist.utils;


public class GoodsCategory {
    private int id;
    private int sort;
    private String name;

    public GoodsCategory(int id, int sort, String name){
        this.id = id;

        this.sort = sort;
        this.name = name;
    }
    public GoodsCategory(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
