package ru.fromtheseventhsky.buylist.utils;


import java.util.ArrayList;

public class AppPreferences {
    public static int fontSize;
    public static float fontSizeTitleList;
    public static float fontSizeCommentList;
    public static float fontSizeDate;
    public static float fontSizeCurrencyAndWeight;
    public static float fontSizeTextView;
    public static float fontSizeEditText;
    public static float fontSizeCheckBox;
    public static float fontSizeButton;
    public static float fontSizeSpinner;
    public static float fontSizePrefTitle;
    public static float fontSizePrefComment;
    public static float fontSizePrefGoodsTitle;


    public static int colorThemeNumber;

    public static int colorTheme_ListViewShadow;
    public static int colorTheme_Actionbar;
    public static int colorTheme_Background;
    public static int colorTheme_EditTextBck;
    public static int colorTheme_FontColor;
    public static int colorTheme_HintColor;
    public static int colorTheme_SelectedBtnColor;
    public static int colorTheme_DisabledBtnColor;
    public static int colorTheme_DeselectedBtnColor;

    public static ArrayList<String> weightType;

    public static boolean costAvailable;
    public static boolean categoryAvailable;

    public static String currency;
    public static int defaultBckColor;
    public static int defaultTextColor;
    public static int defaultTempColor;

    public static boolean vibrationEnabled;
    public static boolean remember;
    public static boolean delete;
    public static long deletePeriod;
    public static boolean screenLightAlways;


    public static boolean doubleTapToEdit;



}
