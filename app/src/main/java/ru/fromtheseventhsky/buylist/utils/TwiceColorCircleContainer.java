package ru.fromtheseventhsky.buylist.utils;


public class TwiceColorCircleContainer {
    private int firstColor;
    private int secondColor;
    private boolean original;

    public TwiceColorCircleContainer(int firstColor, int secondColor, boolean original){
        this.firstColor = firstColor;
        this.secondColor = secondColor;
        this.original = original;
    }

    public int getFirstColor() {
        return firstColor;
    }


    public int getSecondColor() {
        return secondColor;
    }

    public boolean isOriginal() {
        return original;
    }

}
