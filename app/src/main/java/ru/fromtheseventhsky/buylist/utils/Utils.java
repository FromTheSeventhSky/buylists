package ru.fromtheseventhsky.buylist.utils;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import ru.fromtheseventhsky.buylist.R;

public class Utils {

    public static boolean buyListCreateStarted;
    public static boolean buyListItemCreateStarted;

    public static boolean buyListEditStarted;
    public static boolean buyListItemEditStarted;

    public static boolean buyListItemDeleteMode;

    public static boolean itemCreateStarted;


    public static long idOfCalledBuyList;
    public static long idOfSelectedProduct;

    public static boolean deleteMode = false;

    public static int levelPosition;

    public static int currentFragment;

    public static boolean loaderStarted = false;

    public static boolean isPrefLvlSecondOpened = false;

    public static boolean isFragmentCreateBuyListThemeChanged = false;
    public static boolean isFragmentCreateBuyListItemThemeChanged = false;
    public static boolean isFragmentBuyListThemeChanged = false;

    public static boolean preferencesVisible = false;
    public static boolean hasInit = false;

    public static void init(){
        if(!hasInit){
            hasInit = true;
            Utils.orig = new ArrayList();
            Utils.customBck = new ArrayList();
            Utils.customTxt = new ArrayList();
            Utils.origTemp = new ArrayList();
            Utils.customTemp = new ArrayList();
            Utils.goodsCategories = new ArrayList<>();
            Utils.availableProducts = new HashMap<>();
            Utils.productNames = new ArrayList<>();
            Utils.todayAndWeekArrList = new ArrayList<>();
            Utils.availableProductsInCurrentList = new HashMap<>();
            Utils.expandedBuyLists = new ArrayList<>();
            Utils.arrListsExpanded = new HashMap<>();

            Utils.currentFragment = IntConst.FRAGMENT_MAIN;

            Utils.orig.add(new ColorCircleContainer(true, Color.WHITE, false));
            Utils.orig.add(new ColorCircleContainer(true, Color.LTGRAY, false));
            Utils.orig.add(new ColorCircleContainer(true, Color.GRAY, false));
            Utils.orig.add(new ColorCircleContainer(true, Color.DKGRAY, false));
            Utils.orig.add(new ColorCircleContainer(true, Color.BLACK, false));

            Utils.origTemp.add(new TwiceColorCircleContainer(Color.BLACK, Color.WHITE, true));
            Utils.origTemp.add(new TwiceColorCircleContainer(Color.BLACK, Color.LTGRAY, true));
            Utils.origTemp.add(new TwiceColorCircleContainer(Color.GRAY, Color.WHITE, true));
            Utils.origTemp.add(new TwiceColorCircleContainer(Color.WHITE, Color.DKGRAY, true));
            Utils.origTemp.add(new TwiceColorCircleContainer(Color.WHITE, Color.BLACK, true));
        }
    }

    public static StateListDrawable getStateList(){
        StateListDrawable btnSelector = new StateListDrawable();
        btnSelector.addState(new int[]{android.R.attr.state_pressed},
                new ColorDrawable(AppPreferences.colorTheme_SelectedBtnColor));
        btnSelector.addState(new int[]{android.R.attr.state_activated},
                new ColorDrawable(AppPreferences.colorTheme_SelectedBtnColor));
        btnSelector.addState(new int[]{-android.R.attr.state_enabled},
                new ColorDrawable(AppPreferences.colorTheme_SelectedBtnColor));
        btnSelector.addState(new int[]{},
                new ColorDrawable(AppPreferences.colorTheme_DeselectedBtnColor));
        return btnSelector;
    }

    public static void reFillTodayAndWeekArrList(){
        Calendar yesterday = Calendar.getInstance();
        yesterday.set(Calendar.HOUR_OF_DAY, 23);
        yesterday.set(Calendar.MINUTE, 59);
        yesterday.set(Calendar.SECOND, 59);
        yesterday.set(Calendar.MILLISECOND, 59);

        Utils.todayAndWeekArrList.clear();

        for(int i =0; i < 10; i++){
            if(i < 9) {
                Utils.todayAndWeekArrList.add(yesterday.getTimeInMillis());
                yesterday.add(Calendar.DATE, -1);
            } else {
                Utils.todayAndWeekArrList.add(1L);
            }
        }
    }



    public static boolean changeNotSavedBuyListItem;

    public static ArrayList<ColorCircleContainer> orig;
    public static ArrayList<ColorCircleContainer> customBck;
    public static ArrayList<ColorCircleContainer> customTxt;

    public static ArrayList<TwiceColorCircleContainer> origTemp;
    public static ArrayList<TwiceColorCircleContainer> customTemp;

    public static ArrayList<GoodsCategory> goodsCategories;

    public static ArrayList<Boolean> expandedBuyLists;
    public static HashMap<Integer, Boolean> arrListsExpanded;



    public static ArrayList<String> productNames;
    public static HashMap<String, ProductInfo> availableProducts;
    public static HashMap<String, Void> availableProductsInCurrentList;

    public static int positionsInCurrentList;
    public static int boughtInCurrentList;
    public static int costInCurrentList;

    public static ArrayList<Long> todayAndWeekArrList;

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getTitle(int num, Context ctx){

        if(AppPreferences.categoryAvailable) {
            if(num==IntConst.ID_BOUGHT_CAT_ON){
                return ctx.getString(R.string.buy_list_title_bought);
            } else {
                for (int i = 0; i < goodsCategories.size(); i++) {
                    GoodsCategory gc = goodsCategories.get(i);
                    if(gc.getId() == num){
                        return gc.getName();
                    }
                }

            }

        } else {
            if (num == IntConst.ID_NOT_BOUGHT_CAT_OFF)
                return ctx.getString(R.string.buy_list_title_not_bought);
            if (num == IntConst.ID_BOUGHT_CAT_OFF)
                return ctx.getString(R.string.buy_list_title_bought);
        }

        return "";
    }

    public static String getTitleWeek(int num, Context ctx){

        if (num == 0)
            return ctx.getString(R.string.day_of_week_today);
        if (num == 1)
            return ctx.getString(R.string.day_of_week_yesterday);
        if (num == 8)
            return ctx.getString(R.string.day_of_week_ago);
        if (num == 9)
            return ctx.getString(R.string.day_of_week_old);
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(todayAndWeekArrList.get(num));
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                return ctx.getString(R.string.day_of_week_sunday);
            case 2:
                return ctx.getString(R.string.day_of_week_monday);
            case 3:
                return ctx.getString(R.string.day_of_week_tuesday);
            case 4:
                return ctx.getString(R.string.day_of_week_wednesday);
            case 5:
                return ctx.getString(R.string.day_of_week_thursday);
            case 6:
                return ctx.getString(R.string.day_of_week_friday);
            case 7:
                return ctx.getString(R.string.day_of_week_saturday);
        }


        return "";
    }




}
