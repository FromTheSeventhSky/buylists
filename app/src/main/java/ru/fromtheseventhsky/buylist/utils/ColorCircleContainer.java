package ru.fromtheseventhsky.buylist.utils;


public class ColorCircleContainer {
    private int color;
    private boolean isAdd;
    private boolean original;

    public ColorCircleContainer(boolean original, int color, boolean isAdd){
        this.isAdd = isAdd;
        this.color = color;
        this.original = original;
    }
    public int getColor() {
        return color;
    }

    public boolean getIsAdd() {
        return isAdd;
    }

    public boolean isOriginal() {
        return original;
    }
}
