package ru.fromtheseventhsky.buylist.utils;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import java.io.File;

public class DBHelper {
    static DB dbHelper;
    static SQLiteDatabase db;
    static boolean opened=false;


    public static void initWritable(Context context){
        if(!opened) {

            String name;
            if(Environment.isExternalStorageEmulated()) {
                name = context.getExternalCacheDir().getAbsolutePath()
                        + File.separator + "/files/" + File.separator
                        + "cache.cch";
            } else {
                name = context.getExternalCacheDir().getAbsolutePath()
                        + File.separator + "/files/" + File.separator
                        + "cache.cch";
            }
            dbHelper = new DB(context, name, null, 1);
            db = dbHelper.getWritableDatabase();
            opened=true;
        }
    }

    public static void close(){
        dbHelper.close();
        opened=false;
    }

    public static SQLiteDatabase getDb(){
        return db;
    }
}