package ru.fromtheseventhsky.buylist.utils;

/**
 * Created by 123 on 16.09.2015.
 */
public class CalendarCell {
    private int day;
    private int month;
    private int weekDay;

    public CalendarCell(int day, int month){
        this.day = day;
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(int weekDay) {
        this.weekDay = weekDay;
    }
}
