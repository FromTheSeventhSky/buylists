package ru.fromtheseventhsky.buylist.utils;

/**
 * информация о данном списке покупок
 */
public class BuyList {
    public int id;
    public String name;
    public String comment;
    public long date;
    public long dateEnd;
    public int colorBck;
    public int colorTxt;
    public int type;
    public boolean sync;
    public int accountId;
    public int positions;
    public int bought;
    public int cost;

    public BuyList(){};

    public BuyList(int id,
            String name,
            String comment,
            long date,
            long dateEnd,
            int colorBck,
            int colorTxt,
            int type,
            boolean sync,
            int accountId,
            int positions,
            int bought,
            int cost){
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.date = date;
        this.dateEnd = dateEnd;
        this.colorBck = colorBck;
        this.colorTxt = colorTxt;
        this.type = type;
        this.sync = sync;
        this.accountId = accountId;
        this.positions = positions;
        this.bought = bought;
        this.cost = cost;
    }
    public BuyList(BuyList obj){
        this.id = obj.id;
        this.name = obj.name;
        this.comment = obj.comment;
        this.date = obj.date;
        this.dateEnd = obj.dateEnd;
        this.colorBck = obj.colorBck;
        this.colorTxt = obj.colorTxt;
        this.type = obj.type;
        this.sync = obj.sync;
        this.accountId = obj.accountId;
        this.positions = obj.positions;
        this.bought = obj.bought;
        this.cost = obj.cost;
    }
}
