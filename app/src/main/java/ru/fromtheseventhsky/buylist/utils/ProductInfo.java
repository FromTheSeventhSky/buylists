package ru.fromtheseventhsky.buylist.utils;


public class ProductInfo {
    private int id;
    private String name;
    private int cost;
    private int unit;
    private int category;
    private int categoryId;
    private long lastUsed;

    public ProductInfo(int id, String name, int cost, int unit, int category, int categoryId, long lastUsed){
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.unit = unit;
        this.category = category;
        this.categoryId = categoryId;
        this.lastUsed = lastUsed;
    }
    public ProductInfo(String name, int cost, int unit, int category, int categoryId, long lastUsed){
        this.name = name;
        this.cost = cost;
        this.unit = unit;
        this.category = category;
        this.categoryId = categoryId;
        this.lastUsed = lastUsed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public long getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(long lastUsed) {
        this.lastUsed = lastUsed;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
