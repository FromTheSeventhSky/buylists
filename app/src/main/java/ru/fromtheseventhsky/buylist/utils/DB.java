package ru.fromtheseventhsky.buylist.utils;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DB extends SQLiteOpenHelper {

    private static final String BUY_LIST_TABLE =
            "create table buy_list (" +
                    "id integer primary key autoincrement, " +      //АЙДИШНИК
                    "name TEXT, " +                                 //
                    "comment TEXT, " +                              //
                    "date_start INTEGER," +                         //ДАТА НАЧАЛА
                    "date_end INTEGER," +                           //ДАТА УСТАРЕВАНИЯ
                    "type INTEGER," +                               //ТИП - АКТУАЛЬНО 1, УСТАРЕЛО 2
                    "colorBck INTEGER," +                           //ЦВЕТ ПУНКТА
                    "colorTxt INTEGER," +                           //ЦВЕТ ТЕКСТА
                    "positions INTEGER," +                          //ЧИСЛО ПОЗИЦИЙ
                    "bought INTEGER," +                             //ЧИСЛО КУПЛЕННОГО
                    "cost INTEGER" +                                //ЦЕНА
                    ");";

    private static final String COLOR_TABLE =
            "create table colors (" +
                    "id integer primary key autoincrement, " +      //АЙДИШНИК
                    "type INTEGER, " +                              //ТИП - ДЛЯ БЕКГРАУНДА 1 ИЛИ ДЛЯ ТЕКСТА 2
                    "color INTEGER);";                              //ЦВЕТ

    private static final String COLOR_TABLE_TEMPLATES =
            "create table colors_temp (" +
                    "id integer primary key autoincrement, " +      //АЙДИШНИК
                    "colorFirst INTEGER, " +                        //ТИП - ДЛЯ БЕКГРАУНДА 1 ИЛИ ДЛЯ ТЕКСТА 2
                    "colorSecond INTEGER);";                        //ЦВЕТ

    private static final String RELATIONS_TABLE =
            "create table relations (" +
                    "id integer primary key autoincrement, " +      //АЙДИШНИК
                    "id_buy_list INTEGER, " +                       //АЙДИ СПИСКА ПОКУПОК
                    "id_goods INTEGER," +                           //АЙДИ ТОВАРА
                    "comment TEXT," +                               //КОММЕНТАРИЙ
                    "type INTEGER," +                               //ТИП - КУПЛЕНО НЕ КУПЛЕНО
                    "colorBck INTEGER," +                           //ЦВЕТ ПУНКТА
                    "colorTxt INTEGER," +                           //ЦВЕТ ТЕКСТА
                    "dateAdd INTEGER," +                            //ЦВЕТ ТЕКСТА
                    "photo TEXT," +                                 //ФОТО ТОВАРА
                    "unitCount INTEGER" +                           //ЧИСЛО ЕДИНИЦ ИЗМЕРЕНИЯ
                    ");";



    private static final String GOODS_TABLE =
            "create table goods (" +
                    "id integer primary key autoincrement, " +      //АЙДИШНИК
                    "name TEXT, " +                                 //НАЗВАНИЕ
                    "cost INTEGER, " +                              //ЦЕНА
                    "unit INTEGER," +                               //ЕДИНИЦА ИЗМЕРЕНИЯ
                    "category INTEGER," +                           //КАТЕГОРИЯ
                    "categoryId INTEGER," +                         //АЙДИ КАТЕГОРИИ
                    "lastUseDate INTEGER" +                         //ДАТА ПОСЛЕДНЕГО ИСПОЛЬЗОВАНИ
                    ");";


    private static final String CATEGORIES =
            "create table categories (" +
                    "id integer primary key autoincrement, " +      //АЙДИШНИК
                    "name TEXT," +                                  //НАЗВАНИЕ
                    "sort INTEGER" +
                    ");";


    Context context;

    public DB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, null, version);
        this.context = context;
    }

    public DB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BUY_LIST_TABLE);
        db.execSQL(COLOR_TABLE);
        db.execSQL(RELATIONS_TABLE);
        db.execSQL(GOODS_TABLE);
        db.execSQL(COLOR_TABLE_TEMPLATES);
        db.execSQL(CATEGORIES);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}