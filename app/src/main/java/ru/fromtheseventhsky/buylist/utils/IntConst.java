package ru.fromtheseventhsky.buylist.utils;


public class IntConst {

    public static final int DIVIDER_1 = 0;
    public static final int COLOR_THEME = 1;
    public static final int FONT_SIZE = 2;
    public static final int DIVIDER_2 = 3;
    public static final int USE_CATEGORIES = 4;
    public static final int CATEGORIES = 5;
    public static final int USE_COST = 6;
    public static final int CURRENCY = 7;
    public static final int COLOR_DEFAULT = 8;
    public static final int DIVIDER_3 = 9;
    public static final int VIBRATION = 10;
    public static final int REMEMBER = 11;
    public static final int GOODS = 12;
    public static final int REMOVE_OLD = 13;
    public static final int LIGHT_SCREEN = 14;
    public static final int DIVIDER_4 = 15;
    public static final int BACKUP_CREATE = 16;
    public static final int BACKUP_RECOVER = 17;

    public static final int LOADER_GET_GOODS_LIST= 20000;
    public static final int LOADER_GET_CATEGORIES_LIST= 21020;
    public static final int LOADER_GET_MAIN_PAGE_GOODS_LIST= 21022;

    public static final int LOADER_CHANGE_ITEM_TYPE= 21052;
    public static final int LOADER_CHANGE_TYPE_IF_OLD= 21062;

    public static final int LOADER_UPDATE_COMMODITY = 20001;
    public static final int LOADER_ADD_COMMODITY = 20002;
    public static final int LOADER_REMOVE_COMMODITY = 20003;
    public static final int LOADER_UPDATE_COMMODITY_LAST_USED_DATE = 20004;


    public static final int LOADER_ADD_CATEGORY = 30002;
    public static final int LOADER_REMOVE_CATEGORY = 30003;
    public static final int LOADER_UPDATE_CATEGORY_NAME = 30004;
    public static final int LOADER_UPDATE_CATEGORY_SORT = 30005;


    public static final int LOADER_UPDATE_BL = 720405;
    public static final int LOADER_CREATE_BL = 720406;
    public static final int LOADER_REMOVE_BL = 720407;

    public static final int LOADER_GET_BLI = 727807;
    public static final int LOADER_GET_BUY_LIST_WITH_ITEMS = 667802;

    public static final int LOADER_SET_BOUGHT_TRUE = 427407;
    public static final int LOADER_SET_BOUGHT_FALSE = 427904;

    public static final int LOADER_UPDATE_BLI = 7300605;
    public static final int LOADER_CREATE_BLI = 7300606;

    public static final int LOADER_GET_COLOR = 7200605;
    public static final int LOADER_CREATE_COLOR = 7200606;
    public static final int LOADER_DELETE_COLOR = 7200607;

    public static final int FRAGMENT_MAIN = 456701;
    public static final int FRAGMENT_CREATE_BL = 456702;
    public static final int FRAGMENT_BL = 456703;
    public static final int FRAGMENT_CREATE_BLI = 456704;
    public static final int FRAGMENT_PREF = 456705;
    public static final int FRAGMENT_PREF_CATEGORY = 456706;
    public static final int FRAGMENT_PREF_GOODS = 456707;
    public static final int FRAGMENT_ = 456708;
    public static final int FRAGMENT_EDIT_BL = 456709;

    public static final int EDIT = 353;
    public static final int CREATE = 354;

    public static final int BUY_LIST_ADD = 354981;
    public static final int BUY_LIST_DELETE_COLOR = 354982;
    public static final int BUY_LIST_UNDO_CHANGES = 354983;
    public static final int BUY_LIST_CHANGE = 354984;

    public static final int BUY_LIST_ITEM_ADD = 7354981;
    public static final int BUY_LIST_ITEM_ADD_NEW_CATEGORY = 7654381;
    public static final int BUY_LIST_ITEM_DELETE_COLOR = 7354982;
    public static final int BUY_LIST_ITEM_DELETE_PHOTO = 7354987;
    public static final int BUY_LIST_ITEM_UNDO_CHANGES = 7354983;
    public static final int BUY_LIST_ITEM_CHANGE = 7354984;


    public static final int ID_BOUGHT_CAT_ON = -3489677;
    public static final int ID_NOT_BOUGHT_CAT_OFF = -3489671;
    public static final int ID_BOUGHT_CAT_OFF = -3489672;

    public static final int ID_DIALOG_EDIT_BL = -54856721;
    public static final int ID_DIALOG_EDIT_BLI = -54856726;
    public static final int ID_DIALOG_CREATE_BL = -54856826;


    public static final float SHORT_DURATION = 0.5f;
    public static final float MEDIUM_DURATION = 1f;
    public static final float LONG_DURATION = 2f;



}
