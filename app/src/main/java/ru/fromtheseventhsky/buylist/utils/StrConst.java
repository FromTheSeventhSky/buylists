package ru.fromtheseventhsky.buylist.utils;


public class StrConst {

    // public static String ;

    public static String APP_PREFS = "appPreference";
    public static String PREF_FIRST_START = "firstStart";
    public static String PREF_SCHEME_NUM = "schemeNum";
    public static String PREF_FONT_SIZE_NUM = "fontSize";
    public static String PREF_FONT_TYPE_NUM = "fontType";
    public static String PREF_CURRENCY = "currencyName";
    public static String DOUBLE_TAP_TO_EDIT = "doubleTapToEdit";
    public static String PREF_COLOR_BCK = "defColorBck";
    public static String PREF_COLOR_TXT = "defColorTxt";
    public static String PREF_COLOR_TEMP = "defColorTmp";
    public static String PREF_VIBRATION = "vibration";
    public static String PREF_REMEMBER = "remember";
    public static String PREF_DELETE_PERIOD = "period";
    public static String PREF_SHUT_OFF_DISPLAY = "shutOff";
    public static String CATEGORIES_AVAILABLE = "categoriesAvailable";
    public static String COST_AVAILABLE = "costAvailable";

    public static String PATH_HAD_BEEN_CHANGED = "PATH_HAD_BEEN_CHANGED";

}
